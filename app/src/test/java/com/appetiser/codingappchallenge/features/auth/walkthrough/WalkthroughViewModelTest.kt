package com.appetiser.codingappchallenge.features.auth.walkthrough

import com.appetiser.codingappchallenge.core.BaseViewModelTest
import com.appetiser.codingappchallenge.utils.PAGE_STEP_1_POSITION
import com.appetiser.codingappchallenge.utils.PAGE_STEP_2_POSITION
import com.appetiser.codingappchallenge.utils.PAGE_STEP_3_POSITION
import com.appetiser.codingappchallenge.utils.PAGE_STEP_4_POSITION
import com.appetiser.module.domain.utils.argumentCaptor
import com.appetiser.module.domain.utils.mock
import io.reactivex.observers.TestObserver
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import org.mockito.Mockito.times

class WalkthroughViewModelTest : BaseViewModelTest() {

    private lateinit var subject: WalkthroughViewModel

    private val observer: TestObserver<WalkthroughState> = mock()

    @Before
    fun setUp() {
        subject = WalkthroughViewModel()
        subject.schedulers = schedulers
        subject.state.subscribe(observer)
    }

    @Test
    fun onPageSelected_ShouldEmitCorrectPage_WhenSelectedPageIs2() {
        val previousPage = PAGE_STEP_1_POSITION
        val currentPage = PAGE_STEP_2_POSITION
        val expected =
            WalkthroughState.UpdatePageIndicator(
                previousPage,
                currentPage
            )

        subject.onPageSelected(currentPage)

        argumentCaptor<WalkthroughState>()
            .run {
                Mockito
                    .verify(
                        observer,
                        times(1)
                    )
                    .onNext(capture())

                Assert.assertEquals(expected, value)
            }
    }

    @Test
    fun onPageSelected_ShouldEmitCorrectPage_WhenSelectedPageIs3() {
        val previousPage = PAGE_STEP_2_POSITION
        val currentPage = PAGE_STEP_3_POSITION
        val expected =
            WalkthroughState.UpdatePageIndicator(
                previousPage,
                currentPage
            )

        subject.onPageSelected(PAGE_STEP_2_POSITION)
        subject.onPageSelected(currentPage)

        argumentCaptor<WalkthroughState>()
            .run {
                Mockito
                    .verify(
                        observer,
                        times(2)
                    )
                    .onNext(capture())

                Assert.assertEquals(expected, value)
            }
    }

    @Test
    fun onPageSelected_ShouldEmitShowStep4Buttons_WhenSelectedPageIs4() {
        val currentPage = PAGE_STEP_4_POSITION
        val expected = WalkthroughState.ShowStep4Buttons

        subject.onPageSelected(PAGE_STEP_3_POSITION)
        subject.onPageSelected(currentPage)

        argumentCaptor<WalkthroughState>()
            .run {
                Mockito
                    .verify(
                        observer,
                        times(3)
                    )
                    .onNext(capture())

                Assert.assertEquals(expected, value)
            }
    }

    @Test
    fun onPageSelected_ShouldEmitHideStep4Buttons_WhenSelectedPageIs4AndPreviousPageIs3() {
        val currentPage = PAGE_STEP_3_POSITION
        val expected = WalkthroughState.HideStep4Buttons

        subject.onPageSelected(PAGE_STEP_4_POSITION)
        subject.onPageSelected(currentPage)

        argumentCaptor<WalkthroughState>()
            .run {
                Mockito
                    .verify(
                        observer,
                        times(4)
                    )
                    .onNext(capture())

                Assert.assertEquals(expected, value)
            }
    }
}
