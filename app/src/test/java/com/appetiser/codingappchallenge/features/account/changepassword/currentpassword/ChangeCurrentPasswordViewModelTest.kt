package com.appetiser.codingappchallenge.features.account.changepassword.currentpassword

import com.appetiser.codingappchallenge.core.BaseViewModelTest
import com.appetiser.module.common.utils.ResourceManager
import com.appetiser.module.data.features.auth.AuthRepository
import com.appetiser.module.domain.utils.anyInt
import com.appetiser.module.domain.utils.argumentCaptor
import com.appetiser.module.domain.utils.mock
import com.appetiser.module.domain.utils.whenever
import io.reactivex.Observer
import io.reactivex.Single
import okhttp3.ResponseBody.Companion.toResponseBody
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import retrofit2.HttpException
import retrofit2.Response

class ChangeCurrentPasswordViewModelTest : BaseViewModelTest() {

    private val authRepository: AuthRepository = mock()
    private val resourceManager: ResourceManager = mock()
    private val stateObserver: Observer<ChangeCurrentPasswordState> = mock()

    private lateinit var subject: ChangeCurrentPasswordViewModel

    @Before
    fun setUp() {
        subject = ChangeCurrentPasswordViewModel(authRepository, resourceManager)
        subject.schedulers = schedulers
        subject.state.subscribe(stateObserver)
    }

    @Test
    fun onPasswordTextChanged_ShouldDisableButton_WhenPasswordIsEmpty() {
        val password = ""
        val expectedState = ChangeCurrentPasswordState.DisableButton

        subject.onPasswordTextChanged(password)

        argumentCaptor<ChangeCurrentPasswordState>()
            .run {
                Mockito
                    .verify(
                        stateObserver,
                        Mockito.times(1)
                    )
                    .onNext(capture())

                Assert.assertEquals(expectedState, value)
            }
    }

    @Test
    fun onPasswordTextChanged_ShouldEnableButton_WhenPasswordIsNotEmpty() {
        val password = "password"
        val expectedState = ChangeCurrentPasswordState.EnableButton

        subject.onPasswordTextChanged(password)

        argumentCaptor<ChangeCurrentPasswordState>()
            .run {
                Mockito
                    .verify(
                        stateObserver,
                        Mockito.times(1)
                    )
                    .onNext(capture())

                Assert.assertEquals(expectedState, value)
            }
    }

    @Test
    fun verifyPassword_ShouldNotCallRepository_WhenPasswordIsEmpty() {
        val password = ""

        subject.verifyPassword(password)

        Mockito.verifyZeroInteractions(authRepository)
    }

    @Test
    fun verifyPassword_ShouldEmitInvalidPassword_WhenPasswordIsIncorrect() {
        val password = "incorrect password"
        val expectedState1 = ChangeCurrentPasswordState.ShowLoading
        val expectedState2 = ChangeCurrentPasswordState.HideLoading
        val expectedState3 = ChangeCurrentPasswordState.InvalidPassword
        val throwable =
            HttpException(
                Response.error<String>(
                    400,
                    "Test exception".toResponseBody()
                )
            )

        whenever(authRepository.verifyPassword(password))
            .thenReturn(Single.error(throwable))

        subject.verifyPassword(password)
        testScheduler.triggerActions()

        argumentCaptor<ChangeCurrentPasswordState>()
            .run {
                Mockito
                    .verify(
                        stateObserver,
                        Mockito.times(3)
                    )
                    .onNext(capture())

                Assert.assertEquals(expectedState1, allValues[0])
                Assert.assertEquals(expectedState2, allValues[1])
                Assert.assertEquals(expectedState3, allValues[2])
            }
    }

    @Test
    fun verifyPassword_ShouldEmitError_WhenResponseThrowsException() {
        val password = "password"
        val errorMessage = "Test error"
        val expectedState1 = ChangeCurrentPasswordState.ShowLoading
        val expectedState2 = ChangeCurrentPasswordState.HideLoading
        val expectedState3 = ChangeCurrentPasswordState.Error(errorMessage)
        val throwable = Throwable(errorMessage)

        whenever(authRepository.verifyPassword(password))
            .thenReturn(Single.error(throwable))
        whenever(resourceManager.getString(anyInt()))
            .thenReturn(errorMessage)

        subject.verifyPassword(password)
        testScheduler.triggerActions()

        argumentCaptor<ChangeCurrentPasswordState>()
            .run {
                Mockito
                    .verify(
                        stateObserver,
                        Mockito.times(3)
                    )
                    .onNext(capture())

                Assert.assertEquals(expectedState1, allValues[0])
                Assert.assertEquals(expectedState2, allValues[1])
                Assert.assertEquals(expectedState3, allValues[2])
            }
    }

    @Test
    fun verifyPassword_ShouldEmitVerificationSuccess_WhenResponseIsSuccessful() {
        val password = "password"
        val verificationToken = "test verification token"
        val expectedState1 = ChangeCurrentPasswordState.ShowLoading
        val expectedState2 = ChangeCurrentPasswordState.HideLoading
        val expectedState3 =
            ChangeCurrentPasswordState.VerificationSuccess(
                password,
                verificationToken
            )

        whenever(authRepository.verifyPassword(password))
            .thenReturn(Single.just(verificationToken))

        subject.verifyPassword(password)
        testScheduler.triggerActions()

        argumentCaptor<ChangeCurrentPasswordState>()
            .run {
                Mockito
                    .verify(
                        stateObserver,
                        Mockito.times(3)
                    )
                    .onNext(capture())

                Assert.assertEquals(expectedState1, allValues[0])
                Assert.assertEquals(expectedState2, allValues[1])
                Assert.assertEquals(expectedState3, allValues[2])
            }
    }
}
