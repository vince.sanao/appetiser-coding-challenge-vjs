package com.appetiser.codingappchallenge.features.auth.login
import com.appetiser.codingappchallenge.Stubs
import com.appetiser.codingappchallenge.Stubs.SESSION_LOGGED_IN
import com.appetiser.codingappchallenge.Stubs.SESSION_NOT_VERIFIED
import com.appetiser.codingappchallenge.Stubs.SESSION_USER_LOGGED_IN_NO_FULLNAME
import com.appetiser.codingappchallenge.Stubs.SESSION_USER_LOGGED_IN_NO_PROFILE_PHOTO
import com.appetiser.codingappchallenge.core.BaseViewModelTest
import com.appetiser.codingappchallenge.utils.PhoneNumberHelper
import com.appetiser.module.common.utils.ResourceManager
import com.appetiser.module.data.features.auth.AuthRepository
import com.appetiser.module.domain.utils.any
import io.reactivex.Single
import io.reactivex.observers.TestObserver
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentCaptor
import org.mockito.Mockito.*
import com.appetiser.module.domain.utils.mock
import com.appetiser.module.domain.utils.whenever

class LoginViewModelTest : BaseViewModelTest() {

    private lateinit var loginViewModel: LoginViewModel

    private val deviceToken = "sampledevicetoken"
    private val deviceId = "device_id"

    private val repository: AuthRepository = mock()
    private val resourceManager: ResourceManager = mock()
    private val phoneNumberHelper: PhoneNumberHelper = mock()

    private val observer: TestObserver<LoginState> = mock()

    @Before
    fun setup() {
        loginViewModel = LoginViewModel(
            repository,
            resourceManager,
            phoneNumberHelper
        )
        loginViewModel.schedulers = schedulers
        loginViewModel.state.subscribe(observer)
    }

    @Test
    fun login_ShouldEmitLoginSuccess_WhenUserIsLoggedIn() {
        val email = "test@test.test"
        val password = "password"
        val session = SESSION_LOGGED_IN
        val expected = LoginState.LoginSuccess(user = Stubs.USER_LOGGED_IN)

        whenever(repository.loginWithEmail(any(), any()))
            .thenReturn(Single.just(session))

        loginViewModel.login(email, password)
        testScheduler.triggerActions()

        ArgumentCaptor.forClass(LoginState.LoginSuccess::class.java).run {
            verify(observer, times(3)).onNext(capture())
            Assert.assertEquals(expected, value)
        }
    }

    @Test
    fun login_ShouldEmitNoUserFirstAndLastName_WhenUserHasNoFirstAndLastName() {
        val email = "test@test.test"
        val password = "password"
        val session = SESSION_USER_LOGGED_IN_NO_FULLNAME
        val expected = LoginState.NoFullName

        whenever(repository.loginWithEmail(any(), any()))
            .thenReturn(Single.just(session))

        loginViewModel.login(email, password)
        testScheduler.triggerActions()

        ArgumentCaptor.forClass(LoginState.LoginSuccess::class.java).run {
            verify(observer, times(3)).onNext(capture())
            Assert.assertEquals(expected, value)
        }
    }

    @Test
    fun login_ShouldEmitNoProfilePhoto_WhenUserHasNoProfilePhoto() {
        val email = "test@test.test"
        val password = "password"
        val session = SESSION_USER_LOGGED_IN_NO_PROFILE_PHOTO
        val expected = LoginState.NoProfilePhoto

        whenever(repository.loginWithEmail(any(), any()))
            .thenReturn(Single.just(session))

        loginViewModel.login(email, password)
        testScheduler.triggerActions()

        ArgumentCaptor.forClass(LoginState.LoginSuccess::class.java).run {
            verify(observer, times(3)).onNext(capture())
            Assert.assertEquals(expected, value)
        }
    }

    @Test
    fun login_ShouldEmitUserNotVerified_WhenUserIsNotVerified() {
        val email = "test@test.test"
        val password = "password"
        val session = SESSION_NOT_VERIFIED
        val expected =
            LoginState
                .UserNotVerified(
                    session.user,
                    session.user.email,
                    session.user.phoneNumber
                )

        whenever(repository.loginWithEmail(any(), any()))
            .thenReturn(Single.just(session))

        loginViewModel.login(email, password)
        testScheduler.triggerActions()

        ArgumentCaptor.forClass(LoginState.UserNotVerified::class.java).run {
            verify(observer, times(3)).onNext(capture())
            Assert.assertEquals(expected, value)
        }
    }

    @Test
    fun login_ShouldEmitErrorState_WhenResponseThrowsException() {
        val email = "foo@bar.baz"
        val password = "password"

        val error = Throwable("Something went wrong")

        val expected = LoginState.Error(error)

        whenever(repository.loginWithEmail(any(), any()))
            .thenReturn(Single.error(error))

        loginViewModel.login(email, password)
        testScheduler.triggerActions()

        ArgumentCaptor.forClass(LoginState.Error::class.java).run {
            verify(observer, atLeast(2)).onNext(capture())
            Assert.assertEquals(expected, value)
        }
    }
}
