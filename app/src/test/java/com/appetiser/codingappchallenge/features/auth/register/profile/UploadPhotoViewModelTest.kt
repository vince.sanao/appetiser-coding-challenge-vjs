package com.appetiser.codingappchallenge.features.auth.register.profile

import com.appetiser.codingappchallenge.Stubs
import com.appetiser.codingappchallenge.core.BaseViewModelTest
import com.appetiser.module.common.utils.ResourceManager
import com.appetiser.module.data.features.auth.AuthRepository
import com.appetiser.module.data.features.user.UserRepository
import com.appetiser.module.domain.models.session.Session
import com.appetiser.module.domain.utils.any
import com.appetiser.module.domain.utils.argumentCaptor
import com.appetiser.module.domain.utils.mock
import com.appetiser.module.domain.utils.whenever
import io.reactivex.Single
import io.reactivex.observers.TestObserver
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.times
import org.mockito.Mockito.verify

class UploadPhotoViewModelTest : BaseViewModelTest() {

    private lateinit var subject: UploadPhotoViewModel
    private val resourceManager: ResourceManager = mock()

    private val authRepository: AuthRepository = mock()
    private val repository: UserRepository = mock()
    private val observer: TestObserver<UploadPhotoState> = mock()

    @Before
    fun setup() {
        subject = UploadPhotoViewModel(authRepository, repository, resourceManager)
        subject.schedulers = schedulers
        subject.state.subscribe(observer)
    }

    @Test
    fun uploadPhoto_ShouldEmitSuccessUploadPhoto_WhenResponseIsSuccessful() {
        val filePath = mutableListOf<String>()
        filePath.add("/mnt/test")

        val user = Stubs.USER_SESSION_LOGGED_IN_NO_EMAIL
        val expectedPair = Pair("/mnt/test", "/mnt/test")

        val expectedState1 = UploadPhotoState.ShowProgressLoading
        val expectedState2 = UploadPhotoState.HideProgressLoading
        val expectedState3 = UploadPhotoState.ShowSubscriptionScreen
        val session = Session(user, Stubs.VALID_ACCESS_TOKEN)

        whenever(resourceManager.scaleDownImage(any()))
            .thenReturn(Single.just(expectedPair))

        whenever(repository.uploadPhoto(any()))
            .thenReturn(Single.just(user))

        whenever(authRepository.onBoardingCompleted())
            .thenReturn(Single.just(session))

        whenever(repository.getUser())
            .thenReturn(Single.just(user))

        subject.uploadPhoto(filePath)
        testScheduler.triggerActions()

        argumentCaptor<UploadPhotoState>()
            .run {
                verify(
                    observer,
                    times(3)
                ).onNext(capture())

                Assert.assertEquals(expectedState1, allValues[0])
                Assert.assertEquals(expectedState2, allValues[1])
                Assert.assertEquals(expectedState3, allValues[2])
            }
    }

    @Test
    fun uploadPhoto_ShouldEmitErrorUploadPhoto_WhenResponseThrowsException() {
        val filePath = mutableListOf<String>()
        filePath.add("/mnt/test")
        val expectedPair = Pair("/mnt/test", "/mnt/test")

        val error = Throwable("Something went wrong")
        val expectedState1 = UploadPhotoState.ShowProgressLoading
        val expectedState2 = UploadPhotoState.HideProgressLoading
        val expectedState3 = UploadPhotoState.ErrorUploadPhoto(error)

        whenever(resourceManager.scaleDownImage(any()))
            .thenReturn(Single.just(expectedPair))

        whenever(repository.uploadPhoto(any()))
            .thenReturn(Single.error(error))

        subject.uploadPhoto(filePath)
        testScheduler.triggerActions()

        argumentCaptor<UploadPhotoState>()
            .run {
                verify(
                    observer,
                    times(3)
                ).onNext(capture())

                Assert.assertEquals(expectedState1, allValues[0])
                Assert.assertEquals(expectedState2, allValues[1])
                Assert.assertEquals(expectedState3, allValues[2])
            }
    }
}
