package com.appetiser.codingappchallenge.features.account.changephone.newphone

import com.appetiser.codingappchallenge.Stubs
import com.appetiser.codingappchallenge.core.BaseViewModelTest
import com.appetiser.codingappchallenge.utils.PhoneNumberHelper
import com.appetiser.module.common.utils.ResourceManager
import com.appetiser.module.data.features.auth.AuthRepository
import com.appetiser.module.data.features.session.SessionRepository
import com.appetiser.module.domain.utils.*
import io.reactivex.Completable
import io.reactivex.Observer
import io.reactivex.Single
import okhttp3.ResponseBody.Companion.toResponseBody
import org.junit.Assert

import org.junit.Before

import org.junit.Test
import org.mockito.Mockito
import retrofit2.HttpException
import retrofit2.Response

class ChangePhoneNewPhoneViewModelTest : BaseViewModelTest() {

    private val authRepository: AuthRepository = mock()
    private val sessionRepository: SessionRepository = mock()
    private val resourceManager: ResourceManager = mock()
    private val phoneNumberHelper: PhoneNumberHelper = mock()
    private val stateObserver: Observer<ChangePhoneNewPhoneState> = mock()

    private lateinit var subject: ChangePhoneNewPhoneViewModel

    @Before
    fun setUp() {
        subject = ChangePhoneNewPhoneViewModel(authRepository, sessionRepository, resourceManager, phoneNumberHelper)
        subject.schedulers = schedulers
        subject.state.subscribe(stateObserver)
    }

    @Test
    fun onPhoneTextChanged_ShouldDisableButton_WhenPhoneNumberIsEmpty() {
        val phoneNumber = ""
        val expectedState = ChangePhoneNewPhoneState.DisableButton

        subject.onPhoneTextChanged(phoneNumber)

        argumentCaptor<ChangePhoneNewPhoneState>()
            .run {
                Mockito
                    .verify(
                        stateObserver,
                        Mockito.times(1)
                    )
                    .onNext(capture())

                Assert.assertEquals(expectedState, value)
            }
    }

    @Test
    fun onPhoneTextChanged_ShouldEnableButton_WhenPhoneNumberIsNotEmpty() {
        val phoneNumber = "+639435542134"
        val expectedState = ChangePhoneNewPhoneState.EnableButton

        subject.onPhoneTextChanged(phoneNumber)

        argumentCaptor<ChangePhoneNewPhoneState>()
            .run {
                Mockito
                    .verify(
                        stateObserver,
                        Mockito.times(1)
                    )
                    .onNext(capture())

                Assert.assertEquals(expectedState, value)
            }
    }

    @Test
    fun changePhone_ShouldNotCallRepository_WhenPhoneNumberIsEmpty() {
        val countryCode = ""
        val countryNameCode = ""
        val phoneNumber = ""

        subject
            .changePhone(
                countryCode,
                countryNameCode,
                phoneNumber
            )

        Mockito.verifyZeroInteractions(authRepository)
    }

    @Test
    fun changePhone_ShouldEmitPhoneNumberExists_WhenPhoneNumberIsAlreadyTaken() {
        val countryCode = "63"
        val countryNameCode = "PH"
        val phoneNumber = "9435542134"
        val errorMessage = "Phone number already taken."
        val verificationToken = "test verification token"

        val expectedState1 = ChangePhoneNewPhoneState.ShowLoading
        val expectedState2 = ChangePhoneNewPhoneState.HideLoading
        val throwable =
            HttpException(
                Response
                    .error<Unit>(
                        422,
                        errorMessage.toResponseBody()
                    )
            )

        whenever(authRepository.requestChangePhone(any(), any()))
            .thenReturn(Completable.error(throwable))
        whenever(phoneNumberHelper.getInternationalFormattedPhoneNumber(any(), any(), any()))
            .thenReturn("+$countryCode$phoneNumber")

        subject.setVerificationToken(verificationToken)
        subject
            .changePhone(
                countryCode,
                countryNameCode,
                phoneNumber
            )
        testScheduler.triggerActions()

        argumentCaptor<ChangePhoneNewPhoneState>()
            .run {
                Mockito
                    .verify(
                        stateObserver,
                        Mockito.times(3)
                    )
                    .onNext(capture())

                Assert.assertEquals(expectedState1, allValues[0])
                Assert.assertEquals(expectedState2, allValues[1])
                Assert
                    .assertTrue(
                        allValues[2] is ChangePhoneNewPhoneState.PhoneNumberError
                    )
            }
    }

    @Test
    fun changePhone_ShouldEmitError_WhenResponseThrowsException() {
        val countryCode = "63"
        val countryNameCode = "PH"
        val phoneNumber = "9435542134"
        val errorMessage = "Test error."
        val verificationToken = "test verification token"

        val expectedState1 = ChangePhoneNewPhoneState.ShowLoading
        val expectedState2 = ChangePhoneNewPhoneState.HideLoading
        val expectedState3 = ChangePhoneNewPhoneState.Error(errorMessage)
        val throwable = Throwable(errorMessage)

        whenever(resourceManager.getString(anyInt()))
            .thenReturn(errorMessage)
        whenever(authRepository.requestChangePhone(any(), any()))
            .thenReturn(Completable.error(throwable))
        whenever(phoneNumberHelper.getInternationalFormattedPhoneNumber(any(), any(), any()))
            .thenReturn("+$countryCode$phoneNumber")

        subject.setVerificationToken(verificationToken)
        subject
            .changePhone(
                countryCode,
                countryNameCode,
                phoneNumber
            )
        testScheduler.triggerActions()

        argumentCaptor<ChangePhoneNewPhoneState>()
            .run {
                Mockito
                    .verify(
                        stateObserver,
                        Mockito.times(3)
                    )
                    .onNext(capture())

                Assert.assertEquals(expectedState1, allValues[0])
                Assert.assertEquals(expectedState2, allValues[1])
                Assert.assertEquals(expectedState3, allValues[2])
            }
    }

    @Test
    fun changePhone_ShouldEmitChangePhoneSuccess_WhenResponseIsSuccessful() {
        val countryCode = "63"
        val countryNameCode = "PH"
        val phoneNumber = "9435542134"
        val formattedPhoneNumbeer = "+$countryCode$phoneNumber"
        val user = Stubs.SESSION_LOGGED_IN
        val verificationToken = "test verification token"

        val expectedState0 = ChangePhoneNewPhoneState.EnableButton
        val expectedState1 = ChangePhoneNewPhoneState.ShowLoading
        val expectedState2 = ChangePhoneNewPhoneState.HideLoading
        val expectedState3 =
            ChangePhoneNewPhoneState
                .ChangePhoneSuccess(
                    formattedPhoneNumbeer,
                    verificationToken
                )

        whenever(sessionRepository.getSession())
            .thenReturn(Single.just(user))
        whenever(authRepository.requestChangePhone(any(), any()))
            .thenReturn(Completable.complete())
        whenever(phoneNumberHelper.getInternationalFormattedPhoneNumber(any(), any(), any()))
            .thenReturn(formattedPhoneNumbeer)

        subject.setVerificationToken(verificationToken)
        subject.onPhoneTextChanged(phoneNumber)

        subject
            .changePhone(
                countryCode,
                countryNameCode,
                phoneNumber
            )
        testScheduler.triggerActions()

        argumentCaptor<ChangePhoneNewPhoneState>()
            .run {
                Mockito
                    .verify(
                        stateObserver,
                        Mockito.times(4)
                    )
                    .onNext(capture())

                Assert.assertEquals(expectedState0, allValues[0])
                Assert.assertEquals(expectedState1, allValues[1])
                Assert.assertEquals(expectedState2, allValues[2])
                Assert.assertEquals(expectedState3, allValues[3])
            }
    }
}
