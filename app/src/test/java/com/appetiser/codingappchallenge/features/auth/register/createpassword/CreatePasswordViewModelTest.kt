package com.appetiser.codingappchallenge.features.auth.register.createpassword

import com.appetiser.codingappchallenge.Stubs
import com.appetiser.codingappchallenge.core.BaseViewModelTest
import com.appetiser.codingappchallenge.utils.PhoneNumberHelper
import com.appetiser.module.common.utils.ResourceManager
import com.appetiser.module.data.features.auth.AuthRepository
import com.appetiser.module.domain.models.session.Session
import com.appetiser.module.domain.utils.any
import com.appetiser.module.domain.utils.argumentCaptor
import com.appetiser.module.domain.utils.mock
import com.appetiser.module.domain.utils.whenever
import io.reactivex.Single
import io.reactivex.observers.TestObserver
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import org.mockito.Mockito.times

class CreatePasswordViewModelTest : BaseViewModelTest() {

    private val deviceId = "device_id"
    private val deviceToken = "sampledevicetoken"

    private val authRepository: AuthRepository = mock()
    private val resourceManager: ResourceManager = mock()
    private val phoneNumberHelper: PhoneNumberHelper = mock()

    private val observer: TestObserver<CreatePasswordState> = mock()

    private lateinit var subject: CreatePasswordViewModel

    @Before
    fun setUp() {
        subject = CreatePasswordViewModel(
            authRepository,
            resourceManager,
            phoneNumberHelper
        )
        subject.schedulers = schedulers
        subject.state.subscribe(observer)
    }

    @Test
    fun savePassword_ShouldEmitStartVerificationCodeScreen_WhenResponseIsSuccessful() {
        val userSession = Stubs.USER_NOT_VERIFIED
        val accessToken = Stubs.VALID_ACCESS_TOKEN
        val expectedState1 =
            CreatePasswordState
                .FillUsername(
                    userSession.email
                )
        val expectedState2 = CreatePasswordState.ShowLoading
        val expectedState3 = CreatePasswordState.HideLoading
        val expectedState4 =
            CreatePasswordState
                .StartVerificationCodeScreen(
                    userSession.email,
                    userSession.phoneNumber
                )
        val response = Session(userSession, accessToken)

        whenever(
            authRepository
                .registerWithEmail(
                    any(),
                    any(),
                    any(),
                    any(),
                    any()
                )
        ).thenReturn(Single.just(response))

        whenever(resourceManager.getDeviceId())
            .thenReturn(deviceId)

        subject
            .setUsername(
                userSession.email,
                userSession.phoneNumber
            )

        subject.savePassword("12345678")
        testScheduler.triggerActions()

        argumentCaptor<CreatePasswordState>()
            .run {
                Mockito
                    .verify(
                        observer,
                        times(4)
                    )
                    .onNext(capture())

                Assert.assertEquals(expectedState1, allValues[0])
                Assert.assertEquals(expectedState2, allValues[1])
                Assert.assertEquals(expectedState3, allValues[2])
                Assert.assertEquals(expectedState4, allValues[3])
            }
    }

    @Test
    fun savePassword_ShouldEmitError_WhenResponseThrowsException() {
        val userSession = Stubs.USER_NOT_VERIFIED
        val errorMessage = "Test error."
        val throwable = Throwable(errorMessage)
        val expectedState1 =
            CreatePasswordState
                .FillUsername(
                    userSession.email
                )
        val expectedState2 = CreatePasswordState.ShowLoading
        val expectedState3 = CreatePasswordState.HideLoading
        val expectedState4 = CreatePasswordState.Error(errorMessage)

        whenever(
            authRepository
                .registerWithEmail(
                    any(),
                    any(),
                    any(),
                    any(),
                    any()
                )
        ).thenReturn(Single.error(throwable))

        subject
            .setUsername(
                userSession.email,
                userSession.phoneNumber
            )
        subject.savePassword("12345678")
        testScheduler.triggerActions()

        argumentCaptor<CreatePasswordState>()
            .run {
                Mockito
                    .verify(
                        observer,
                        times(4)
                    )
                    .onNext(capture())

                Assert.assertEquals(expectedState1, allValues[0])
                Assert.assertEquals(expectedState2, allValues[1])
                Assert.assertEquals(expectedState3, allValues[2])
                Assert.assertEquals(expectedState4, allValues[3])
            }
    }

    @Test
    fun savePassword_ShouldEmitPasswordBelowMinLengthState_WhenPasswordIsBelowMinimumLength() {
        val expectedState = CreatePasswordState.PasswordBelowMinLength

        subject.savePassword("1234")

        argumentCaptor<CreatePasswordState>()
            .run {
                Mockito
                    .verify(
                        observer,
                        times(1)
                    )
                    .onNext(capture())

                Assert.assertEquals(expectedState, value)
            }
    }

    @Test
    fun savePassword_ShouldEmitPasswordExceedsMaxLengthState_WhenPasswordExceedsMaxLength() {
        val expectedState = CreatePasswordState.PasswordExceedsMaxLength

        subject
            .savePassword(
                "1234567812345678123456781234567812345678"
            )

        argumentCaptor<CreatePasswordState>()
            .run {
                Mockito
                    .verify(
                        observer,
                        times(1)
                    )
                    .onNext(capture())

                Assert.assertEquals(expectedState, value)
            }
    }

    @Test
    fun onPasswordTextChange_ShouldEmitDisableButtonState_WhenPasswordIsEmpty() {
        val expectedState = CreatePasswordState.DisableButton

        subject.onPasswordTextChange("")

        argumentCaptor<CreatePasswordState>()
            .run {
                Mockito
                    .verify(
                        observer,
                        times(1)
                    )
                    .onNext(capture())

                Assert.assertEquals(expectedState, value)
            }
    }

    @Test
    fun onPasswordTextChange_ShouldEmitEnableButtonState_WhenPasswordIsNotEmpty() {
        val expectedState = CreatePasswordState.EnableButton

        subject.onPasswordTextChange("12345")

        argumentCaptor<CreatePasswordState>()
            .run {
                Mockito
                    .verify(
                        observer,
                        times(1)
                    )
                    .onNext(capture())

                Assert.assertEquals(expectedState, value)
            }
    }
}
