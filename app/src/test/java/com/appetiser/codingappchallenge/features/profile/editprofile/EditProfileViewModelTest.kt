package com.appetiser.codingappchallenge.features.profile.editprofile

import com.appetiser.codingappchallenge.Stubs
import com.appetiser.codingappchallenge.core.BaseViewModelTest
import com.appetiser.codingappchallenge.utils.DEFAULT_DOB_SUBTRACTION
import com.appetiser.codingappchallenge.utils.DateUtils
import com.appetiser.module.common.utils.ResourceManager
import com.appetiser.module.data.features.session.SessionRepository
import com.appetiser.module.data.features.user.UserRepository
import com.appetiser.module.domain.utils.*
import io.reactivex.Single
import io.reactivex.observers.TestObserver
import org.junit.Assert

import org.junit.Before

import org.junit.Test
import org.mockito.ArgumentCaptor
import org.mockito.Mockito
import org.mockito.Mockito.times
import java.time.LocalDate

class EditProfileViewModelTest : BaseViewModelTest() {

    private val sessionRepository: SessionRepository = mock()
    private val userRepository: UserRepository = mock()
    private val resourceManager: ResourceManager = mock()
    private val stateObserver: TestObserver<EditProfileState> = mock()

    private lateinit var subject: EditProfileViewModel

    private val session = Stubs.SESSION_LOGGED_IN
    private val user = session.user
    private val fullName = "Jose Santos"
    private val description = "This is Jose Santos."

    @Before
    fun setUp() {
        subject = EditProfileViewModel(
            sessionRepository,
            userRepository,
            resourceManager
        )
        subject.schedulers = schedulers
        subject.state.subscribe(stateObserver)
    }

    @Test
    fun saveProfile_ShouldEmitDisableSaveButton_WhenFullNameIsEmpty() {
        val fullName = ""
        val expectedState = EditProfileState.DisableSaveButton

        subject.saveProfile(fullName, description)

        captureState {
            Mockito
                .verify(
                    stateObserver,
                    times(1)
                )
                .onNext(capture())

            Assert.assertEquals(expectedState, value)
        }
    }

    @Test
    fun saveProfile_ShouldEmitDescriptionExceedsMaxLength_WhenDescriptionExceedsMaxLength() {
        val description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
        val expectedState = EditProfileState.DescriptionExceedsMaxLength(PROFILE_DESCRIPTION_MAX_LENGTH)

        whenever(resourceManager.getInteger(anyInt()))
            .thenReturn(PROFILE_DESCRIPTION_MAX_LENGTH)

        subject.saveProfile(fullName, description)

        captureState {
            Mockito
                .verify(
                    stateObserver,
                    times(1)
                )
                .onNext(capture())

            Assert.assertEquals(expectedState, value)
        }
    }

    @Test
    fun saveProfile_ShouldCallUploadPhoto_WhenImageLocalPathIsNotEmpty() {
        val imageLocalPath = "test path"

        whenever(resourceManager.getInteger(anyInt()))
            .thenReturn(PROFILE_DESCRIPTION_MAX_LENGTH)
        whenever(userRepository.updateUser(any()))
            .thenReturn(Single.just(user))
        whenever(userRepository.uploadPhoto(any()))
            .thenReturn(Single.just(user))

        subject.onProfilePhotoChanged(imageLocalPath)
        subject.saveProfile(fullName, description)
        testScheduler.triggerActions()

        Mockito
            .verify(
                userRepository,
                times(1)
            )
            .uploadPhoto(any())
    }

    @Test
    fun saveProfile_ShouldCallNotUploadPhoto_WhenImageLocalPathIsEmpty() {
        val imageLocalPath = ""

        whenever(resourceManager.getInteger(anyInt()))
            .thenReturn(PROFILE_DESCRIPTION_MAX_LENGTH)
        whenever(userRepository.updateUser(any()))
            .thenReturn(Single.just(user))
        whenever(userRepository.uploadPhoto(any()))
            .thenReturn(Single.just(user))

        subject.onProfilePhotoChanged(imageLocalPath)
        subject.saveProfile(fullName, description)

        Mockito
            .verify(
                userRepository,
                times(0)
            )
            .uploadPhoto(any())
    }

    @Test
    fun saveProfile_ShouldEmitUpdateProfileSuccess_WhenResponseIsSuccessful() {
        val expectedState1 = EditProfileState.ShowLoading
        val expectedState2 = EditProfileState.HideLoading
        val expectedState3 = EditProfileState.UpdateProfileSuccess

        whenever(resourceManager.getInteger(anyInt()))
            .thenReturn(PROFILE_DESCRIPTION_MAX_LENGTH)
        whenever(userRepository.updateUser(any()))
            .thenReturn(Single.just(user))

        subject.saveProfile(fullName, description)
        testScheduler.triggerActions()

        captureState {
            Mockito
                .verify(
                    stateObserver,
                    times(3)
                )
                .onNext(capture())

            Assert.assertEquals(expectedState1, allValues[0])
            Assert.assertEquals(expectedState2, allValues[1])
            Assert.assertEquals(expectedState3, allValues[2])
        }
    }

    @Test
    fun saveProfile_ShouldEmitError_IfResponseThrowsException() {
        val errorMessage = "Test error"

        val expectedState1 = EditProfileState.ShowLoading
        val expectedState2 = EditProfileState.HideLoading
        val expectedState3 = EditProfileState.Error(errorMessage)
        val throwable = Throwable(errorMessage)

        whenever(resourceManager.getInteger(anyInt()))
            .thenReturn(PROFILE_DESCRIPTION_MAX_LENGTH)
        whenever(resourceManager.getString(anyInt()))
            .thenReturn(errorMessage)
        whenever(userRepository.updateUser(any()))
            .thenReturn(Single.error(throwable))

        subject.saveProfile(fullName, description)
        testScheduler.triggerActions()

        captureState {
            Mockito
                .verify(
                    stateObserver,
                    times(3)
                )
                .onNext(capture())

            Assert.assertEquals(expectedState1, allValues[0])
            Assert.assertEquals(expectedState2, allValues[1])
            Assert.assertEquals(expectedState3, allValues[2])
        }
    }

    @Test
    fun loadProfile_ShouldEmitPrefillFields() {
        val expectedState =
            EditProfileState.PreFillFields(
                user.avatar.thumbUrl,
                user.fullName,
                user.description
            )

        whenever(sessionRepository.getSession())
            .thenReturn(Single.just(Stubs.SESSION_USER_LOGGED_IN_NO_DOB))

        subject.loadProfile()
        testScheduler.triggerActions()

        captureState {
            Mockito
                .verify(
                    stateObserver,
                    times(1)
                )
                .onNext(capture())

            Assert.assertEquals(expectedState, value)
        }
    }

    @Test
    fun loadProfile_ShouldEmitDisplayDob_WhenBirthdateIsNotEmpty() {
        val parsedBirthDate = DateUtils.parseBirthDate(user.birthDate)

        val expectedState1 =
            EditProfileState.PreFillFields(
                user.avatar.thumbUrl,
                user.fullName,
                user.description
            )
        val expectedState2 =
            EditProfileState.DisplayDob(
                parsedBirthDate.year,
                parsedBirthDate.monthValue,
                parsedBirthDate.dayOfMonth
            )

        whenever(sessionRepository.getSession())
            .thenReturn(Single.just(session))

        subject.loadProfile()
        testScheduler.triggerActions()

        captureState {
            Mockito
                .verify(
                    stateObserver,
                    times(2)
                )
                .onNext(capture())

            Assert.assertEquals(expectedState1, allValues[0])
            Assert.assertEquals(expectedState2, allValues[1])
        }
    }

    @Test
    fun onNameTextChanged_ShouldEmitDisableSaveButton_IfNameIsEmpty() {
        val fullName = ""
        val expectedState = EditProfileState.DisableSaveButton

        subject.onNameTextChanged(fullName)

        captureState {
            Mockito
                .verify(
                    stateObserver,
                    times(1)
                )
                .onNext(capture())

            Assert.assertEquals(expectedState, value)
        }
    }

    @Test
    fun onNameTextChanged_ShouldEmitEnableSaveButton_IfNameIsNotEmpty() {
        val expectedState = EditProfileState.EnableSaveButton

        subject.onNameTextChanged(fullName)

        captureState {
            Mockito
                .verify(
                    stateObserver,
                    times(1)
                )
                .onNext(capture())

            Assert.assertEquals(expectedState, value)
        }
    }

    @Test
    fun onDatePickerClick_ShouldEmitNewlySelectedDob_WhenUserHasSelectedOne() {
        val newYear = 1995
        val newMonth = 8
        val newDayOfMonth = 28

        val expectedState1 =
            EditProfileState.DisplayDob(
                newYear,
                newMonth,
                newDayOfMonth
            )
        val expectedState2 =
            EditProfileState.ShowDatePicker(
                newYear,
                newMonth,
                newDayOfMonth
            )

        subject.onDobSelected(
            newYear,
            newMonth,
            newDayOfMonth
        )

        subject.onDatePickerClick()

        captureState {
            Mockito
                .verify(
                    stateObserver,
                    times(2)
                )
                .onNext(capture())

            Assert.assertEquals(expectedState1, allValues[0])
            Assert.assertEquals(expectedState2, allValues[1])
        }
    }

    @Test
    fun onDatePickerClick_ShouldEmitUserDob_WhenUserHasNotSelectedNewOne() {
        val parsedBirthDate = DateUtils.parseBirthDate(user.birthDate)

        val expectedState =
            EditProfileState.ShowDatePicker(
                parsedBirthDate.year,
                parsedBirthDate.monthValue,
                parsedBirthDate.dayOfMonth
            )

        whenever(sessionRepository.getSession())
            .thenReturn(Single.just(session))

        subject.loadProfile()
        testScheduler.triggerActions()

        subject.onDatePickerClick()

        captureState {
            Mockito
                .verify(
                    stateObserver,
                    times(3)
                )
                .onNext(capture())

            Assert.assertEquals(expectedState, value)
        }
    }

    @Test
    fun onDatePickerClick_ShouldEmitDefaultDob_WhenUserBirthdateIsEmpty() {
        val defaultDob =
            LocalDate
                .now()
                .minusYears(DEFAULT_DOB_SUBTRACTION)

        val expectedState =
            EditProfileState.ShowDatePicker(
                defaultDob.year,
                defaultDob.monthValue,
                defaultDob.dayOfMonth
            )

        whenever(sessionRepository.getSession())
            .thenReturn(Single.just(Stubs.SESSION_USER_LOGGED_IN_NO_DOB))

        subject.loadProfile()
        testScheduler.triggerActions()

        subject.onDatePickerClick()

        captureState {
            Mockito
                .verify(
                    stateObserver,
                    times(2)
                )
                .onNext(capture())

            Assert.assertEquals(expectedState, value)
        }
    }

    @Test
    fun onBackPressed_ShouldEmitShowUnsavedChangesDialog_WhenThereAreUnsavedChanges() {
        val expectedState = EditProfileState.ShowUnsavedChangesDialog

        whenever(sessionRepository.getSession())
            .thenReturn(Single.just(session))

        subject.loadProfile()
        testScheduler.triggerActions()

        subject
            .onBackPressed(
                fullName,
                description
            )

        captureState {
            Mockito
                .verify(
                    stateObserver,
                    times(3)
                )
                .onNext(capture())

            Assert.assertEquals(expectedState, value)
        }
    }

    @Test
    fun onBackPressed_ShouldEmitNavigateUp_WhenThereAreNoUnsavedChanges() {
        val expectedState = EditProfileState.NavigateUp

        whenever(sessionRepository.getSession())
            .thenReturn(Single.just(session))

        subject.loadProfile()
        testScheduler.triggerActions()

        subject
            .onBackPressed(
                user.fullName,
                user.description
            )

        captureState {
            Mockito
                .verify(
                    stateObserver,
                    times(3)
                )
                .onNext(capture())

            Assert.assertEquals(expectedState, value)
        }
    }

    private fun captureState(block: ArgumentCaptor<EditProfileState>.() -> Unit) {
        argumentCaptor<EditProfileState>().run(block)
    }
}
