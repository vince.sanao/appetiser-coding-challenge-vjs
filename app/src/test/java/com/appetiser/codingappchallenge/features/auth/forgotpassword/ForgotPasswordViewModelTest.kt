package com.appetiser.codingappchallenge.features.auth.forgotpassword

import com.appetiser.codingappchallenge.core.BaseViewModelTest
import com.appetiser.module.data.features.auth.AuthRepository
import com.appetiser.module.domain.utils.any
import com.appetiser.module.domain.utils.argumentCaptor
import com.appetiser.module.domain.utils.mock
import com.appetiser.module.domain.utils.whenever
import io.reactivex.Single
import io.reactivex.observers.TestObserver
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import org.mockito.Mockito.times

class ForgotPasswordViewModelTest : BaseViewModelTest() {

    private lateinit var subject: ForgotPasswordViewModel

    private val repository: AuthRepository = mock()
    private val observer: TestObserver<ForgotPasswordState> = mock()

    @Before
    fun setup() {
        subject = ForgotPasswordViewModel(repository)
        subject.schedulers = schedulers
        subject.state.subscribe(observer)
    }

    @Test
    fun setUsername_ShouldEmitGetUserNameState_WhenUsernameIsNotEmpty() {
        val username = "test@appetiser.com.au"
        val expected = ForgotPasswordState.GetUsername(username)

        subject.setUsername(username)

        argumentCaptor<ForgotPasswordState>()
            .run {
                Mockito
                    .verify(
                        observer,
                        times(1)
                    )
                    .onNext(capture())

                Assert.assertEquals(expected, value)
            }
    }

    @Test
    fun forgotPassword_ShouldEmitSuccess_WhenResponseIsSuccessful() {
        val username = "test@appetiser.com.au"
        val expectedState1 = ForgotPasswordState.ShowProgressLoading
        val expectedState2 = ForgotPasswordState.HideProgressLoading
        val expectedState3 = ForgotPasswordState.Success(username)

        whenever(repository.forgotPassword(any()))
            .thenReturn(Single.just(true))

        subject.forgotPassword(username)
        testScheduler.triggerActions()

        argumentCaptor<ForgotPasswordState>()
            .run {
                Mockito
                    .verify(
                        observer,
                        times(3)
                    )
                    .onNext(capture())

                Assert.assertEquals(expectedState1, allValues[0])
                Assert.assertEquals(expectedState2, allValues[1])
                Assert.assertEquals(expectedState3, allValues[2])
            }
    }

    @Test
    fun forgotPassword_ShouldEmitError_WhenResponseThrowsException() {
        val username = "test@appetiser.com.au"
        val throwable = Throwable("error message.")
        val expectedState1 = ForgotPasswordState.ShowProgressLoading
        val expectedState2 = ForgotPasswordState.HideProgressLoading
        val expectedState3 = ForgotPasswordState.Error(throwable)

        whenever(repository.forgotPassword(any()))
            .thenReturn(Single.error(throwable))

        subject.forgotPassword(username)
        testScheduler.triggerActions()

        argumentCaptor<ForgotPasswordState>()
            .run {
                Mockito
                    .verify(
                        observer,
                        times(3)
                    )
                    .onNext(capture())

                Assert.assertEquals(expectedState1, allValues[0])
                Assert.assertEquals(expectedState2, allValues[1])
                Assert.assertEquals(expectedState3, allValues[2])
            }
    }
}
