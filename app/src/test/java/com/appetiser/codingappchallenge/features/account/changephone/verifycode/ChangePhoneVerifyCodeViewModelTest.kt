package com.appetiser.codingappchallenge.features.account.changephone.verifycode

import com.appetiser.codingappchallenge.core.BaseViewModelTest
import com.appetiser.codingappchallenge.utils.COUNTDOWN_MAX_TIMER
import com.appetiser.module.common.utils.ResourceManager
import com.appetiser.module.data.features.auth.AuthRepository
import com.appetiser.module.domain.utils.*
import io.reactivex.Completable
import io.reactivex.Observer
import okhttp3.ResponseBody.Companion.toResponseBody
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import retrofit2.HttpException
import retrofit2.Response

class ChangePhoneVerifyCodeViewModelTest : BaseViewModelTest() {

    private val authRepository: AuthRepository = mock()
    private val resourceManager: ResourceManager = mock()
    private val stateObserver: Observer<ChangePhoneVerifyCodeState> = mock()
    private val mockPhoneNumber = "+639435501234"
    private val countDown = COUNTDOWN_MAX_TIMER.toLong()

    private lateinit var subject: ChangePhoneVerifyCodeViewModel

    @Before
    fun setUp() {
        subject = ChangePhoneVerifyCodeViewModel(authRepository, resourceManager)
        subject.schedulers = schedulers
        subject.state.subscribe(stateObserver)
    }

    @Test
    fun onCodeTextChanged_ShouldNotCallRepository_WhenCodeLengthIsLessThanFiveDigits() {
        val code = "123"

        setPhoneAndVerificationToken()

        subject.onCodeTextChanged(code)

        Mockito
            .verifyZeroInteractions(
                authRepository
            )
    }

    @Test
    fun onCodeTextChanged_ShouldCallRepository_WhenCodeLengthReachesFiveDigits() {
        val code = "12345"

        setPhoneAndVerificationToken()

        whenever(authRepository.verifyChangePhone(any(), any()))
            .thenReturn(Completable.complete())

        subject.onCodeTextChanged(code)

        Mockito
            .verify(
                authRepository,
                Mockito.times(1)
            )
            .verifyChangePhone(any(), any())
    }

    @Test
    fun onCodeTextChanged_ShouldEmitInvalidVerificationCode_WhenVerificationCodeIsInvalid() {
        val code = "12345"
        val expectedState1 = ChangePhoneVerifyCodeState.DisplayPhone(mockPhoneNumber)
        val expectedState2 = ChangePhoneVerifyCodeState.HideKeyboard
        val expectedState3 = ChangePhoneVerifyCodeState.ShowLoading
        val expectedState4 = ChangePhoneVerifyCodeState.HideLoading
        val throwable =
            HttpException(
                Response
                    .error<Unit>(
                        400,
                        "Invalid verification code.".toResponseBody()
                    )
            )

        setPhoneAndVerificationToken()

        whenever(authRepository.verifyChangePhone(any(), any()))
            .thenReturn(Completable.error(throwable))

        subject.onCodeTextChanged(code)
        testScheduler.triggerActions()

        argumentCaptor<ChangePhoneVerifyCodeState>()
            .run {
                Mockito
                    .verify(
                        stateObserver,
                        Mockito.times(5)
                    )
                    .onNext(capture())

                Assert.assertEquals(expectedState1, allValues[0])
                Assert.assertEquals(expectedState2, allValues[1])
                Assert.assertEquals(expectedState3, allValues[2])
                Assert.assertEquals(expectedState4, allValues[3])
                Assert
                    .assertTrue(
                        allValues[4] is ChangePhoneVerifyCodeState.InvalidVerificationCode
                    )
            }
    }

    @Test
    fun onCodeTextChanged_ShouldEmitError_WhenResponseThrowsException() {
        val code = "12345"
        val errorMessage = "Test error."
        val expectedState1 = ChangePhoneVerifyCodeState.DisplayPhone(mockPhoneNumber)
        val expectedState2 = ChangePhoneVerifyCodeState.HideKeyboard
        val expectedState3 = ChangePhoneVerifyCodeState.ShowLoading
        val expectedState4 = ChangePhoneVerifyCodeState.HideLoading
        val expectedState5 = ChangePhoneVerifyCodeState.Error(errorMessage)
        val throwable = Throwable(errorMessage)

        setPhoneAndVerificationToken()

        whenever(authRepository.verifyChangePhone(any(), any()))
            .thenReturn(Completable.error(throwable))
        whenever(resourceManager.getString(anyInt()))
            .thenReturn(errorMessage)

        subject.onCodeTextChanged(code)
        testScheduler.triggerActions()

        argumentCaptor<ChangePhoneVerifyCodeState>()
            .run {
                Mockito
                    .verify(
                        stateObserver,
                        Mockito.times(5)
                    )
                    .onNext(capture())

                Assert.assertEquals(expectedState1, allValues[0])
                Assert.assertEquals(expectedState2, allValues[1])
                Assert.assertEquals(expectedState3, allValues[2])
                Assert.assertEquals(expectedState4, allValues[3])
                Assert.assertEquals(expectedState5, allValues[4])
            }
    }

    @Test
    fun onCodeTextChanged_ShouldEmitVerificationSuccess_WhenResponseIsSuccessful() {
        val code = "12345"
        val expectedState1 = ChangePhoneVerifyCodeState.DisplayPhone(mockPhoneNumber)
        val expectedState2 = ChangePhoneVerifyCodeState.HideKeyboard
        val expectedState3 = ChangePhoneVerifyCodeState.ShowLoading
        val expectedState4 = ChangePhoneVerifyCodeState.HideLoading
        val expectedState5 = ChangePhoneVerifyCodeState.VerificationSuccess

        setPhoneAndVerificationToken()

        whenever(authRepository.verifyChangePhone(any(), any()))
            .thenReturn(Completable.complete())

        subject.onCodeTextChanged(code)
        testScheduler.triggerActions()

        argumentCaptor<ChangePhoneVerifyCodeState>()
            .run {
                Mockito
                    .verify(
                        stateObserver,
                        Mockito.times(5)
                    )
                    .onNext(capture())

                Assert.assertEquals(expectedState1, allValues[0])
                Assert.assertEquals(expectedState2, allValues[1])
                Assert.assertEquals(expectedState3, allValues[2])
                Assert.assertEquals(expectedState4, allValues[3])
                Assert.assertEquals(expectedState5, allValues[4])
            }
    }

    @Test
    fun resendCode_ShouldEmitError_WhenResponseThrowsException() {
        val errorMessage = "Test error."
        val expectedState1 = ChangePhoneVerifyCodeState.DisplayPhone(mockPhoneNumber)
        val expectedState2 = ChangePhoneVerifyCodeState.ResendTimer(countDown)
        val expectedState3 = ChangePhoneVerifyCodeState.ShowResendProgressLoading
        val expectedState4 = ChangePhoneVerifyCodeState.Error(errorMessage)
        val throwable = Throwable(errorMessage)

        setPhoneAndVerificationToken()

        whenever(authRepository.requestChangePhone(any(), any()))
            .thenReturn(Completable.error(throwable))
        whenever(resourceManager.getString(anyInt()))
            .thenReturn(errorMessage)

        subject.resendCode()
        testScheduler.triggerActions()

        argumentCaptor<ChangePhoneVerifyCodeState>()
            .run {
                Mockito
                    .verify(
                        stateObserver,
                        Mockito.times(7)
                    )
                    .onNext(capture())

                Assert.assertEquals(expectedState1, allValues[0])
                Assert.assertEquals(expectedState2, allValues[1])
                Assert.assertEquals(expectedState3, allValues[2])
                Assert.assertEquals(expectedState4, allValues[6])
            }
    }

    @Test
    fun resendCode_ShouldEmitResendCodeSuccess_WhenResponseIsSuccessful() {
        val expectedState1 = ChangePhoneVerifyCodeState.DisplayPhone(mockPhoneNumber)
        val expectedState2 = ChangePhoneVerifyCodeState.ResendTimer(countDown)
        val expectedState3 = ChangePhoneVerifyCodeState.ShowResendProgressLoading
        val expectedState4 = ChangePhoneVerifyCodeState.ResendCodeSuccess

        setPhoneAndVerificationToken()

        whenever(authRepository.requestChangePhone(any(), any()))
            .thenReturn(Completable.complete())

        subject.resendCode()
        testScheduler.triggerActions()

        argumentCaptor<ChangePhoneVerifyCodeState>()
            .run {
                Mockito
                    .verify(
                        stateObserver,
                        Mockito.times(5)
                    )
                    .onNext(capture())

                Assert.assertEquals(expectedState1, allValues[0])
                Assert.assertEquals(expectedState2, allValues[1])
                Assert.assertEquals(expectedState3, allValues[2])
                Assert.assertEquals(expectedState4, allValues[4])
            }
    }

    private fun setPhoneAndVerificationToken() {
        val verificationToken = "test verification token"

        subject
            .setPhoneAndVerificationToken(
                mockPhoneNumber,
                verificationToken
            )
    }
}
