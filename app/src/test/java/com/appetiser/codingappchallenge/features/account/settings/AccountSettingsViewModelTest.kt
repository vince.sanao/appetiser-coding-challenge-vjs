package com.appetiser.codingappchallenge.features.account.settings
import com.appetiser.codingappchallenge.core.BaseViewModelTest
import com.appetiser.module.common.utils.ResourceManager
import com.appetiser.module.data.features.auth.AuthRepository
import com.appetiser.module.data.features.session.SessionRepository
import com.appetiser.module.data.features.user.UserRepository
import com.appetiser.module.domain.utils.*
import io.reactivex.Completable
import io.reactivex.Observer
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import org.mockito.Mockito.times

class AccountSettingsViewModelTest : BaseViewModelTest() {

    private val authRepository: AuthRepository = mock()
    private val userRepository: UserRepository = mock()
    private val sessionRepository: SessionRepository = mock()
    private val resourceManager: ResourceManager = mock()

    private val stateObserver: Observer<AccountSettingsState> = mock()

    private lateinit var subject: AccountSettingsViewModel

    @Before
    fun setUp() {
        subject = AccountSettingsViewModel(
            authRepository,
            userRepository,
            sessionRepository,
            resourceManager
        )
        subject.schedulers = schedulers
        subject.state.subscribe(stateObserver)
    }

    @Test
    fun logout_ShouldEmitError_WhenResponseThrowsException() {
        val errorMessage = "Test error."
        val mockToken = "mock token"

        val expectedState1 = AccountSettingsState.ShowLoading
        val expectedState2 = AccountSettingsState.HideLoading
        val expectedState3 = AccountSettingsState.Error(errorMessage)
        val throwable = Throwable(errorMessage)

        whenever(authRepository.logout())
            .thenReturn(Completable.error(throwable))
        whenever(resourceManager.getString(anyInt()))
            .thenReturn(errorMessage)

        subject
            .logout()
        testScheduler.triggerActions()

        argumentCaptor<AccountSettingsState>()
            .run {
                Mockito
                    .verify(
                        stateObserver,
                        times(3)
                    )
                    .onNext(capture())

                Assert.assertEquals(expectedState1, allValues[0])
                Assert.assertEquals(expectedState2, allValues[1])
                Assert.assertEquals(expectedState3, allValues[2])
            }
    }

    @Test
    fun logout_ShouldEmitNavigateToWalkthroughScreen_WhenResponseIsSuccessful() {
        val mockToken = "mock token"

        val expectedState1 = AccountSettingsState.ShowLoading
        val expectedState2 = AccountSettingsState.HideLoading
        val expectedState3 = AccountSettingsState.NavigateToWalkthroughScreen

        whenever(authRepository.logout())
            .thenReturn(Completable.complete())

        subject
            .logout()
        testScheduler.triggerActions()

        argumentCaptor<AccountSettingsState>()
            .run {
                Mockito
                    .verify(
                        stateObserver,
                        times(3)
                    )
                    .onNext(capture())

                Assert.assertEquals(expectedState1, allValues[0])
                Assert.assertEquals(expectedState2, allValues[1])
                Assert.assertEquals(expectedState3, allValues[2])
            }
    }
}
