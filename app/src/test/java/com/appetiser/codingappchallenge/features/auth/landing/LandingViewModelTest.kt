package com.appetiser.codingappchallenge.features.auth.landing

import com.appetiser.codingappchallenge.Stubs
import com.appetiser.codingappchallenge.core.BaseViewModelTest
import com.appetiser.codingappchallenge.utils.PhoneNumberHelper
import com.appetiser.module.common.utils.ResourceManager
import com.appetiser.module.data.features.auth.AuthRepository
import com.appetiser.module.domain.utils.*
import com.google.gson.Gson
import io.reactivex.Single
import io.reactivex.observers.TestObserver
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.times
import org.mockito.Mockito.verify
import org.mockito.internal.matchers.apachecommons.ReflectionEquals

class LandingViewModelTest : BaseViewModelTest() {

    private lateinit var subject: LandingViewModel

    private val authRepository: AuthRepository = mock()
    private val resourceManager: ResourceManager = mock()
    private val phoneNumberHelper: PhoneNumberHelper = mock()
    private val observer: TestObserver<LandingState> = mock()

    private val sessionLoggedIn = Stubs.SESSION_LOGGED_IN
    private val sessionLoggedInButNotOnboarded = Stubs.SESSION_USER_LOGGED_IN_NO_FULLNAME

    @Before
    fun setUp() {
        subject =
            LandingViewModel(
                authRepository,
                phoneNumberHelper,
                resourceManager,
                Gson()
            )
        subject.schedulers = schedulers
        subject.state.subscribe(observer)
    }

    @Test
    fun onFacebookLogin_ShouldReturnLoginSuccess_WhenUserOnboardingDetailsAreFilled() {
        val accessToken = "access_token"
        val expectedLoginType = LandingViewModel.FACEBOOK
        val expectedState1 = LandingState.ShowLoading(expectedLoginType)
        val expectedState2 = LandingState.HideLoading
        val expectedState3 = LandingState.SocialLoginSuccess

        whenever(authRepository.socialLogin(any(), any()))
            .thenReturn(Single.just(sessionLoggedIn))

        subject.onFacebookLogin(accessToken)
        testScheduler.triggerActions()

        val accessTokenProviderCaptor = argumentCaptor<String>()

        accessTokenProviderCaptor
            .run {
                verify(authRepository, times(1))
                    .socialLogin(
                        any(),
                        capture(this)
                    )

                Assert.assertEquals(expectedLoginType, value)
            }

        argumentCaptor<LandingState>()
            .run {
                verify(observer, times(3)).onNext(capture())

                Assert
                    .assertTrue(ReflectionEquals(expectedState1).matches(allValues[0]))
                Assert.assertEquals(expectedState2, allValues[1])
                Assert.assertEquals(expectedState3, allValues[2])
            }
    }

    @Test
    fun onFacebookLogin_ShouldReturnLoginSuccessButNotOnboarded_WhenUserOnboardingDetailsAreNotFilled() {
        val accessToken = "access_token"
        val expectedLoginType = LandingViewModel.FACEBOOK
        val expectedState1 = LandingState.ShowLoading(expectedLoginType)
        val expectedState2 = LandingState.HideLoading
        val expectedState3 = LandingState.SocialLoginSuccessButNotOnboarded

        whenever(authRepository.socialLogin(any(), any()))
            .thenReturn(Single.just(sessionLoggedInButNotOnboarded))

        subject.onFacebookLogin(accessToken)
        testScheduler.triggerActions()

        argumentCaptor<String>()
            .run {
                verify(authRepository, times(1))
                    .socialLogin(
                        any(),
                        capture(this)
                    )

                Assert.assertEquals(expectedLoginType, value)
            }

        argumentCaptor<LandingState>()
            .run {
                verify(observer, times(3)).onNext(capture())

                Assert
                    .assertTrue(ReflectionEquals(expectedState1).matches(allValues[0]))
                Assert.assertEquals(expectedState2, allValues[1])
                Assert.assertEquals(expectedState3, allValues[2])
            }
    }

    @Test
    fun onFacebookLogin_ShouldReturnError_WhenErrorOccurs() {
        val accessToken = "access_token"
        val expectedLoginType = LandingViewModel.FACEBOOK
        val exception = Exception("Mock exception")
        val expectedState1 = LandingState.ShowLoading(expectedLoginType)
        val expectedState2 = LandingState.HideLoading
        val expectedState3 = LandingState.Error(exception)

        whenever(authRepository.socialLogin(any(), any()))
            .thenReturn(Single.error(exception))

        subject.onFacebookLogin(accessToken)
        testScheduler.triggerActions()

        val accessTokenProviderCaptor = argumentCaptor<String>()

        accessTokenProviderCaptor
            .run {
                verify(authRepository, times(1))
                    .socialLogin(
                        any(),
                        capture(this)
                    )

                Assert.assertEquals(expectedLoginType, value)
            }

        argumentCaptor<LandingState>()
            .run {
                verify(observer, times(3)).onNext(capture())

                Assert
                    .assertTrue(ReflectionEquals(expectedState1).matches(allValues[0]))
                Assert.assertEquals(expectedState2, allValues[1])
                Assert
                    .assertTrue(ReflectionEquals(expectedState3).matches(allValues[2]))
            }
    }

    @Test
    fun onGoogleSignIn_ShouldReturnLoginSuccess_WhenUserOnboardingDetailsAreFilled() {
        val accessToken = "access_token"
        val expectedLoginType = LandingViewModel.GOOGLE
        val expectedState1 = LandingState.ShowLoading(expectedLoginType)
        val expectedState2 = LandingState.HideLoading
        val expectedState3 = LandingState.SocialLoginSuccess

        whenever(authRepository.socialLogin(any(), any()))
            .thenReturn(Single.just(sessionLoggedIn))

        subject.onGoogleSignIn(accessToken)
        testScheduler.triggerActions()

        val accessTokenProviderCaptor = argumentCaptor<String>()

        accessTokenProviderCaptor
            .run {
                verify(authRepository, times(1))
                    .socialLogin(
                        any(),
                        capture(this)
                    )

                Assert.assertEquals(expectedLoginType, value)
            }

        argumentCaptor<LandingState>()
            .run {
                verify(observer, times(3)).onNext(capture())

                Assert
                    .assertTrue(ReflectionEquals(expectedState1).matches(allValues[0]))
                Assert.assertEquals(expectedState2, allValues[1])
                Assert.assertEquals(expectedState3, allValues[2])
            }
    }

    @Test
    fun onGoogleSignIn_ShouldReturnLoginSuccessButNotOnboarded_WhenUserOnboardingDetailsAreNotFilled() {
        val accessToken = "access_token"
        val expectedLoginType = LandingViewModel.GOOGLE
        val expectedState1 = LandingState.ShowLoading(expectedLoginType)
        val expectedState2 = LandingState.HideLoading
        val expectedState3 = LandingState.SocialLoginSuccessButNotOnboarded

        whenever(authRepository.socialLogin(any(), any()))
            .thenReturn(Single.just(sessionLoggedInButNotOnboarded))

        subject.onGoogleSignIn(accessToken)
        testScheduler.triggerActions()

        argumentCaptor<String>()
            .run {
                verify(authRepository, times(1))
                    .socialLogin(
                        any(),
                        capture(this)
                    )

                Assert.assertEquals(expectedLoginType, value)
            }

        argumentCaptor<LandingState>()
            .run {
                verify(observer, times(3)).onNext(capture())

                Assert
                    .assertTrue(ReflectionEquals(expectedState1).matches(allValues[0]))
                Assert.assertEquals(expectedState2, allValues[1])
                Assert.assertEquals(expectedState3, allValues[2])
            }
    }

    @Test
    fun onGoogleSignIn_ShouldReturnError_WhenErrorOccurs() {
        val accessToken = "access_token"
        val expectedLoginType = LandingViewModel.GOOGLE
        val exception = Exception("Mock exception")
        val expectedState1 = LandingState.ShowLoading(expectedLoginType)
        val expectedState2 = LandingState.HideLoading
        val expectedState3 = LandingState.Error(exception)

        whenever(authRepository.socialLogin(any(), any()))
            .thenReturn(Single.error(exception))

        subject.onGoogleSignIn(accessToken)
        testScheduler.triggerActions()

        val accessTokenProviderCaptor = argumentCaptor<String>()

        accessTokenProviderCaptor
            .run {
                verify(authRepository, times(1))
                    .socialLogin(
                        any(),
                        capture(this)
                    )

                Assert.assertEquals(expectedLoginType, value)
            }

        argumentCaptor<LandingState>()
            .run {
                verify(observer, times(3)).onNext(capture())

                Assert
                    .assertTrue(ReflectionEquals(expectedState1).matches(allValues[0]))
                Assert.assertEquals(expectedState2, allValues[1])
                Assert
                    .assertTrue(ReflectionEquals(expectedState3).matches(allValues[2]))
            }
    }
}
