package com.appetiser.codingappchallenge.features.splash

import android.os.Bundle
import com.appetiser.codingappchallenge.Stubs
import com.appetiser.codingappchallenge.core.BaseViewModelTest
import com.appetiser.module.data.features.miscellaneous.MiscellaneousRepository
import com.appetiser.module.data.features.session.SessionRepository
import com.appetiser.module.domain.models.session.Session
import com.appetiser.module.domain.models.token.AccessToken
import com.appetiser.module.domain.models.user.User
import com.appetiser.module.domain.utils.any
import com.appetiser.module.domain.utils.argumentCaptor
import com.appetiser.module.domain.utils.mock
import com.appetiser.module.domain.utils.whenever
import io.reactivex.Single
import io.reactivex.observers.TestObserver
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito

class SplashViewModelTest : BaseViewModelTest() {

    private val sessionRepository: SessionRepository = mock()
    private val miscellaneousRepository: MiscellaneousRepository = mock()
    private val mockBundle: Bundle = mock()
    private val observer: TestObserver<SplashState> = mock()

    private lateinit var subject: SplashViewModel

    @Before
    fun setUp() {
        subject = SplashViewModel(sessionRepository, miscellaneousRepository)
        subject.schedulers = schedulers
        subject.state.subscribe(observer)
    }

    @Test
    fun isFirstTimeUiCreate_ShouldEmitLoggedInState_WhenUserSessionExistsAndOnboarded() {
        val expected = SplashState.UserIsLoggedIn

        whenever(miscellaneousRepository.versionCheck(any()))
            .thenReturn(Single.just(Stubs.UPDATE_GATE_EMPTY))
        whenever(sessionRepository.getSession())
            .thenReturn(Single.just(Stubs.SESSION_LOGGED_IN))

        subject.isFirstTimeUiCreate(mockBundle)

        testScheduler.triggerActions()

        Mockito
            .verify(
                sessionRepository,
                Mockito.times(1)
            )
            .getSession()

        argumentCaptor<SplashState>()
            .run {
                Mockito
                    .verify(
                        observer,
                        Mockito.times(1)
                    )
                    .onNext(capture())

                Assert.assertEquals(expected, value)
            }
    }

    @Test
    fun isFirstTimeUiCreate_ShouldEmitNoFullNameState_WhenUserSessionExistsButHasNoFullName() {
        val expected = SplashState.NoFullName

        whenever(miscellaneousRepository.versionCheck(any()))
            .thenReturn(Single.just(Stubs.UPDATE_GATE_EMPTY))
        whenever(sessionRepository.getSession())
            .thenReturn(Single.just(Stubs.SESSION_USER_LOGGED_IN_NO_FULLNAME))

        subject.isFirstTimeUiCreate(mockBundle)

        testScheduler.triggerActions()

        Mockito
            .verify(
                sessionRepository,
                Mockito.times(1)
            )
            .getSession()

        argumentCaptor<SplashState>()
            .run {
                Mockito
                    .verify(
                        observer,
                        Mockito.times(1)
                    )
                    .onNext(capture())

                Assert.assertEquals(expected, value)
            }
    }

    @Test
    fun isFirstTimeUiCreate_ShouldEmitNoProfilePhotoState_WhenUserSessionExistsButHasNoProfilePhoto() {
        val expected = SplashState.NoProfilePhoto

        whenever(miscellaneousRepository.versionCheck(any()))
            .thenReturn(Single.just(Stubs.UPDATE_GATE_EMPTY))
        whenever(sessionRepository.getSession())
            .thenReturn(Single.just(Stubs.SESSION_USER_LOGGED_IN_NO_PROFILE_PHOTO))

        subject.isFirstTimeUiCreate(mockBundle)

        testScheduler.triggerActions()

        Mockito
            .verify(
                sessionRepository,
                Mockito.times(1)
            )
            .getSession()

        argumentCaptor<SplashState>()
            .run {
                Mockito
                    .verify(
                        observer,
                        Mockito.times(1)
                    )
                    .onNext(capture())

                Assert.assertEquals(expected, value)
            }
    }

    @Test
    fun isFirstTimeUiCreate_ShouldEmitUserIsNotLoggedIn_WhenUserSessionDoesNotExist() {
        val expected = SplashState.UserIsNotLoggedIn
        val emptySession = Session(User.empty(), AccessToken())

        whenever(miscellaneousRepository.versionCheck(any()))
            .thenReturn(Single.just(Stubs.UPDATE_GATE_EMPTY))
        whenever(sessionRepository.getSession())
            .thenReturn(Single.just(emptySession))

        subject.isFirstTimeUiCreate(mockBundle)

        testScheduler.triggerActions()

        Mockito
            .verify(
                sessionRepository,
                Mockito.times(1)
            )
            .getSession()

        argumentCaptor<SplashState>()
            .run {
                Mockito
                    .verify(
                        observer,
                        Mockito.times(1)
                    )
                    .onNext(capture())

                Assert.assertEquals(expected, value)
            }
    }

    @Test
    fun isFirstTimeUiCreate_ShouldEmitUserIsNotLoggedIn_WhenUserSessionExistsButNotVerified() {
        val session = Stubs.SESSION_NOT_VERIFIED
        val expected = SplashState.UserIsLoggedInButNotVerified(
            session.user.email,
            session.user.phoneNumber
        )

        whenever(miscellaneousRepository.versionCheck(any()))
            .thenReturn(Single.just(Stubs.UPDATE_GATE_EMPTY))
        whenever(sessionRepository.getSession())
            .thenReturn(Single.just(session))

        subject.isFirstTimeUiCreate(mockBundle)

        testScheduler.triggerActions()

        Mockito
            .verify(
                sessionRepository,
                Mockito.times(1)
            )
            .getSession()

        argumentCaptor<SplashState>()
            .run {
                Mockito
                    .verify(
                        observer,
                        Mockito.times(1)
                    )
                    .onNext(capture())

                Assert.assertEquals(expected, value)
            }
    }
}
