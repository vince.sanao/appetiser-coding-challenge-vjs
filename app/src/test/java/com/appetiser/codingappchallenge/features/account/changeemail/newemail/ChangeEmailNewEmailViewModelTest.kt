package com.appetiser.codingappchallenge.features.account.changeemail.newemail

import com.appetiser.codingappchallenge.Stubs.SESSION_LOGGED_IN
import com.appetiser.codingappchallenge.core.BaseViewModelTest
import com.appetiser.module.common.utils.ResourceManager
import com.appetiser.module.data.features.auth.AuthRepository
import com.appetiser.module.data.features.session.SessionRepository
import com.appetiser.module.domain.utils.*
import io.reactivex.Completable
import io.reactivex.Observer
import io.reactivex.Single
import okhttp3.ResponseBody.Companion.toResponseBody
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import org.mockito.Mockito.times
import retrofit2.HttpException
import retrofit2.Response

class ChangeEmailNewEmailViewModelTest : BaseViewModelTest() {

    private val authRepository: AuthRepository = mock()
    private val sessionRepository: SessionRepository = mock()
    private val resourceManager: ResourceManager = mock()
    private val stateObserver: Observer<ChangeEmailNewEmailState> = mock()

    private lateinit var subject: ChangeEmailNewEmailViewModel

    @Before
    fun setUp() {
        subject = ChangeEmailNewEmailViewModel(authRepository, sessionRepository, resourceManager)
        subject.schedulers = schedulers
        subject.state.subscribe(stateObserver)
    }

    @Test
    fun onEmailTextChanged_ShouldDisableButton_WhenEmailIsEmpty() {
        val email = ""
        val expectedState = ChangeEmailNewEmailState.DisableButton

        subject.onEmailTextChanged(email)

        argumentCaptor<ChangeEmailNewEmailState>()
            .run {
                Mockito
                    .verify(
                        stateObserver,
                        times(1)
                    )
                    .onNext(capture())

                Assert.assertEquals(expectedState, value)
            }
    }

    @Test
    fun onEmailTextChanged_ShouldEnableButton_WhenEmailIsNotEmpty() {
        val email = "jermaine.dilao@appetiser.com.au"
        val expectedState = ChangeEmailNewEmailState.EnableButton

        subject.onEmailTextChanged(email)

        argumentCaptor<ChangeEmailNewEmailState>()
            .run {
                Mockito
                    .verify(
                        stateObserver,
                        times(1)
                    )
                    .onNext(capture())

                Assert.assertEquals(expectedState, value)
            }
    }

    @Test
    fun changeEmail_ShouldNotCallRepository_WhenEmailIsEmpty() {
        val email = ""

        subject.changeEmail(email)

        Mockito.verifyZeroInteractions(authRepository)
    }

    @Test
    fun changeEmail_ShouldEmitInvalidEmail_WhenEmailIsInvalid() {
        val email = "jermaine.dilao"
        val expectedState = ChangeEmailNewEmailState.InvalidEmail

        whenever(resourceManager.validateEmail(any()))
            .thenReturn(false)
        subject.changeEmail(email)

        argumentCaptor<ChangeEmailNewEmailState>()
            .run {
                Mockito
                    .verify(
                        stateObserver,
                        times(1)
                    )
                    .onNext(capture())

                Assert.assertEquals(expectedState, value)
            }
    }

    @Test
    fun changeEmail_ShouldEmitEmailAlreadyTaken_WhenEmailIsAlreadyTaken() {
        val email = "jermaine.dilao@appetiser.com.au"
        val errorMessage = "Email already taken."
        val verificationToken = "test verification token"

        val expectedState1 = ChangeEmailNewEmailState.ShowLoading
        val expectedState2 = ChangeEmailNewEmailState.HideLoading
        val throwable =
            HttpException(
                Response
                    .error<Unit>(
                        422,
                        errorMessage.toResponseBody()
                    )
            )

        whenever(resourceManager.validateEmail(any()))
            .thenReturn(true)
        whenever(authRepository.requestChangeEmail(any(), any()))
            .thenReturn(Completable.error(throwable))

        subject.setVerificationToken(verificationToken)
        subject.changeEmail(email)
        testScheduler.triggerActions()

        argumentCaptor<ChangeEmailNewEmailState>()
            .run {
                Mockito
                    .verify(
                        stateObserver,
                        times(3)
                    )
                    .onNext(capture())

                Assert.assertEquals(expectedState1, allValues[0])
                Assert.assertEquals(expectedState2, allValues[1])
                Assert
                    .assertTrue(
                        allValues[2] is ChangeEmailNewEmailState.EmailAlreadyTaken
                    )
            }
    }

    @Test
    fun changeEmail_ShouldEmitError_WhenResponseThrowsException() {
        val email = "jermaine.dilao@appetiser.com.au"
        val errorMessage = "Test error."
        val verificationToken = "test verification token"

        val expectedState1 = ChangeEmailNewEmailState.ShowLoading
        val expectedState2 = ChangeEmailNewEmailState.HideLoading
        val expectedState3 = ChangeEmailNewEmailState.Error(errorMessage)
        val throwable = Throwable(errorMessage)

        whenever(resourceManager.validateEmail(any()))
            .thenReturn(true)
        whenever(resourceManager.getString(anyInt()))
            .thenReturn(errorMessage)
        whenever(authRepository.requestChangeEmail(any(), any()))
            .thenReturn(Completable.error(throwable))

        subject.setVerificationToken(verificationToken)
        subject.changeEmail(email)
        testScheduler.triggerActions()

        argumentCaptor<ChangeEmailNewEmailState>()
            .run {
                Mockito
                    .verify(
                        stateObserver,
                        times(3)
                    )
                    .onNext(capture())

                Assert.assertEquals(expectedState1, allValues[0])
                Assert.assertEquals(expectedState2, allValues[1])
                Assert.assertEquals(expectedState3, allValues[2])
            }
    }

    @Test
    fun changeEmail_ShouldEmitChangeEmailSuccess_WhenResponseIsSuccessful() {
        val email = "jermaine.dilao@appetiser.com.au"
        val verificationToken = "test verification token"

        val user = SESSION_LOGGED_IN

        val expectedState1 = ChangeEmailNewEmailState.ShowLoading
        val expectedState2 = ChangeEmailNewEmailState.HideLoading
        val expectedState3 =
            ChangeEmailNewEmailState
                .ChangeEmailSuccess(
                    email,
                    verificationToken
                )

        whenever(sessionRepository.getSession())
            .thenReturn(Single.just(user))

        whenever(resourceManager.validateEmail(any()))
            .thenReturn(true)
        whenever(authRepository.requestChangeEmail(any(), any()))
            .thenReturn(Completable.complete())

        subject.setVerificationToken(verificationToken)
        subject.changeEmail(email)
        testScheduler.triggerActions()

        argumentCaptor<ChangeEmailNewEmailState>()
            .run {
                Mockito
                    .verify(
                        stateObserver,
                        times(3)
                    )
                    .onNext(capture())

                Assert.assertEquals(expectedState1, allValues[0])
                Assert.assertEquals(expectedState2, allValues[1])
                Assert.assertEquals(expectedState3, allValues[2])
            }
    }
}
