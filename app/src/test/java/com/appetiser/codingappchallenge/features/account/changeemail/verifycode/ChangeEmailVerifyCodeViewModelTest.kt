package com.appetiser.codingappchallenge.features.account.changeemail.verifycode

import com.appetiser.codingappchallenge.core.BaseViewModelTest
import com.appetiser.codingappchallenge.utils.COUNTDOWN_MAX_TIMER
import com.appetiser.module.common.utils.ResourceManager
import com.appetiser.module.data.features.auth.AuthRepository
import com.appetiser.module.data.features.session.SessionRepository
import com.appetiser.module.domain.utils.*
import io.reactivex.Completable
import io.reactivex.Observer
import okhttp3.ResponseBody.Companion.toResponseBody
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import org.mockito.Mockito.times
import retrofit2.HttpException
import retrofit2.Response

class ChangeEmailVerifyCodeViewModelTest : BaseViewModelTest() {

    private val authRepository: AuthRepository = mock()
    private val sessionRepository: SessionRepository = mock()
    private val resourceManager: ResourceManager = mock()
    private val stateObserver: Observer<ChangeEmailVerifyCodeState> = mock()
    private val mockEmail = "jermaine.dilao@appetiser.com.au"
    private val countDown = COUNTDOWN_MAX_TIMER.toLong()

    private lateinit var subject: ChangeEmailVerifyCodeViewModel

    @Before
    fun setUp() {
        subject = ChangeEmailVerifyCodeViewModel(authRepository, sessionRepository, resourceManager)
        subject.schedulers = schedulers
        subject.state.subscribe(stateObserver)
    }

    @Test
    fun onCodeTextChanged_ShouldNotCallRepository_WhenCodeLengthIsLessThanFiveDigits() {
        val code = "123"

        setEmailAndVerificationToken()

        subject.onCodeTextChanged(code)

        Mockito
            .verifyZeroInteractions(
                authRepository
            )
    }

    @Test
    fun onCodeTextChanged_ShouldCallRepository_WhenCodeLengthReachesFiveDigits() {
        val code = "12345"

        setEmailAndVerificationToken()

        whenever(authRepository.verifyChangeEmail(any(), any()))
            .thenReturn(Completable.complete())

        subject.onCodeTextChanged(code)

        Mockito
            .verify(
                authRepository,
                times(1)
            )
            .verifyChangeEmail(any(), any())
    }

    @Test
    fun onCodeTextChanged_ShouldEmitInvalidVerificationCode_WhenVerificationCodeIsInvalid() {
        val code = "12345"
        val expectedState1 = ChangeEmailVerifyCodeState.DisplayEmail(mockEmail)
        val expectedState2 = ChangeEmailVerifyCodeState.HideKeyboard
        val expectedState3 = ChangeEmailVerifyCodeState.ShowLoading
        val expectedState4 = ChangeEmailVerifyCodeState.HideLoading
        val throwable =
            HttpException(
                Response
                    .error<Unit>(
                        400,
                        "Invalid verification code.".toResponseBody()
                    )
            )

        setEmailAndVerificationToken()

        whenever(authRepository.verifyChangeEmail(any(), any()))
            .thenReturn(Completable.error(throwable))

        subject.onCodeTextChanged(code)
        testScheduler.triggerActions()

        argumentCaptor<ChangeEmailVerifyCodeState>()
            .run {
                Mockito
                    .verify(
                        stateObserver,
                        times(5)
                    )
                    .onNext(capture())

                Assert.assertEquals(expectedState1, allValues[0])
                Assert.assertEquals(expectedState2, allValues[1])
                Assert.assertEquals(expectedState3, allValues[2])
                Assert.assertEquals(expectedState4, allValues[3])
                Assert
                    .assertTrue(
                        allValues[4] is ChangeEmailVerifyCodeState.InvalidVerificationCode
                    )
            }
    }

    @Test
    fun onCodeTextChanged_ShouldEmitError_WhenResponseThrowsException() {
        val code = "12345"
        val errorMessage = "Test error."
        val expectedState1 = ChangeEmailVerifyCodeState.DisplayEmail(mockEmail)
        val expectedState2 = ChangeEmailVerifyCodeState.HideKeyboard
        val expectedState3 = ChangeEmailVerifyCodeState.ShowLoading
        val expectedState4 = ChangeEmailVerifyCodeState.HideLoading
        val expectedState5 = ChangeEmailVerifyCodeState.Error(errorMessage)
        val throwable = Throwable(errorMessage)

        setEmailAndVerificationToken()

        whenever(authRepository.verifyChangeEmail(any(), any()))
            .thenReturn(Completable.error(throwable))
        whenever(resourceManager.getString(anyInt()))
            .thenReturn(errorMessage)

        subject.onCodeTextChanged(code)
        testScheduler.triggerActions()

        argumentCaptor<ChangeEmailVerifyCodeState>()
            .run {
                Mockito
                    .verify(
                        stateObserver,
                        times(5)
                    )
                    .onNext(capture())

                Assert.assertEquals(expectedState1, allValues[0])
                Assert.assertEquals(expectedState2, allValues[1])
                Assert.assertEquals(expectedState3, allValues[2])
                Assert.assertEquals(expectedState4, allValues[3])
                Assert.assertEquals(expectedState5, allValues[4])
            }
    }

    @Test
    fun onCodeTextChanged_ShouldEmitVerificationSuccess_WhenResponseIsSuccessful() {
        val code = "12345"
        val expectedState1 = ChangeEmailVerifyCodeState.DisplayEmail(mockEmail)
        val expectedState2 = ChangeEmailVerifyCodeState.HideKeyboard
        val expectedState3 = ChangeEmailVerifyCodeState.ShowLoading
        val expectedState4 = ChangeEmailVerifyCodeState.HideLoading
        val expectedState5 = ChangeEmailVerifyCodeState.VerificationSuccess

        setEmailAndVerificationToken()

        whenever(authRepository.verifyChangeEmail(any(), any()))
            .thenReturn(Completable.complete())

        subject.onCodeTextChanged(code)
        testScheduler.triggerActions()

        argumentCaptor<ChangeEmailVerifyCodeState>()
            .run {
                Mockito
                    .verify(
                        stateObserver,
                        times(5)
                    )
                    .onNext(capture())

                Assert.assertEquals(expectedState1, allValues[0])
                Assert.assertEquals(expectedState2, allValues[1])
                Assert.assertEquals(expectedState3, allValues[2])
                Assert.assertEquals(expectedState4, allValues[3])
                Assert.assertEquals(expectedState5, allValues[4])
            }
    }

    @Test
    fun resendCode_ShouldEmitError_WhenResponseThrowsException() {
        val errorMessage = "Oops, something went wrong."

        val expectedState0 = ChangeEmailVerifyCodeState.DisplayEmail(mockEmail)
        val expectedState1 = ChangeEmailVerifyCodeState.ResendTimer(countDown)
        val expectedState2 = ChangeEmailVerifyCodeState.ShowResendProgressLoading
        val expectedState3 = ChangeEmailVerifyCodeState.HideLoading
        val expectedState4 = ChangeEmailVerifyCodeState.HideResendProgressLoading
        val expectedState5 = ChangeEmailVerifyCodeState.ResendTimerCompleted
        val expectedState6 = ChangeEmailVerifyCodeState.Error(errorMessage)
        val throwable = Throwable(errorMessage)

        setEmailAndVerificationToken()

        whenever(authRepository
            .requestChangeEmail(
                anyString(),
                anyString()
            ))
            .thenReturn(Completable.error(throwable))
        whenever(resourceManager.getString(anyInt()))
            .thenReturn(errorMessage)

        subject.resendCode()
        testScheduler.triggerActions()

        argumentCaptor<ChangeEmailVerifyCodeState>()
            .run {
                Mockito
                    .verify(
                        stateObserver,
                        times(7)
                    )
                    .onNext(capture())

                Assert.assertEquals(expectedState0, allValues[0])
                Assert.assertEquals(expectedState1, allValues[1])
                Assert.assertEquals(expectedState2, allValues[2])
                Assert.assertEquals(expectedState3, allValues[3])
                Assert.assertEquals(expectedState4, allValues[4])
                Assert.assertEquals(expectedState5, allValues[5])
                Assert.assertEquals(expectedState6, allValues[6])
            }
    }

    @Test
    fun resendCode_ShouldEmitResendCodeSuccess_WhenResponseIsSuccessful() {
        val expectedState1 = ChangeEmailVerifyCodeState.DisplayEmail(mockEmail)
        val expectedState2 = ChangeEmailVerifyCodeState.ResendTimer(countDown)
        val expectedState3 = ChangeEmailVerifyCodeState.ShowResendProgressLoading
        val expectedState4 = ChangeEmailVerifyCodeState.ResendCodeSuccess

        setEmailAndVerificationToken()

        whenever(authRepository
            .requestChangeEmail(
                anyString(),
                anyString()
            ))
            .thenReturn(Completable.complete())

        subject.resendCode()
        testScheduler.triggerActions()

        argumentCaptor<ChangeEmailVerifyCodeState>()
            .run {
                Mockito
                    .verify(
                        stateObserver,
                        times(5)
                    )
                    .onNext(capture())

                Assert.assertEquals(expectedState1, allValues[0])
                Assert.assertEquals(expectedState2, allValues[1])
                Assert.assertEquals(expectedState3, allValues[2])
                Assert.assertEquals(expectedState4, allValues[4])
            }
    }

    private fun setEmailAndVerificationToken() {
        val verificationToken = "test verification token"

        subject
            .setEmailAndVerificationToken(
                mockEmail,
                verificationToken
            )
    }
}
