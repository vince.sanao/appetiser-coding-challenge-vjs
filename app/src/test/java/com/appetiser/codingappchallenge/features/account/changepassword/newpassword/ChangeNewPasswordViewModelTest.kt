package com.appetiser.codingappchallenge.features.account.changepassword.newpassword

import com.appetiser.codingappchallenge.core.BaseViewModelTest
import com.appetiser.module.common.utils.ResourceManager
import com.appetiser.module.data.features.auth.AuthRepository
import com.appetiser.module.domain.utils.anyInt
import com.appetiser.module.domain.utils.argumentCaptor
import com.appetiser.module.domain.utils.mock
import com.appetiser.module.domain.utils.whenever
import io.reactivex.Observer
import io.reactivex.Single
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito

class ChangeNewPasswordViewModelTest : BaseViewModelTest() {

    private val authRepository: AuthRepository = mock()
    private val resourceManager: ResourceManager = mock()
    private val stateObserver: Observer<ChangeNewPasswordState> = mock()

    private lateinit var subject: ChangeNewPasswordViewModel

    @Before
    fun setUp() {
        subject = ChangeNewPasswordViewModel(authRepository, resourceManager)
        subject.schedulers = schedulers
        subject.state.subscribe(stateObserver)
    }

    @Test
    fun onPasswordTextChanged_ShouldDisableButton_WhenPasswordIsEmpty() {
        val password = ""
        val expectedState = ChangeNewPasswordState.DisableButton

        subject.onPasswordTextChanged(password)

        argumentCaptor<ChangeNewPasswordState>()
            .run {
                Mockito
                    .verify(
                        stateObserver,
                        Mockito.times(1)
                    )
                    .onNext(capture())

                Assert.assertEquals(expectedState, value)
            }
    }

    @Test
    fun onPasswordTextChanged_ShouldEnableButton_WhenPasswordIsNotEmpty() {
        val password = "password"
        val expectedState = ChangeNewPasswordState.EnableButton

        subject.onPasswordTextChanged(password)

        argumentCaptor<ChangeNewPasswordState>()
            .run {
                Mockito
                    .verify(
                        stateObserver,
                        Mockito.times(1)
                    )
                    .onNext(capture())

                Assert.assertEquals(expectedState, value)
            }
    }

    @Test
    fun changePassword_ShouldNotCallRepository_WhenPasswordIsEmpty() {
        val password = ""

        subject.changePassword(password)

        Mockito.verifyZeroInteractions(authRepository)
    }

    @Test
    fun changePassword_ShouldEmitError_WhenResponseThrowsException() {
        val oldPassword = "password-old"
        val verificationToken = "verification-token"
        val password = "password"
        val errorMessage = "Test error"
        val expectedState1 = ChangeNewPasswordState.ShowProgressLoading
        val expectedState2 = ChangeNewPasswordState.HideProgressLoading
        val expectedState3 = ChangeNewPasswordState.Error(errorMessage)
        val throwable = Throwable(errorMessage)

        whenever(authRepository.changePassword(oldPassword, password, password))
            .thenReturn(Single.error(throwable))
        whenever(resourceManager.getString(anyInt()))
            .thenReturn(errorMessage)

        subject.setOldPasswordAndToken(oldPassword, verificationToken)
        subject.changePassword(password)
        testScheduler.triggerActions()

        argumentCaptor<ChangeNewPasswordState>()
            .run {
                Mockito
                    .verify(
                        stateObserver,
                        Mockito.times(3)
                    )
                    .onNext(capture())

                Assert.assertEquals(expectedState1, allValues[0])
                Assert.assertEquals(expectedState2, allValues[1])
                Assert.assertEquals(expectedState3, allValues[2])
            }
    }

    @Test
    fun changePassword_ShouldEmitSuccess_WhenResponseIsSuccessful() {
        val oldPassword = "password-old"
        val verificationToken = "verification-token"
        val password = "password"
        val expectedState1 = ChangeNewPasswordState.ShowProgressLoading
        val expectedState2 = ChangeNewPasswordState.HideProgressLoading
        val expectedState3 = ChangeNewPasswordState.Success

        whenever(authRepository.changePassword(oldPassword, password, password))
            .thenReturn(Single.just(true))

        subject.setOldPasswordAndToken(oldPassword, verificationToken)
        subject.changePassword(password)
        testScheduler.triggerActions()

        argumentCaptor<ChangeNewPasswordState>()
            .run {
                Mockito
                    .verify(
                        stateObserver,
                        Mockito.times(3)
                    )
                    .onNext(capture())

                Assert.assertEquals(expectedState1, allValues[0])
                Assert.assertEquals(expectedState2, allValues[1])
                Assert.assertEquals(expectedState3, allValues[2])
            }
    }
}
