package com.appetiser.codingappchallenge

import com.appetiser.module.domain.models.miscellaneous.UpdateGate
import com.appetiser.module.domain.models.session.Session
import com.appetiser.module.domain.models.token.AccessToken
import com.appetiser.module.domain.models.user.MediaFile
import com.appetiser.module.domain.models.user.User

object Stubs {

    val USER_LOGGED_IN = User(
        id = "1231232132123",
        fullName = "Jose Mari Chan",
        firstName = "Jose Mari",
        lastName = "Chan",
        email = "josemarichan@gmail.com",
        avatarPermanentThumbUrl = "http://www.google.com/",
        avatarPermanentUrl = "http://www.google.com/",
        phoneNumber = "+639435643214",
        emailVerified = true,
        phoneNumberVerified = true,
        verified = true,
        avatar = MediaFile(
            id = 123,
            name = "name",
            fileName = "fileName",
            collectionName = "collectionName",
            mimeType = "image",
            size = 160000,
            createdAt = "created_at",
            url = "url",
            thumbUrl = "thumbUrl"
        ),
        description = "description",
        birthDate = "1993-12-28"
    )

    val USER_SESSION_LOGGED_IN_NO_FULLNAME = User(
        id = "1231232132123",
        fullName = "",
        firstName = "",
        lastName = "",
        email = "",
        avatarPermanentThumbUrl = "/",
        avatarPermanentUrl = "",
        phoneNumber = "+639435643214",
        emailVerified = true,
        phoneNumberVerified = true,
        verified = true,
        avatar = MediaFile.empty(),
        description = "description",
        birthDate = "1993-12-28"
    )

    val USER_SESSION_LOGGED_IN_NO_EMAIL = User(
        id = "1231232132123",
        fullName = "",
        firstName = "",
        lastName = "",
        email = "",
        avatarPermanentThumbUrl = "/",
        avatarPermanentUrl = "",
        phoneNumber = "+639435643214",
        emailVerified = true,
        phoneNumberVerified = true,
        verified = true,
        avatar = MediaFile.empty(),
        description = "description",
        birthDate = "1993-12-28"
    )

    val USER_SESSION_LOGGED_IN_NO_DOB = User(
        id = "1231232132123",
        fullName = "Jose Mari Chan",
        firstName = "Jose Mari",
        lastName = "Chan",
        email = "josemarichan@gmail.com",
        avatarPermanentThumbUrl = "http://www.google.com/",
        avatarPermanentUrl = "http://www.google.com/",
        phoneNumber = "+639435643214",
        emailVerified = true,
        phoneNumberVerified = true,
        verified = true,
        avatar = MediaFile(
            id = 123,
            name = "name",
            fileName = "fileName",
            collectionName = "collectionName",
            mimeType = "image",
            size = 160000,
            createdAt = "created_at",
            url = "url",
            thumbUrl = "thumbUrl"
        ),
        description = "description",
        birthDate = ""
    )

    val USER_SESSION_LOGGED_IN_NO_PROFILE_PHOTO = User(
        id = "1231232132123",
        fullName = "Jose Mari Chan",
        firstName = "Jose Mari",
        lastName = "Chan",
        email = "josemarichan@gmail.com",
        avatarPermanentThumbUrl = "http://www.google.com/",
        avatarPermanentUrl = "http://www.google.com/",
        phoneNumber = "+639435643214",
        emailVerified = true,
        phoneNumberVerified = true,
        verified = true,
        avatar = MediaFile.empty(),
        description = "description",
        birthDate = "1993-12-28"
    )

    val USER_NOT_VERIFIED = User(
        id = "1111",
        fullName = "Test",
        firstName = "Test",
        lastName = "Test",
        email = "test@test.test",
        avatarPermanentThumbUrl = "http://photo",
        avatarPermanentUrl = "http://photo",
        verified = false,
        emailVerified = false,
        phoneNumber = "09177707257",
        phoneNumberVerified = false,
        avatar = MediaFile.empty(),
        description = "description",
        birthDate = "1993-12-28"
    )

    val VALID_ACCESS_TOKEN = AccessToken(token = "sampleToken")

    val SESSION_LOGGED_IN = Session(USER_LOGGED_IN, VALID_ACCESS_TOKEN)

    val SESSION_NOT_VERIFIED = Session(USER_NOT_VERIFIED, VALID_ACCESS_TOKEN)

    val SESSION_USER_LOGGED_IN_NO_FULLNAME = Session(USER_SESSION_LOGGED_IN_NO_FULLNAME, VALID_ACCESS_TOKEN)

    val SESSION_USER_LOGGED_IN_NO_DOB = Session(USER_SESSION_LOGGED_IN_NO_DOB, VALID_ACCESS_TOKEN)

    val SESSION_USER_LOGGED_IN_NO_PROFILE_PHOTO = Session(USER_SESSION_LOGGED_IN_NO_PROFILE_PHOTO, VALID_ACCESS_TOKEN)

    val UPDATE_GATE_EMPTY = UpdateGate.empty()
}
