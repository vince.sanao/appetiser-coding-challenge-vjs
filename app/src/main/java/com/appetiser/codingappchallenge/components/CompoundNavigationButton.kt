package com.appetiser.codingappchallenge.components

import android.content.Context
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.view.View
import androidx.appcompat.widget.AppCompatImageView
import androidx.constraintlayout.widget.ConstraintLayout
import com.appetiser.codingappchallenge.R
import com.appetiser.module.common.extensions.gone
import com.appetiser.module.common.extensions.visible
import com.google.android.material.textview.MaterialTextView

/**
 * Component used for navigation purposes. Contains [leftIcon], [rightIcon], [title], and [description].
 */
class CompoundNavigationButton @JvmOverloads constructor(
    context: Context,
    attributeSet: AttributeSet? = null,
    defStyle: Int = 0
) : ConstraintLayout(context, attributeSet, defStyle) {

    private lateinit var container: ConstraintLayout
    private lateinit var imgLeftIcon: AppCompatImageView
    private lateinit var imgRightIcon: AppCompatImageView
    private lateinit var txtTitle: MaterialTextView
    private lateinit var txtDescription: MaterialTextView
    private lateinit var txtRightLabel: MaterialTextView

    private var title: String = ""
    private var description: String = ""
    private var rightLabel: String = ""
    private var leftIcon: Drawable? = null
    private var rightIcon: Drawable? = null

    init {
        val attrs = context.obtainStyledAttributes(attributeSet, R.styleable.CompoundNavigationButton)

        try {
            title =
                attrs.getString(
                    R.styleable.CompoundNavigationButton_navigationButtonTitle
                ) ?: ""
            description =
                attrs.getString(
                    R.styleable.CompoundNavigationButton_navigationButtonDescription
                ) ?: ""
            rightLabel =
                attrs.getString(
                    R.styleable.CompoundNavigationButton_navigationButtonRightLabel
                ) ?: ""
            leftIcon =
                attrs.getDrawable(
                    R.styleable.CompoundNavigationButton_navigationButtonLeftIcon
                )
            rightIcon =
                attrs.getDrawable(
                    R.styleable.CompoundNavigationButton_navigationButtonRightIcon
                )
        } finally {
            attrs.recycle()
            initViews()
        }
    }

    private fun initViews() {
        View.inflate(context, R.layout.component_compound_navigation_button, this)

        container = findViewById(R.id.container)
        imgLeftIcon = findViewById(R.id.imgLeftIcon)
        imgRightIcon = findViewById(R.id.imgRightIcon)
        txtTitle = findViewById(R.id.txtTitle)
        txtDescription = findViewById(R.id.txtDescription)
        txtRightLabel = findViewById(R.id.txtRightLabel)

        setLeftIcon(leftIcon)
        setRightIcon(rightIcon)
        txtTitle.text = title
        setDescription(description)
        setRightLabel(rightLabel)
    }

    fun setRightLabel(label: String?) {
        if (label.isNullOrEmpty()) {
            txtRightLabel.gone()
        } else {
            txtRightLabel.text = label
            txtRightLabel.visible()
        }
    }

    fun setDescription(description: String?) {
        if (description.isNullOrEmpty()) {
            txtDescription.gone()
        } else {
            txtDescription.text = description
            txtDescription.visible()
        }
    }

    fun setLeftIcon(drawable: Drawable?) {
        if (drawable != null) {
            imgLeftIcon.setImageDrawable(leftIcon)
            imgLeftIcon.visible()
        } else {
            imgLeftIcon.gone()
        }
    }

    fun setRightIcon(drawable: Drawable?) {
        if (drawable != null) {
            imgRightIcon.setImageDrawable(rightIcon)
            imgRightIcon.visible()
        } else {
            imgRightIcon.gone()
        }
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
    }

    override fun setOnClickListener(l: OnClickListener?) {
        container.setOnClickListener(l)
    }
}
