package com.appetiser.codingappchallenge.features.main

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import androidx.annotation.ColorInt
import androidx.navigation.NavDirections
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import com.appetiser.codingappchallenge.R
import com.appetiser.module.common.base.BaseViewModelActivity
import com.appetiser.codingappchallenge.databinding.ActivityMainBinding
import com.appetiser.codingappchallenge.utils.NavigationUtils
import com.appetiser.codingappchallenge.utils.ViewUtils
import com.appetiser.module.common.extensions.*
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber
import com.appetiser.module.common.utils.ViewUtils as CommonViewUtils

class MainActivity : BaseViewModelActivity<ActivityMainBinding, MainViewModel>() {
    companion object {
        const val EXTRA_START_SCREEN = "EXTRA_START_SCREEN"
        const val EXTRA_EMAIL = "EXTRA_EMAIL"
        const val EXTRA_PHONE = "EXTRA_PHONE"

        const val START_MAIN = "START_MAIN"
        const val START_VERIFICATION = "START_VERIFICATION"
        const val START_WALKTHROUGH = "START_WALKTHROUGH"
        const val START_INPUT_NAME = "START_INPUT_NAME"
        const val START_UPLOAD_PROFILE_PHOTO = "START_UPLOAD_PROFILE_PHOTO"

        fun openActivity(context: Context, extras: Bundle) {
            context.startActivity(
                Intent(
                    context,
                    MainActivity::class.java
                ).apply {
                    putExtras(extras)
                }
            )
        }
    }

    override fun getLayoutId(): Int = R.layout.activity_main

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        handleExtras()

        setupVmObservers()
        setupViews()
        setupBroadcastReceivers()
    }

    override fun onDestroy() {
        unregisterReceiver(ioExceptionReceiver)
        super.onDestroy()
    }

    private val ioExceptionReceiver = object : BroadcastReceiver() {
        override fun onReceive(p0: Context?, p1: Intent?) {
            viewModel.onIoExceptionReceived()
        }
    }

    private fun setupBroadcastReceivers() {
        registerReceiver(
            ioExceptionReceiver,
            IntentFilter(getString(R.string.intent_action_io_exception))
        )
    }

    override fun onResume() {
        super.onResume()
        viewModel.versionCheck()
    }

    private fun setupViews() {
        binding.bottomNavigation.apply {
            setupWithNavController(navController())
            setOnNavigationItemReselectedListener { /* do nothing */ }

            // https://github.com/material-components/material-components-android/issues/499
            setOnApplyWindowInsetsListener(null)
        }
    }

    private fun setupVmObservers() {
        viewModel
            .state
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = ::handleState,
                onError = {
                    Timber.e(it)
                }
            )
            .apply { disposables.add(this) }
    }

    private fun handleState(state: MainState) {
        when (state) {
            MainState.ShowBottomNavigation -> {
                binding.bottomNavigation setVisible true
            }
            MainState.HideBottomNavigation -> {
                binding.bottomNavigation setVisible false
            }
            MainState.ChangeStatusBarToDarkTransparent -> {
                makeStatusBarTransparent(false)
            }
            MainState.ChangeStatusBarToLightTransparent -> {
                makeStatusBarTransparent(true)
            }
            MainState.ChangeStatusBarToDefault -> {
                makeStatusBarNonTransparent()
                changeStatusBarColor(getThemeColor(android.R.attr.colorBackground))
            }
            MainState.ChangeSoftInputModeAdjustPan -> {
                makeSoftInputModeAdjustPan()
            }
            MainState.ChangeSoftInputModeDefault -> {
                makeSoftInputModeAdjustResize()
            }
            is MainState.ShowUpdateGateForced -> {
                ViewUtils.showUpdateGate(
                    this,
                    state.updateGate.storeUrl,
                    true,
                    state.updateGate
                )
            }
            is MainState.ShowUpdateGateRecommended -> {
                ViewUtils.showUpdateGate(
                    this,
                    state.updateGate.storeUrl,
                    false,
                    state.updateGate,
                    updateLaterClickListener = {
                        viewModel.onUpdateLaterClicked()
                    }
                )
            }
            MainState.ShowNoInternetConnectionError -> {
                CommonViewUtils.showGenericErrorSnackBar(
                    binding.root,
                    getString(R.string.no_internet_connection_error)
                )
            }
        }
    }

    private fun changeStatusBarColor(@ColorInt color: Int) {
        if (window.statusBarColor != color) {
            window.statusBarColor = color
        }
    }

    private fun handleExtras() {
        intent?.extras?.let { bundle ->
            val startScreen = bundle.getString(EXTRA_START_SCREEN) ?: return

            require(startScreen.isNotEmpty()) {
                "EXTRA_START_SCREEN not found!"
            }

            when (startScreen) {
                START_VERIFICATION -> {
                    NavigationUtils.navigateToRegisterVerificationCodeScreen(
                        mainActivity = this,
                        email = bundle.getString(EXTRA_EMAIL, ""),
                        phone = bundle.getString(EXTRA_PHONE, "")
                    )
                }
                START_INPUT_NAME -> {
                    NavigationUtils.navigateToInputNameScreen(
                        mainActivity = this,
                        hideBackButton = true,
                        isExitOnBack = true
                    )
                }
                START_UPLOAD_PROFILE_PHOTO -> {
                    NavigationUtils.navigateToUploadProfilePhotoScreen(
                        mainActivity = this,
                        hideBackButton = true,
                        isExitOnBack = true
                    )
                }
            }
        }
    }

    private fun navigate(navDirections: NavDirections, destinationScreenId: Int) {
        if (navController().currentDestination!!.id != destinationScreenId) {
            navController()
                .navigate(navDirections)
        }
    }

    fun navController() = findNavController(R.id.nav_host_fragment)
}
