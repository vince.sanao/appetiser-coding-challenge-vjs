package com.appetiser.codingappchallenge.features.track

import android.os.Bundle
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.appetiser.codingappchallenge.ext.getThrowableError
import com.appetiser.codingappchallenge.utils.DateUtils
import com.appetiser.codingappchallenge.utils.DateUtils.formatToString
import com.appetiser.module.common.base.BaseViewModel
import com.appetiser.module.data.features.track.TrackRepository
import com.appetiser.module.domain.models.track.Track
import io.reactivex.Observable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

class TrackViewModel @Inject constructor(
    private val trackRepository: TrackRepository
) : BaseViewModel() {

    private val _state by lazy {
        PublishSubject.create<TrackState>()
    }

    val state: Observable<TrackState> = _state

    private val _tracks by lazy {
        MutableLiveData<List<Track>>()
    }

    private val _date by lazy {
        MutableLiveData<String>()
    }

    val tracks: LiveData<List<Track>> = _tracks

    val date: LiveData<String> = _date

    override fun isFirstTimeUiCreate(bundle: Bundle?) = Unit

    fun getTracks() {
        trackRepository
            .getTracks()
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .doOnSubscribe {
                _state.onNext(TrackState.ShowLoading)
            }
            .doOnSuccess { _state.onNext(TrackState.HideLoading) }
            .doOnError { _state.onNext(TrackState.HideLoading) }
            .subscribeBy(
                onSuccess = {
                    _tracks.value = it.results
                },
                onError = {
                    _state.onNext(TrackState.Error(it.getThrowableError()))
                }
            ).addTo(disposables)
    }

    fun getDate() {
        _date.postValue(
            trackRepository
            .getDateFromPref()
        )
    }

    fun saveDate() {
        trackRepository
            .saveDateFromPref(DateUtils.getCurrentDateTime().formatToString())
    }
}
