package com.appetiser.codingappchallenge.features.track.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.appetiser.codingappchallenge.R
import com.appetiser.codingappchallenge.databinding.ItemTrackBinding
import com.appetiser.module.common.base.BaseItemCallback
import com.appetiser.module.common.base.BaseListAdapter
import com.appetiser.module.common.extensions.loadImageUrl
import com.appetiser.module.domain.models.track.Track
import io.reactivex.subjects.PublishSubject

class TrackAdapter(val context: Context) :
    BaseListAdapter<Track, BaseListAdapter.BaseViewViewHolder<Track>>(BaseItemCallback<Track>()) {

    val itemClickListener = PublishSubject.create<Track>()

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): BaseViewViewHolder<Track> {
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.item_track, parent, false
            )
        )
    }

    override fun onBindViewHolder(holder: BaseViewViewHolder<Track>, position: Int) {
        holder.bind(getItem(position))
    }

    inner class ViewHolder(override val binding: ItemTrackBinding) :
        BaseViewViewHolder<Track>(binding) {
        override fun bind(item: Track) {
            with(binding) {
                txtTrackName.text =
                    if (item.trackName != "null" && !item.trackName.isNullOrEmpty())
                        item.trackName
                    else
                        item.collectionName

                txtTrackGenre.text = item.primaryGenreName

                txtTrackPrice.text =
                    if (item.trackPrice != null && item.trackPrice != 0.0)
                        "$${String.format("%.2f", item.trackPrice)}"
                    else
                        "Free"

                if (!item.artworkUrl100.isNullOrEmpty())
                    imgTrack.loadImageUrl(item.artworkUrl100)

                binding
                    .viewTrackItem
                    .setOnClickListener {
                        itemClickListener.onNext(item)
                    }
            }
        }
    }
}
