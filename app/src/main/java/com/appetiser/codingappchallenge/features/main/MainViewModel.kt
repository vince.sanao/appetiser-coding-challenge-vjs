package com.appetiser.codingappchallenge.features.main

import android.os.Bundle
import com.appetiser.module.common.base.BaseViewModel
import com.appetiser.module.data.features.session.SessionRepository
import com.appetiser.module.domain.models.miscellaneous.UpdateGate
import com.appetiser.module.domain.models.session.Session
import com.appetiser.module.domain.utils.RECOMMEND_UPDATE_INTERVAL_IN_MILLIS
import io.reactivex.Observable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.PublishSubject
import timber.log.Timber
import javax.inject.Inject

class MainViewModel @Inject constructor(
    private val sessionRepository: SessionRepository
) : BaseViewModel() {

    private val _state by lazy {
        PublishSubject.create<MainState>()
    }

    val state: Observable<MainState> = _state

    override fun isFirstTimeUiCreate(bundle: Bundle?) = Unit

    fun versionCheck() {
        sessionRepository
            .getSession()
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribeBy(
                onSuccess = { session ->
                    if (session.updateGate == null) return@subscribeBy

                    handleUpdateGate(session.updateGate!!, session)
                },
                onError = {
                    Timber.e(it)
                }
            )
            .addTo(disposables)
    }

    fun onUpdateLaterClicked() {
        sessionRepository
            .getSession()
            .flatMap { session ->
                session.recommendUpdateTimeStamp = System.currentTimeMillis()

                sessionRepository.saveSession(session)
            }
            .ignoreElement()
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribeBy(
                onComplete = {},
                onError = {
                    Timber.e(it)
                }
            )
            .addTo(disposables)
    }

    private fun handleUpdateGate(updateGate: UpdateGate, session: Session) {
        if (updateGate.isRequired) {
            _state.onNext(
                MainState.ShowUpdateGateForced(
                    updateGate
                )
            )
        } else {
            val recommendUpdateTimeElapsed = System.currentTimeMillis() - session.recommendUpdateTimeStamp
            val shouldRecommendUpdate = recommendUpdateTimeElapsed >= RECOMMEND_UPDATE_INTERVAL_IN_MILLIS

            // If recommend update has not yet passed interval time.
            if (!shouldRecommendUpdate) return

            _state.onNext(
                MainState.ShowUpdateGateRecommended(
                    updateGate
                )
            )
        }
    }

    fun onIoExceptionReceived() {
        _state.onNext(MainState.ShowNoInternetConnectionError)
    }
}
