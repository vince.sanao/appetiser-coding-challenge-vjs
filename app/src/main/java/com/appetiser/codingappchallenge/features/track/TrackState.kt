package com.appetiser.codingappchallenge.features.track

sealed class TrackState {

    object ShowLoading : TrackState()

    object HideLoading : TrackState()

    data class Error(val message: String) : TrackState()
}
