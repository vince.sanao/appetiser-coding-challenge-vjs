package com.appetiser.codingappchallenge.features.main

import android.view.View
import com.appetiser.module.domain.models.miscellaneous.UpdateGate

sealed class MainState {
    data class ShowUpdateGateForced(val updateGate: UpdateGate) : MainState()

    data class ShowUpdateGateRecommended(val updateGate: UpdateGate) : MainState()

    object ShowBottomNavigation : MainState()

    object HideBottomNavigation : MainState()

    object ChangeSoftInputModeAdjustPan : MainState()

    object ChangeSoftInputModeDefault : MainState()

    /**
     * Default status bar color.
     */
    object ChangeStatusBarToDefault : MainState()

    /**
     * Changes status bar to transparent with [View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR] flag set to false.
     */
    object ChangeStatusBarToDarkTransparent : MainState()

    /**
     * Changes status bar to transparent with [View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR] flag set to true.
     */
    object ChangeStatusBarToLightTransparent : MainState()

    object ShowNoInternetConnectionError : MainState()
}
