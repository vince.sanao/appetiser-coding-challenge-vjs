package com.appetiser.codingappchallenge.features.splash

sealed class SplashState {
    /**
     * User has logged in and completed onboarding flow.
     */
    object UserIsLoggedIn : SplashState()

    /**
     * User is not logged in, will show walkthrough.
     */
    object UserIsNotLoggedIn : SplashState()

    /**
     * User is logged in but not verified.
     */
    data class UserIsLoggedInButNotVerified(
        val email: String,
        val phone: String
    ) : SplashState()

    /**
     * User has logged in but has no name yet.
     */
    object NoFullName : SplashState()

    object NoProfilePhoto : SplashState()
}
