package com.appetiser.codingappchallenge.features.track.detail

import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.navArgs
import com.appetiser.codingappchallenge.R
import com.appetiser.codingappchallenge.databinding.FragmentTrackDetailBinding
import com.appetiser.module.common.base.BaseFragment
import com.appetiser.module.common.extensions.loadImageUrl
import com.appetiser.module.common.extensions.toHtml

class TrackDetailFragment : BaseFragment<FragmentTrackDetailBinding>() {

    override fun getLayoutId(): Int = R.layout.fragment_track_detail
    private val args: TrackDetailFragmentArgs by navArgs()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupViews()
    }

    private fun setupViews() {
        val detail = args.track

        with(binding) {
            imgTrackDetail.loadImageUrl(detail.artworkUrl100)

            txtTrackNameDetail.text =
                if (detail.trackName != "null" && !detail.trackName.isNullOrEmpty())
                    detail.trackName
                else
                    detail.collectionName

            txtTrackGenreDetail.text = detail.primaryGenreName

            btnTrackPriceDetail.text =
                if (detail.trackPrice != null && detail.trackPrice != 0.0)
                    "$${String.format("%.2f", detail.trackPrice)}"
                else
                    "Free"

            txtTrackDescDetail.text =
                if (!detail.longDescription.isNullOrEmpty() && detail.longDescription != "null")
                    detail.longDescription!!.toHtml()
                else if (!detail.description.isNullOrEmpty() && detail.description != "null")
                    detail.description!!.toHtml()
                else
                    "No Description"

            imgBack.setOnClickListener {
                activity?.onBackPressed()
            }
        }
    }
}
