package com.appetiser.codingappchallenge.features.track

import android.os.Bundle
import android.view.View
import com.appetiser.codingappchallenge.R
import com.appetiser.codingappchallenge.databinding.FragmentTrackBinding
import com.appetiser.codingappchallenge.features.track.adapter.TrackAdapter
import com.appetiser.module.common.base.BaseViewModelFragment
import com.appetiser.module.common.extensions.NINJA_TAP_THROTTLE_TIME
import com.appetiser.module.common.extensions.navigate
import com.appetiser.module.common.extensions.setVisible
import com.appetiser.module.common.utils.ViewUtils
import com.appetiser.module.domain.models.track.Track
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber
import java.util.concurrent.TimeUnit

class TrackFragment : BaseViewModelFragment<FragmentTrackBinding, TrackViewModel>() {

    private var trackAdapter: TrackAdapter? = null

    override fun getLayoutId(): Int = R.layout.fragment_track

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupViews()
        setupVmObservers()
        viewModel.getTracks()
        viewModel.saveDate()
        viewModel.getDate()
    }

    private fun setupViews() {
        binding
            .rvTracks
            .apply {
                trackAdapter = TrackAdapter(requireContext())

                trackAdapter!!
                    .itemClickListener
                    .throttleFirst(NINJA_TAP_THROTTLE_TIME, TimeUnit.MILLISECONDS)
                    .observeOn(scheduler.ui())
                    .subscribeBy(
                        onNext = {
                            navigateToDetails(it)
                        }
                    )
                    .addTo(disposables)

                adapter = trackAdapter
            }
    }

    private fun setupVmObservers() {
        viewModel
            .state
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = {
                    handleState(it)
                },
                onError = {
                    Timber.e(it)
                }
            )
            .addTo(disposables)

        viewModel
            .tracks
            .observe(viewLifecycleOwner, {
                trackAdapter?.submitList(it)
            })

        viewModel
            .date
            .observe(viewLifecycleOwner, {
                ViewUtils.showGenericSuccessSnackBar(
                    binding.root,
                    "${getString(R.string.previous_visit_date)}$it"
                )
            })
    }

    private fun handleState(state: TrackState) {
        when (state) {
            is TrackState.Error -> {
                ViewUtils.showGenericErrorSnackBar(
                    binding.root,
                    state.message
                )
            }
            TrackState.HideLoading -> binding.loading setVisible false
            TrackState.ShowLoading -> binding.loading setVisible true
        }
    }

    private fun navigateToDetails(item: Track) {
        navigate(
            TrackFragmentDirections
                .actionTrackFragmentToTrackDetailFragment(
                    item
                )
        )
    }

    override fun onDestroy() {
        super.onDestroy()

        trackAdapter = null
    }
}
