package com.appetiser.codingappchallenge.features.splash

import android.os.Bundle
import androidx.core.os.bundleOf
import com.appetiser.codingappchallenge.R
import com.appetiser.module.common.base.BaseViewModelActivity
import com.appetiser.codingappchallenge.databinding.ActivitySplashBinding
import com.appetiser.codingappchallenge.features.main.MainActivity
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber

class SplashActivity : BaseViewModelActivity<ActivitySplashBinding, SplashViewModel>() {
    override fun getLayoutId(): Int = R.layout.activity_splash

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setupVmObservers()
    }

    private fun setupVmObservers() {
        viewModel
            .state
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = ::handleState,
                onError = {
                    Timber.e(it)
                }
            )
            .addTo(disposables)
    }

    private fun handleState(state: SplashState) {
        when (state) {
            SplashState.UserIsLoggedIn -> {
                MainActivity.openActivity(
                    this,
                    bundleOf(MainActivity.EXTRA_START_SCREEN to MainActivity.START_MAIN)
                )
            }
            is SplashState.UserIsLoggedInButNotVerified -> {
                MainActivity.openActivity(
                    this,
                    bundleOf(
                        MainActivity.EXTRA_START_SCREEN to MainActivity.START_VERIFICATION,
                        MainActivity.EXTRA_EMAIL to state.email,
                        MainActivity.EXTRA_PHONE to state.phone
                    )
                )
            }
            SplashState.UserIsNotLoggedIn -> {
                MainActivity.openActivity(
                    this,
                    bundleOf(MainActivity.EXTRA_START_SCREEN to MainActivity.START_WALKTHROUGH)
                )
            }
            SplashState.NoFullName -> {
                MainActivity.openActivity(
                    this,
                    bundleOf(MainActivity.EXTRA_START_SCREEN to MainActivity.START_INPUT_NAME)
                )
            }
            SplashState.NoProfilePhoto -> {
                MainActivity.openActivity(
                    this,
                    bundleOf(MainActivity.EXTRA_START_SCREEN to MainActivity.START_UPLOAD_PROFILE_PHOTO)
                )
            }
        }

        finishAffinity()
    }
}
