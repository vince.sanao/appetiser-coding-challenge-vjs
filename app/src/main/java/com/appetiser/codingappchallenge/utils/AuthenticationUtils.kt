package com.appetiser.codingappchallenge.utils

import com.appetiser.module.domain.models.session.Session
import com.appetiser.module.domain.models.user.User
import com.appetiser.module.domain.utils.PASSWORD_MAX_LENGTH
import com.appetiser.module.domain.utils.PASSWORD_MIN_LENGTH

object AuthenticationUtils {

    fun validatePasswordReachesMinLength(password: String): Boolean {
        return password.isNotEmpty() && password.length >= PASSWORD_MIN_LENGTH
    }

    fun validatePasswordNotExceedsMaxLength(password: String): Boolean {
        return password.isNotEmpty() && password.length <= PASSWORD_MAX_LENGTH
    }

    /**
     * Used to check for user auth state after login or forgot password flow.
     */
    fun getUserAuthState(user: User, session: Session): AuthState {
        if (user.onBoardedAt != null) {
            return AuthState.Completed
        }

        if (!user.verified && !session.hasSkippedEmailVerification) {
            return AuthState.NotVerified
        }

        return when {
            !user.hasFullName() -> {
                // User has no first and last name.
                AuthState.NoFullName
            }
            !user.hasProfilePhoto() -> {
                // User has no profile photo.
                AuthState.NoProfilePhoto
            }
            else -> {
                // User has all required details.
                AuthState.Completed
            }
        }
    }
}

sealed class AuthState {
    object NotVerified : AuthState()

    object NoFullName : AuthState()

    object NoProfilePhoto : AuthState()

    // skip all auth state if user is done onboarding (when user clicked the skip button in onboarding upload photo)
    object Completed : AuthState()
}
