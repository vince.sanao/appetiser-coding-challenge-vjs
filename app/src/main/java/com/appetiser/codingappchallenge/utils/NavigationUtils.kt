package com.appetiser.codingappchallenge.utils

import androidx.annotation.IdRes
import androidx.core.net.toUri
import androidx.navigation.NavDeepLinkRequest
import androidx.navigation.NavOptions
import com.appetiser.codingappchallenge.R
import com.appetiser.codingappchallenge.features.main.MainActivity

object NavigationUtils {

    /**
     * Using deeplink to navigate to a specific fragment inside nested-graph.
     * This is discouraged. Try to rethink the graph first before falling back to this approach.
     */
    fun navigateToRegisterVerificationCodeScreen(
        mainActivity: MainActivity,
        email: String = "",
        phone: String = "",
        @IdRes popUpTo: Int? = null,
        popUpToInclusive: Boolean = false
    ) {
        var uriString = mainActivity.getString(R.string.register_verification_nav_uri)
        uriString = uriString.replace("{email}", email)
        uriString = uriString.replace("{phone}", phone)

        val request = NavDeepLinkRequest
            .Builder
            .fromUri(uriString.toUri())
            .build()

        val navOptionsBuilder = NavOptions.Builder()
            .setEnterAnim(R.anim.anim_slide_in_right)
            .setExitAnim(R.anim.anim_slide_out_left)
            .setPopEnterAnim(R.anim.anim_slide_in_deep_left)
            .setPopExitAnim(R.anim.anim_slide_out_right)

        if (popUpTo != null) {
            navOptionsBuilder.setPopUpTo(
                popUpTo,
                popUpToInclusive
            )
        }

        mainActivity
            .navController()
            .navigate(
                request,
                navOptionsBuilder.build()
            )
    }

    /**
     * Using deeplink to navigate to a specific fragment inside nested-graph.
     * This is discouraged. Try to rethink the graph first before falling back to this approach.
     *
     * @param hideBackButton set to true to remove "back" button in upload photo screen.
     */
    fun navigateToUploadProfilePhotoScreen(
        mainActivity: MainActivity,
        hideBackButton: Boolean = false,
        isExitOnBack: Boolean = false
    ) {
        var uriString = mainActivity.getString(R.string.upload_profile_photo_nav_uri)
        uriString = uriString.replace("{hideBackButton}", hideBackButton.toString()).replace("{isExitOnBack}", isExitOnBack.toString())

        val request = NavDeepLinkRequest
            .Builder
            .fromUri(uriString.toUri())
            .build()

        val navOptions = NavOptions.Builder()
            .setEnterAnim(R.anim.anim_slide_in_right)
            .setExitAnim(R.anim.anim_slide_out_left)
            .setPopEnterAnim(R.anim.anim_slide_in_deep_left)
            .setPopExitAnim(R.anim.anim_slide_out_right)
            .build()

        mainActivity
            .navController()
            .navigate(
                request,
                navOptions
            )
    }

    /**
     * Using deeplink to navigate to a specific fragment inside nested-graph.
     * This is discouraged. Try to rethink the graph first before falling back to this approach.
     *
     * @param hideBackButton set to true to remove "back" button in upload photo screen.
     */
    fun navigateToInputNameScreen(
        mainActivity: MainActivity,
        hideBackButton: Boolean = false,
        isExitOnBack: Boolean = false
    ) {
        var uriString = mainActivity.getString(R.string.input_name_nav_uri)
        uriString = uriString.replace("{hideBackButton}", hideBackButton.toString()).replace("{isExitOnBack}", isExitOnBack.toString())

        val request = NavDeepLinkRequest
            .Builder
            .fromUri(uriString.toUri())
            .build()

        val navOptions = NavOptions.Builder()
            .setEnterAnim(R.anim.anim_slide_in_right)
            .setExitAnim(R.anim.anim_slide_out_left)
            .setPopEnterAnim(R.anim.anim_slide_in_deep_left)
            .setPopExitAnim(R.anim.anim_slide_out_right)
            .build()

        mainActivity
            .navController()
            .navigate(
                request,
                navOptions
            )
    }
}
