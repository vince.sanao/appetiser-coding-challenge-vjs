package com.appetiser.codingappchallenge.utils

import com.appetiser.module.domain.utils.DATE_FORMAT
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle
import java.util.*

object DateUtils {
    /**
     * Formats date to short date format based on device's locale.
     */
    fun formatDateLocalized(date: LocalDate): String {
        val formatter = DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT)
        return formatter.format(date)
    }

    /**
     * Formats date to short date format based on device's locale.
     */
    fun formatDate(date: LocalDate): String {
        val formatter = DateTimeFormatter.ofPattern(DATE_FORMAT)
        return formatter.format(date)
    }

    /**
     * Parses date of birth of this format "yyyy-mm-d" to [LocalDate].
     */
    fun parseBirthDate(birthDate: String): LocalDate {
        require(birthDate.isNotEmpty()) {
            "Required parameter birthDate was empty."
        }

        val split = birthDate.split("-")

        return LocalDate
            .of(
                split[0].toInt(),
                split[1].toInt(),
                split[2].toInt()
            )
    }

    fun getCurrentDateTime(): Date {
        return Calendar.getInstance().time
    }

    fun Date.formatToString(locale: Locale = Locale.getDefault()): String {
        val formatter = SimpleDateFormat(DATE_FORMAT, locale)
        return formatter.format(this)
    }
}
