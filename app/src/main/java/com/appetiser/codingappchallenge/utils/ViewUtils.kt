package com.appetiser.codingappchallenge.utils

import android.content.Context
import com.appetiser.codingappchallenge.R
import com.appetiser.module.common.extensions.openBrowser
import com.appetiser.module.common.utils.ViewUtils.showConfirmDialog
import com.appetiser.module.domain.models.miscellaneous.UpdateGate

object ViewUtils {
    fun showUpdateGate(
        context: Context,
        storeUrl: String,
        isRequired: Boolean,
        updateGate: UpdateGate,
        updateLaterClickListener: () -> Unit = {}
    ) {
        if (isRequired) {
            showConfirmDialog(
                context = context,
                title = if (updateGate.title.isEmpty())context.getString(R.string.update_gate_title) else updateGate.title,
                body = if (updateGate.message.isEmpty())context.getString(R.string.update_gate_description) else updateGate.message,
                positiveButtonText = context.getString(R.string.update),
                positiveClickListener = {
                    context.openBrowser(storeUrl)
                }
            )
        } else {
            showConfirmDialog(
                context = context,
                title = if (updateGate.title.isEmpty())context.getString(R.string.update_gate_title) else updateGate.title,
                body = if (updateGate.message.isEmpty())context.getString(R.string.update_gate_description) else updateGate.message,
                positiveButtonText = context.getString(R.string.update),
                negativeButtonText = context.getString(R.string.later),
                positiveClickListener = {
                    context.openBrowser(storeUrl)
                },
                negativeClickListener = {
                    updateLaterClickListener()
                }
            )
        }
    }
}
