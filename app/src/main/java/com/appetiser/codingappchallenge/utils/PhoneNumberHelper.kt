package com.appetiser.codingappchallenge.utils

import android.content.Context
import android.text.TextUtils
import com.google.i18n.phonenumbers.PhoneNumberUtil
import com.google.i18n.phonenumbers.Phonenumber
import timber.log.Timber
import java.util.*
import javax.inject.Inject

class PhoneNumberHelper @Inject constructor(
    private val context: Context
) {
    /**
     * Returns phone number in international format.
     *
     * @param phoneNumber phone number to format
     * @param countryNameCode country name code. (i.e. "au", "ph", "cn")
     */
    fun getInternationalFormattedPhoneNumber(phoneNumber: String, countryNameCode: String, format: PhoneNumberUtil.PhoneNumberFormat = PhoneNumberUtil.PhoneNumberFormat.E164): String {
        return try {
            val phoneNumberUtil: PhoneNumberUtil = PhoneNumberUtil.getInstance()
            val numberProto: Phonenumber.PhoneNumber = phoneNumberUtil.parse(
                phoneNumber,
                countryNameCode.toUpperCase(Locale.getDefault())
            )

            phoneNumberUtil
                .format(
                    numberProto,
                    format
                )
        } catch (e: Exception) {
            Timber.e(e)
            phoneNumber
        }
    }

    /**
     * Returns pair of phone country code and the number.
     *
     * @param phoneNumber phone number to format
     * @param countryNameCode country name code. (i.e. "au", "ph", "cn")
     */
    fun getCountryCodeAndNumber(phoneNumber: String, countryNameCode: String): Pair<String, String> {
        return try {
            val phoneNumberUtil: PhoneNumberUtil = PhoneNumberUtil.getInstance()
            val numberProto: Phonenumber.PhoneNumber = phoneNumberUtil.parse(
                phoneNumber,
                countryNameCode.toUpperCase(Locale.getDefault())
            )

            numberProto.countryCode.toString() to numberProto.nationalNumber.toString()
        } catch (e: Exception) {
            Timber.e(e)
            "" to phoneNumber
        }
    }

    /**
     * Validates phone number format as per [country].
     */
    fun validatePhoneNumber(phoneNumber: String, country: String): Boolean {
        return try {
            val phoneNumberUtils = PhoneNumberUtil.getInstance()
            val numberProto =
                phoneNumberUtils
                    .parse(
                        phoneNumber,
                        country.toUpperCase(Locale.getDefault())
                    )
            phoneNumberUtils.isValidNumber(numberProto)
        } catch (e: Exception) {
            return false
        }
    }

    /**
     * Returns true if [value] contains digits only and "+" character.
     */
    fun isNumberDigitsOnly(value: CharSequence): Boolean {
        var trimmed = value.trim()

        if (trimmed.isEmpty()) {
            return false
        }

        trimmed = trimmed.replace(Regex("\\s"), "")

        if (trimmed.startsWith("+")) {
            trimmed = trimmed.subSequence(1, trimmed.length)
        }

        return TextUtils.isDigitsOnly(trimmed)
    }
}
