package com.appetiser.codingappchallenge.di

import com.appetiser.module.common.utils.schedulers.BaseSchedulerProvider
import com.appetiser.module.common.utils.schedulers.SchedulerProvider
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class SchedulerModule {

    @Provides
    @Singleton
    fun providesSchedulerSource(): BaseSchedulerProvider =
            SchedulerProvider.getInstance()
}
