package com.appetiser.codingappchallenge.di.builders

import com.appetiser.codingappchallenge.di.scopes.FragmentScope
import com.appetiser.codingappchallenge.features.track.TrackFragment
import com.appetiser.codingappchallenge.features.track.detail.TrackDetailFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentBuilder {

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributeTrackFragment(): TrackFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributeTrackDetailFragment(): TrackDetailFragment
}
