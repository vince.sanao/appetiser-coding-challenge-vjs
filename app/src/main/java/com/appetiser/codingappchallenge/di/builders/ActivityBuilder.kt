package com.appetiser.codingappchallenge.di.builders

import com.appetiser.codingappchallenge.di.scopes.ActivityScope
import com.appetiser.codingappchallenge.features.main.MainActivity
import com.appetiser.codingappchallenge.features.splash.SplashActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilder {

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeSplashActivity(): SplashActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeMainActivity(): MainActivity
}
