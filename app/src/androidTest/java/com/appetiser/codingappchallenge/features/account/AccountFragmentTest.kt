package com.appetiser.codingappchallenge.features.account

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import com.appetiser.codingappchallenge.R
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@LargeTest
class AccountFragmentTest {

    @get:Rule
    val activityRule = ActivityScenarioRule(AccountFragment::class.java)

    @Test
    fun ifPayoutsSectionClicked_shouldGoToPayoutsScreen() {
        onView(withId(R.id.layoutPayouts)).perform(click())
        onView(withId(R.id.activityPayoutsParent)).check(matches(isDisplayed()))
    }
}
