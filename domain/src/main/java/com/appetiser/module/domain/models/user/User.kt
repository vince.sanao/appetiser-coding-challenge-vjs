package com.appetiser.module.domain.models.user

import android.os.Parcelable
import com.google.gson.JsonObject
import kotlinx.parcelize.Parcelize
import java.time.Instant

@Parcelize
data class User(
    val id: String,
    var email: String,
    var lastName: String,
    var firstName: String,
    var fullName: String,
    var avatarPermanentUrl: String,
    var avatarPermanentThumbUrl: String,
    var phoneNumber: String,
    val emailVerified: Boolean,
    val phoneNumberVerified: Boolean,
    val verified: Boolean,
    var avatar: MediaFile,
    var description: String,
    var birthDate: String,
    val createdAt: Instant? = null,
    val updatedAt: Instant? = null,
    val blockedAt: Instant? = null,
    val onBoardedAt: Instant? = null,
    val primaryUserNameType: PrimaryUserNameType = PrimaryUserNameType.EMAIL
) : Parcelable {
    companion object {
        fun empty(): User {
            return User(
                id = "",
                email = "",
                lastName = "",
                firstName = "",
                fullName = "",
                avatarPermanentUrl = "",
                avatarPermanentThumbUrl = "",
                phoneNumber = "",
                emailVerified = false,
                phoneNumberVerified = false,
                verified = false,
                avatar = MediaFile.empty(),
                description = "",
                birthDate = "",
                primaryUserNameType = PrimaryUserNameType.EMAIL
            )
        }
    }

    fun toJsonStringExcludeEmpty(): String {
        val userJson = JsonObject()
            .apply {
                if (email.isNotEmpty()) addProperty("email", email)
                if (phoneNumber.isNotEmpty()) addProperty("phone_number", phoneNumber)
                if (lastName.isNotEmpty()) addProperty("last_name", lastName)
                if (firstName.isNotEmpty()) addProperty("first_name", firstName)
                if (fullName.isNotEmpty()) addProperty("full_name", fullName)
                if (description.isNotEmpty()) addProperty("description", description)
                if (birthDate.isNotEmpty()) addProperty("birthdate", birthDate)
            }

        return userJson
            .toString()
    }

    /**
     * Returns true if user has full name.
     */
    fun hasFullName(): Boolean {
        return fullName.isNotEmpty()
    }

    /**
     * Returns true if user has profile photo.
     */
    fun hasProfilePhoto(): Boolean {
        return avatar.isNotEmpty()
    }

    /**
     * Returns true if user is empty.
     */
    fun isEmpty() = id.isEmpty()

    /**
     * Returns true if user is not empty.
     */
    fun isNotEmpty() = !isEmpty()

    /**
     *
     * Returns image URL
     */
    fun getUserAvatarURL(): String {
        return if (avatar.thumbUrl.isNotEmpty()) {
            avatar.thumbUrl
        } else {
            avatarPermanentThumbUrl
        }
    }
}