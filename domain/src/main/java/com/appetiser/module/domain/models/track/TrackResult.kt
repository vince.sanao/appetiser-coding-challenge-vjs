package com.appetiser.module.domain.models.track

data class TrackResult (
    val resultCount : Int? = null,
    val results: List<Track>? = null
)