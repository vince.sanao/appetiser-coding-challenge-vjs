package com.appetiser.module.domain.models.track

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Track (
    val wrapperType : String? = null,
    val kind : String? = null,
    val artistId : Int? = null,
    val collectionId : Int? = null,
    val trackId : Int? = null,
    val artistName : String? = null,
    val collectionName : String? = null,
    val trackName : String? = null,
    val collectionCensoredName : String? = null,
    val trackCensoredName : String? = null,
    val artworkUrl100 : String? = null,
    val collectionPrice : Double? = null,
    val trackPrice : Double? = null,
    val releaseDate : String? = null,
    val collectionExplicitness : String? = null,
    val trackExplicitness : String? = null,
    val discCount : Int? = null,
    val discNumber : Int? = null,
    val trackCount : Int? = null,
    val trackNumber : Int? = null,
    val trackTimeMillis : Int? = null,
    val country : String? = null,
    val currency : String? = null,
    val primaryGenreName : String? = null,
    val description : String? = null,
    val longDescription : String? = null,
    val stampDate : Long? = null
) : Parcelable