package com.appetiser.module.domain.models.user

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class MediaFile(
    val id: Long,
    val name: String,
    val fileName: String,
    val collectionName: String,
    val mimeType: String,
    val size: Long,
    val createdAt: String,
    val url: String,
    val thumbUrl: String
) : Parcelable {
    companion object {
        fun empty(): MediaFile {
            return MediaFile(
                id = 0,
                name = "",
                fileName = "",
                collectionName = "",
                mimeType = "",
                size = 0,
                createdAt = "",
                url = "",
                thumbUrl = ""
            )
        }
    }

    /**
     * Returns true if media file is empty.
     */
    fun isEmpty(): Boolean {
        return id == 0L
    }

    /**
     * Returns true if media file is not empty.
     */
    fun isNotEmpty(): Boolean {
        return !isEmpty()
    }
}
