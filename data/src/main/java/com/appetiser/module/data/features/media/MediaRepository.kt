package com.appetiser.module.data.features.media

import com.appetiser.module.domain.models.user.MediaFile
import io.reactivex.Single

interface MediaRepository {
    fun uploadMedia(filePath: String): Single<MediaFile>
}
