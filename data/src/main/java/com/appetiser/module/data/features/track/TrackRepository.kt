package com.appetiser.module.data.features.track

import com.appetiser.module.domain.models.track.TrackResult
import io.reactivex.Single

interface TrackRepository {
    fun getTracks(): Single<TrackResult>

    fun getDateFromPref(): String

    fun saveDateFromPref(date: String)
}
