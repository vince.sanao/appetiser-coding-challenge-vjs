package com.appetiser.module.data.features.session

import com.appetiser.module.domain.models.session.Session
import io.reactivex.Completable
import io.reactivex.Single

interface SessionRepository {
    fun getSession(): Single<Session>

    fun saveSession(session: Session): Single<Session>

    fun clearSession(): Completable
}
