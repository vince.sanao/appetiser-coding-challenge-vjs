package com.appetiser.module.data.features.track

import com.appetiser.module.domain.models.track.TrackResult
import com.appetiser.module.local.features.track.TrackLocalSource
import com.appetiser.module.network.features.track.TrackRemoteSource
import io.reactivex.Single
import javax.inject.Inject

class TrackRepositoryImpl @Inject constructor(
    private val trackRemoteSource: TrackRemoteSource,
    private val trackLocalSource: TrackLocalSource
) : TrackRepository {

    override fun getTracks(): Single<TrackResult> {
        return trackRemoteSource.getTracks()
    }

    override fun getDateFromPref(): String {
        return trackLocalSource.getDateFromPref()
    }

    override fun saveDateFromPref(date: String) {
        return trackLocalSource.saveDateFromPref(date)
    }
}
