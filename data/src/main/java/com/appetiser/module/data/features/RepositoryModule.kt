package com.appetiser.module.data.features

import com.appetiser.module.data.features.auth.AuthRepository
import com.appetiser.module.data.features.auth.AuthRepositoryImpl
import com.appetiser.module.data.features.media.MediaRepository
import com.appetiser.module.data.features.media.MediaRepositoryImpl
import com.appetiser.module.data.features.miscellaneous.MiscellaneousRepository
import com.appetiser.module.data.features.miscellaneous.MiscellaneousRepositoryImpl
import com.appetiser.module.data.features.session.SessionRepository
import com.appetiser.module.data.features.session.SessionRepositoryImpl
import com.appetiser.module.data.features.track.TrackRepository
import com.appetiser.module.data.features.track.TrackRepositoryImpl
import com.appetiser.module.data.features.user.UserRepository
import com.appetiser.module.data.features.user.UserRepositoryImpl
import com.appetiser.module.local.features.session.SessionLocalSource
import com.appetiser.module.local.features.track.TrackLocalSource
import com.appetiser.module.network.features.auth.AuthRemoteSource
import com.appetiser.module.network.features.media.MediaRemoteSource
import com.appetiser.module.network.features.miscellaneous.MiscellaneousRemoteSource
import com.appetiser.module.network.features.track.TrackRemoteSource
import com.appetiser.module.network.features.user.UserRemoteSource
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RepositoryModule {

    @Provides
    @Singleton
    fun providesAuthRepository(
        authRemoteSource: AuthRemoteSource,
        sessionLocalSource: SessionLocalSource,
        userRemoteSource: UserRemoteSource
    ): AuthRepository {
        return AuthRepositoryImpl(authRemoteSource, sessionLocalSource, userRemoteSource)
    }

    @Provides
    @Singleton
    fun providesSessionRepository(
        sessionLocalSource: SessionLocalSource
    ): SessionRepository {
        return SessionRepositoryImpl(sessionLocalSource)
    }

    @Provides
    @Singleton
    fun providesUserRepository(
        sessionLocalSource: SessionLocalSource,
        userRemoteSource: UserRemoteSource
    ): UserRepository {
        return UserRepositoryImpl(sessionLocalSource, userRemoteSource)
    }

    @Provides
    @Singleton
    fun providesMiscellaneousRepository(
        miscellaneousRemoteSource: MiscellaneousRemoteSource,
        sessionLocalSource: SessionLocalSource
    ): MiscellaneousRepository {
        return MiscellaneousRepositoryImpl(miscellaneousRemoteSource, sessionLocalSource)
    }

    @Provides
    @Singleton
    fun providesMediaRepository(
        sessionLocalSource: SessionLocalSource,
        mediaRemoteSource: MediaRemoteSource
    ): MediaRepository {
        return MediaRepositoryImpl(sessionLocalSource, mediaRemoteSource)
    }

    @Provides
    @Singleton
    fun providesTrackRepository(
        trackRemoteSource: TrackRemoteSource,
        trackLocalSource: TrackLocalSource
    ): TrackRepository {
        return TrackRepositoryImpl(trackRemoteSource, trackLocalSource)
    }
}
