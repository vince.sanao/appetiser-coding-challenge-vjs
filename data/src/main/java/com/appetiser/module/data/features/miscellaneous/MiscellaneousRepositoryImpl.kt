package com.appetiser.module.data.features.miscellaneous

import com.appetiser.module.domain.models.miscellaneous.UpdateGate
import com.appetiser.module.local.features.session.SessionLocalSource
import com.appetiser.module.network.features.miscellaneous.MiscellaneousRemoteSource
import io.reactivex.Single
import javax.inject.Inject

class MiscellaneousRepositoryImpl @Inject constructor(
    private val miscellaneousRemoteSource: MiscellaneousRemoteSource,
    private val sessionLocalSource: SessionLocalSource
) : MiscellaneousRepository {

    override fun versionCheck(appVersion: String): Single<UpdateGate> {
        return miscellaneousRemoteSource
            .versionCheck(appVersion)
            .flatMap { updateGate ->
                sessionLocalSource
                    .getSession()
                    .flatMap { session ->
                        if (updateGate.id == UpdateGate.EMPTY_ID) {
                            session.updateGate = null
                        } else {
                            session.updateGate = updateGate
                        }

                        sessionLocalSource
                            .saveSession(session)
                    }
                    .map { updateGate }
            }
    }
}
