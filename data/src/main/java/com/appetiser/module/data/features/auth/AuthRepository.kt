package com.appetiser.module.data.features.auth

import com.appetiser.module.domain.models.CountryCode
import com.appetiser.module.domain.models.auth.UsernameCheck
import com.appetiser.module.domain.models.session.Session
import com.appetiser.module.domain.models.user.User
import io.reactivex.Completable
import io.reactivex.Single

interface AuthRepository {

    /**
     * Checks if username has an existing account in backend.
     *
     * @param username email or phone number
     */
    fun checkUsername(username: String): Single<UsernameCheck>

    fun loginWithEmail(username: String, password: String): Single<Session>

    fun loginWithMobile(username: String, otp: String): Single<Session>

    fun setOnBoardingEmailAddress(email: String): Single<User>

    /**
     * Call this method to login via social media.
     *
     * @param accessToken
     * @param accessTokenProvider provider of access token ["facebook", "google"].
     */
    fun socialLogin(
        accessToken: String,
        accessTokenProvider: String
    ): Single<Session>

    fun registerWithEmail(
        email: String = "",
        password: String = "",
        confirmPassword: String = "",
        firstName: String = "",
        lastName: String = ""
    ): Single<Session>

    /**
     * If user uses phone number for logging in. Pass only [phone].
     */
    fun registerWithMobile(
        phone: String = "",
        otp: String = ""
    ): Single<Session>

    fun verifyAccountEmail(code: String): Single<Boolean>

    fun verifyAccountMobilePhone(code: String): Single<Boolean>

    fun resendEmailVerificationCode(): Single<Boolean>

    fun resendMobilePhoneVerificationCode(): Single<Boolean>

    fun forgotPassword(username: String): Single<Boolean>

    fun forgotPasswordCheckCode(username: String, code: String): Single<Boolean>

    fun newPassword(username: String, token: String, password: String, confirmPassword: String): Single<Boolean>

    fun changePassword(
        oldPassword: String,
        newPassword: String,
        newPasswordConfirm: String
    ): Single<Boolean>

    /**
     * Verifies if password is correct.
     *
     * @param password
     * @return verification token to use in changing email/phone
     */
    fun verifyPassword(
        password: String
    ): Single<String>

    /**
     * Verifies if otp is correct.
     *
     * @param otp
     * @return verification token to use in changing phone number
     */
    fun verifyPhoneNumber(otp: String): Single<String>

    /**
     * Requests changing of email.
     *
     * @param verificationToken token returned from [verifyPassword]
     * @param newEmail new email
     */
    fun requestChangeEmail(
        verificationToken: String?,
        newEmail: String
    ): Completable

    /**
     * Verifies changing of email.
     *
     * @param verificationToken token returned from [verifyPassword]
     * @param code verification code
     */
    fun verifyChangeEmail(
        verificationToken: String,
        code: String
    ): Completable

    /**
     * Requests changing of phone number.
     *
     * @param verificationToken token returned from [verifyPassword]
     * @param newPhoneNumber new phone number
     */
    fun requestChangePhone(
        verificationToken: String,
        newPhoneNumber: String
    ): Completable

    /**
     * Verifies changing of phone number.
     *
     * @param verificationToken token returned from [verifyPassword]
     * @param code verification code
     */
    fun verifyChangePhone(
        verificationToken: String,
        code: String
    ): Completable

    fun getCountryCode(countryCodes: List<Int>): Single<List<CountryCode>>

    fun logout(): Completable

    /**
     * Deletes user account.
     *
     * @param verificationToken token returned from [verifyPassword]
     */
    fun deleteAccount(
        verificationToken: String
    ): Completable

    /**
     * Generate OTP if user doesn't exists!
     */
    fun generateOtp(phoneNumber: String): Single<Boolean>

    fun onBoardingCompleted(): Single<Session>
}
