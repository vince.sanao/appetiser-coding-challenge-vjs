package com.appetiser.module.data.features.auth

import com.appetiser.module.domain.models.CountryCode
import com.appetiser.module.domain.models.auth.UsernameCheck
import com.appetiser.module.domain.models.session.Session
import com.appetiser.module.domain.models.token.AccessToken
import com.appetiser.module.domain.models.user.User
import com.appetiser.module.local.features.session.SessionLocalSource
import com.appetiser.module.network.features.auth.AuthRemoteSource
import com.appetiser.module.network.features.user.UserRemoteSource
import io.reactivex.Completable
import io.reactivex.Single
import retrofit2.HttpException
import timber.log.Timber
import javax.inject.Inject

class AuthRepositoryImpl @Inject constructor(
    private val authRemoteSource: AuthRemoteSource,
    private val sessionLocalSource: SessionLocalSource,
    private val userRemoteSource: UserRemoteSource
) : AuthRepository {

    override fun checkUsername(username: String): Single<UsernameCheck> = authRemoteSource.checkUsername(username)

    override fun loginWithEmail(username: String, password: String): Single<Session> {
        return authRemoteSource
            .loginWithEmail(username, password)
            .flatMap(::saveAuthDataToSession)
    }

    override fun loginWithMobile(username: String, otp: String): Single<Session> {
        return authRemoteSource
            .loginWithMobile(username, otp)
            .flatMap(::saveAuthDataToSession)
    }

    override fun setOnBoardingEmailAddress(email: String): Single<User> {
        return sessionLocalSource
            .getSession()
            .flatMap { session ->
                authRemoteSource.setOnBoardingEmailAddress(session.accessToken.bearerToken, email)
                    .flatMap { user ->
                        session.user = user

                        sessionLocalSource
                            .saveSession(session)
                            .map {
                                user
                            }
                    }
            }
    }

    /**
     * Call this method to login via social media.
     *
     * @param accessToken
     * @param accessTokenProvider provider of access token ["facebook", "google"].
     */
    override fun socialLogin(
        accessToken: String,
        accessTokenProvider: String
    ): Single<Session> {
        return authRemoteSource
            .socialLogin(accessToken, accessTokenProvider)
            .flatMap(::saveAuthDataToSession)
    }

    override fun registerWithEmail(email: String, password: String, confirmPassword: String, firstName: String, lastName: String): Single<Session> {
        return authRemoteSource
            .register(
                email = email,
                password = password,
                confirmPassword = confirmPassword,
                firstName = firstName,
                lastName = lastName
            )
            .flatMap(::saveAuthDataToSession)
    }

    override fun registerWithMobile(phone: String, otp: String): Single<Session> {
        return authRemoteSource
            .register(
                phone = phone,
                otp = otp
            )
            .flatMap(::saveAuthDataToSession)
            .doOnError {
                Timber.e("Error $it")
            }
    }

    private fun saveAuthDataToSession(pair: Pair<User, AccessToken>): Single<Session> {
        val (user, accessToken) = pair

        return sessionLocalSource
            .getSession()
            .flatMap { session ->
                session.user = user
                session.accessToken = accessToken

                sessionLocalSource
                    .saveSession(session)
            }
    }

    override fun verifyAccountEmail(code: String): Single<Boolean> {
        return sessionLocalSource
            .getSession()
            .flatMap { session ->
                authRemoteSource
                    .verifyAccountEmail(
                        session.accessToken,
                        code
                    )
            }
            .flatMap { (isSuccessful, user) ->
                if (isSuccessful) {
                    // If verification succeeds. Save user to database.
                    sessionLocalSource
                        .getSession()
                        .flatMap { session ->
                            session.user = user

                            sessionLocalSource
                                .saveSession(session)
                        }
                        .map { isSuccessful }
                } else {
                    Single.just(isSuccessful)
                }
            }
    }

    override fun verifyAccountMobilePhone(code: String): Single<Boolean> {
        return sessionLocalSource
            .getSession()
            .flatMap { session ->
                authRemoteSource
                    .verifyAccountMobilePhone(
                        session.accessToken,
                        code
                    )
            }
            .flatMap { (isSuccessful, user) ->
                if (isSuccessful) {
                    // If verification succeeds. Save user to database.
                    sessionLocalSource
                        .getSession()
                        .flatMap { session ->
                            session.user = user

                            sessionLocalSource
                                .saveSession(session)
                        }
                        .map { isSuccessful }
                } else {
                    Single.just(isSuccessful)
                }
            }
    }

    override fun resendEmailVerificationCode(): Single<Boolean> {
        return sessionLocalSource
            .getSession()
            .flatMap { session ->
                authRemoteSource
                    .resendEmailVerificationCode(
                        session.accessToken
                    )
            }
    }

    override fun resendMobilePhoneVerificationCode(): Single<Boolean> {
        return sessionLocalSource
            .getSession()
            .flatMap { session ->
                authRemoteSource
                    .resendMobilePhoneVerificationCode(
                        session.accessToken
                    )
            }
    }

    override fun forgotPassword(username: String): Single<Boolean> {
        return authRemoteSource.forgotPassword(username)
    }

    override fun forgotPasswordCheckCode(username: String, code: String): Single<Boolean> {
        return authRemoteSource.forgotPasswordCheckCode(username, code)
    }

    override fun newPassword(
        username: String,
        token: String,
        password: String,
        confirmPassword: String
    ): Single<Boolean> {
        return authRemoteSource.newPassword(username, token, password, confirmPassword)
    }

    override fun changePassword(
        oldPassword: String,
        newPassword: String,
        newPasswordConfirm: String
    ): Single<Boolean> {
        return sessionLocalSource
            .getSession()
            .flatMap { session ->
                authRemoteSource.changePassword(
                    session.accessToken,
                    oldPassword,
                    newPassword,
                    newPasswordConfirm
                )
            }
    }

    override fun verifyPassword(password: String): Single<String> {
        return sessionLocalSource
            .getSession()
            .flatMap { session ->
                authRemoteSource
                    .verifyPassword(
                        session.accessToken,
                        password
                    )
            }
    }

    override fun verifyPhoneNumber(otp: String): Single<String> {
        return sessionLocalSource
            .getSession()
            .flatMap { session ->
                authRemoteSource
                    .verifyPhoneNumber(
                        session.accessToken,
                        otp
                    )
            }
    }

    override fun requestChangeEmail(
        verificationToken: String?,
        newEmail: String
    ): Completable {
        return sessionLocalSource
            .getSession()
            .flatMapCompletable { session ->
                authRemoteSource
                    .requestChangeEmail(
                        session.accessToken,
                        verificationToken,
                        newEmail
                    )
            }
    }

    override fun verifyChangeEmail(
        verificationToken: String,
        code: String
    ): Completable {
        return sessionLocalSource
            .getSession()
            .flatMap { session ->
                authRemoteSource
                    .verifyChangeEmail(
                        session.accessToken,
                        verificationToken,
                        code
                    )
                    .andThen(Single.just(session))
            }
            .flatMap { session ->
                // Get updated user after successfully changing email.
                userRemoteSource
                    .getUser(
                        session.accessToken
                    )
                    .flatMap { user ->
                        session.user = user
                        sessionLocalSource.saveSession(session)
                    }
            }
            .ignoreElement()
    }

    override fun requestChangePhone(
        verificationToken: String,
        newPhoneNumber: String
    ): Completable {
        return sessionLocalSource
            .getSession()
            .flatMapCompletable { session ->
                authRemoteSource
                    .requestChangePhone(
                        session.accessToken,
                        verificationToken,
                        newPhoneNumber
                    )
            }
    }

    override fun verifyChangePhone(verificationToken: String, code: String): Completable {
        return sessionLocalSource
            .getSession()
            .flatMap { session ->
                authRemoteSource
                    .verifyChangePhone(
                        session.accessToken,
                        verificationToken,
                        code
                    )
                    .andThen(Single.just(session))
            }
            .flatMap { session ->
                // Get updated user after successfully changing phone number.
                userRemoteSource
                    .getUser(
                        session.accessToken
                    )
                    .flatMap { user ->
                        session.user = user
                        sessionLocalSource.saveSession(session)
                    }
            }
            .ignoreElement()
    }

    override fun logout(): Completable {
        return sessionLocalSource
            .getSession()
            .flatMapCompletable { session ->
                authRemoteSource
                    .logout(session.accessToken)
            }
            .onErrorResumeNext {
                when (it) {
                    is HttpException -> {
                        if (it.code() == 401) {
                            return@onErrorResumeNext Completable.complete()
                        }
                    }
                }
                return@onErrorResumeNext Completable.error(it)
            }
            .andThen(sessionLocalSource.clearSession())
    }

    override fun deleteAccount(verificationToken: String): Completable {
        return sessionLocalSource
            .getSession()
            .flatMapCompletable { session ->
                authRemoteSource
                    .deleteAccount(
                        session.accessToken,
                        verificationToken
                    )
            }
            .andThen(sessionLocalSource.clearSession())
    }

    override fun generateOtp(phoneNumber: String): Single<Boolean> {
        return sessionLocalSource
            .getSession()
            .flatMap { session ->
                authRemoteSource
                    .generateOtp(session.accessToken, phoneNumber)
            }
    }

    override fun getCountryCode(countryCodes: List<Int>): Single<List<CountryCode>> {
        return authRemoteSource.getCountryCode(countryCodes)
    }

    override fun onBoardingCompleted(): Single<Session> {
        return sessionLocalSource
            .getSession()
            .flatMap { session ->
                authRemoteSource
                    .onBoardingCompleted(session.accessToken)
                    .flatMap {
                        saveAuthDataToSession(Pair(it, session.accessToken))
                    }
            }
    }
}
