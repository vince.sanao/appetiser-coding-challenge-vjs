package com.appetiser.module.network.features.auth.models.response

import com.appetiser.module.network.base.response.BaseResponse

data class AuthDataResponse(val data: UserDataResponse) : BaseResponse()
