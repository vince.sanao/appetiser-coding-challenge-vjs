package com.appetiser.module.network.features

import com.appetiser.module.network.BaseplateApiServices
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
class ApiServiceModule {

    @Provides
    @Singleton
    fun providesBaseplateApiServices(retrofit: Retrofit): BaseplateApiServices =
        retrofit.create(BaseplateApiServices::class.java)
}
