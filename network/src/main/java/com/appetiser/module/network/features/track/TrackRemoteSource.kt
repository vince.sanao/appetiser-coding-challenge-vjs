package com.appetiser.module.network.features.track

import com.appetiser.module.domain.models.track.TrackResult
import io.reactivex.Single

interface TrackRemoteSource {
    fun getTracks(): Single<TrackResult>
}
