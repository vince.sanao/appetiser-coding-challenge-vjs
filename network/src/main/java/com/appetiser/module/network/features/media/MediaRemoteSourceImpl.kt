package com.appetiser.module.network.features.media

import com.appetiser.module.domain.models.token.AccessToken
import com.appetiser.module.domain.models.user.MediaFile
import com.appetiser.module.network.base.BaseRemoteSource
import com.appetiser.module.network.BaseplateApiServices
import com.appetiser.module.network.features.user.models.MediaFileDTO
import io.reactivex.Single
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import java.io.File
import javax.inject.Inject

class MediaRemoteSourceImpl @Inject constructor(
    private val apiServices: BaseplateApiServices
) : BaseRemoteSource(), MediaRemoteSource {

    override fun uploadMedia(accessToken: AccessToken, filePath: String): Single<MediaFile> {
        val imageFile = File(filePath)
        val requestImageBody = imageFile.asRequestBody("image/*".toMediaTypeOrNull())
        val requestBody =
            MultipartBody.Part.createFormData(
                "file",
                imageFile.name,
                requestImageBody
            )

        return apiServices
            .uploadMedia(
                accessToken.bearerToken,
                requestBody
            )
            .map { MediaFileDTO.toDomain(it.data) }
    }
}
