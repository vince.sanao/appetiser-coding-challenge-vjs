package com.appetiser.module.network.features.auth.models

import com.google.gson.annotations.SerializedName

class RawCountryCode(
    val id: Long = 0,
    val name: String = "",
    @field:SerializedName("calling_code")
    val callingCode: String = "",
    val flag: String? = ""
)
