package com.appetiser.module.network.features.miscellaneous.models

import com.appetiser.module.domain.models.miscellaneous.UpdateGate
import com.google.gson.annotations.SerializedName

data class UpdateGateDTO(
    @SerializedName("id") val id: Int,
    @SerializedName("platform") val platform: String,
    @SerializedName("version") val version: String,
    @SerializedName("upgrade_guide") val upgradeGuide: String,
    @SerializedName("title") val title: String?,
    @SerializedName("message") val message: String?,
    @SerializedName("store_url") val storeUrl: String
) {
    companion object {
        private const val UPGRADE_GUIDE_REQUIRED = "required"
        private const val UPGRADE_GUIDE_RECOMMENDED = "recommended"

        fun toDomain(updateGateDto: UpdateGateDTO): UpdateGate {
            return with(updateGateDto) {
                UpdateGate(
                    id = id,
                    version = version,
                    isRequired = upgradeGuide == UPGRADE_GUIDE_REQUIRED,
                    title = title.orEmpty(),
                    message = message.orEmpty(),
                    storeUrl = storeUrl
                )
            }
        }
    }
}
