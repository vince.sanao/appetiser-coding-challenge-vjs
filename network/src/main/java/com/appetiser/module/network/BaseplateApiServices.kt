package com.appetiser.module.network

import com.appetiser.module.network.base.response.BaseResponse
import com.appetiser.module.network.base.response.BaseResponseNew
import com.appetiser.module.network.features.auth.models.UsernameCheckDTO
import com.appetiser.module.network.features.auth.models.response.*
import com.appetiser.module.network.features.miscellaneous.models.UpdateGateDTO
import com.appetiser.module.network.features.profile.models.response.ProfileDataResponse
import com.appetiser.module.network.features.track.models.TrackResultDTO
import com.appetiser.module.network.features.user.models.MediaFileDTO
import com.appetiser.module.network.features.user.models.UserDTO
import io.reactivex.Completable
import io.reactivex.Single
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.*

interface BaseplateApiServices {

    @GET("search?term=star&amp;country=au&amp;media=movie&amp;all")
    fun getTracks(): Single<TrackResultDTO>

    // region Authentication
    @POST("auth/register")
    fun register(@Body registerBody: RequestBody): Single<AuthDataResponse>

    @FormUrlEncoded
    @POST("auth/login")
    fun login(@FieldMap fields: Map<String, @JvmSuppressWildcards Any>): Single<AuthDataResponse>

    @FormUrlEncoded
    @POST("auth/check-username")
    fun checkUsernameIfExists(
        @FieldMap fields: Map<String, @JvmSuppressWildcards Any>
    ): Single<BaseResponseNew<UsernameCheckDTO>>

    @POST("auth/verification/verify")
    fun verifyAccount(
        @Header("Authorization") token: String,
        @Body registerBody: RequestBody
    ): Single<VerifyEmailResponse>

    @POST("auth/onboarding/complete")
    fun onboardingComplete(
        @Header("Authorization") token: String
    ): Single<BaseResponseNew<UserDTO>>

    @FormUrlEncoded
    @POST("auth/onboarding/email")
    fun setEmail(
        @Header("Authorization") token: String,
        @Field("email") email: String
    ): Single<BaseResponse>

    @FormUrlEncoded
    @POST("auth/otp/generate")
    fun generateOTP(
        @Header("Authorization") token: String,
        @Field("phone_number") phoneNumber: String
    ): Single<BaseResponse>

    @FormUrlEncoded
    @POST("auth/onboarding/email")
    fun setOnBoardingEmail(
        @Header("Authorization") token: String,
        @Field("email") email: String
    ): Single<BaseResponseNew<UserDTO>>

    @DELETE("auth/connect/account")
    fun deletePayoutDetails(
        @Header("Authorization") token: String
    ): Completable

    @POST("auth/verification/resend")
    fun resendVerificationCode(
        @Header("Authorization") token: String,
        @Body registerBody: RequestBody
    ): Single<BaseResponse>

    @FormUrlEncoded
    @POST("auth/social")
    fun socialLogin(@FieldMap fields: Map<String, @JvmSuppressWildcards Any>): Single<AuthDataResponse>

    @GET("auth/me")
    fun getUser(
        @Header("Authorization") token: String
    ): Single<BaseResponseNew<UserDTO>>

    @PUT("auth/profile")
    fun updateUserInfo(
        @Header("Authorization") token: String,
        @Body registerBody: RequestBody
    ): Single<ProfileDataResponse>

    @POST("auth/logout")
    fun logout(@Header("Authorization") token: String): Completable

    @DELETE("auth/account")
    fun deleteAccount(
        @Header("Authorization") token: String,
        @Header("Verification-Token") verificationToken: String
    ): Completable

    @POST("auth/reset-password")
    fun resetPassword(
        @Body resetPasswordJson: RequestBody
    ): Single<BaseResponse>

    @POST("auth/forgot-password")
    fun forgotPassword(@Body emailJson: RequestBody): Single<BaseResponse>

    @POST("auth/reset-password/check")
    fun forgotPasswordCheckCode(@Body requestBody: RequestBody): Single<BaseResponse>

    @POST("auth/change/password")
    fun changePassword(
        @Header("Authorization") token: String,
        @Body requestBody: RequestBody
    ): Single<BaseResponse>

    @Multipart
    @POST("auth/profile/avatar")
    fun uploadPhoto(
        @Header("Authorization") token: String,
        @Part avatar: MultipartBody.Part? = null
    ): Single<BaseResponseNew<MediaFileDTO>>

    @POST("auth/account/verification-token")
    fun getVerificationToken(
        @Header("Authorization") token: String,
        @Body requestBody: RequestBody
    ): Single<BaseResponseNew<GetVerificationTokenResponse>>

    @POST("auth/change/email")
    fun requestChangeEmail(
        @Header("Authorization") token: String,
        @Header("Verification-Token") verificationToken: String? = null,
        @Body requestBody: RequestBody
    ): Completable

    @POST("auth/change/email/verify")
    fun verifyChangeEmail(
        @Header("Authorization") token: String,
        @Header("Verification-Token") verificationToken: String,
        @Body requestBody: RequestBody
    ): Completable

    @POST("auth/change/phone-number")
    fun requestChangePhone(
        @Header("Authorization") token: String,
        @Header("Verification-Token") verificationToken: String,
        @Body requestBody: RequestBody
    ): Completable

    @POST("auth/change/phone-number/verify")
    fun verifyChangePhone(
        @Header("Authorization") token: String,
        @Header("Verification-Token") verificationToken: String,
        @Body requestBody: RequestBody
    ): Completable
    // endregion

    // region Media
    @Multipart
    @POST("media")
    fun uploadMedia(
        @Header("Authorization") token: String,
        @Part file: MultipartBody.Part? = null
    ): Single<BaseResponseNew<MediaFileDTO>>
    // endregion

    // region User
    @GET("users/{id}")
    fun getUserById(
        @Header("Authorization") token: String,
        @Path("id") userId: Long
    ): Single<BaseResponseNew<UserDTO>>
    // endregion

    // region Miscellaneous
    /**
     *
     *  getCountryCodes(@Body body: RequestBody)
     *
     *  sample request body
     *   {
     *   "country_ids": [
     *       "608"
     *       ]
     *   }
     *
     * */
    @POST("countries")
    fun getCountryCodes(@Body body: RequestBody): Single<CountryCodeResponse>

    @POST("countries")
    fun getAllCountryCodes(): Single<CountryCodeResponse>

    @GET("app/version-check")
    fun versionCheck(
        @Query("platform") platform: String = "android",
        @Query("version") version: String
    ): Single<BaseResponseNew<UpdateGateDTO>>
    // endregion
}
