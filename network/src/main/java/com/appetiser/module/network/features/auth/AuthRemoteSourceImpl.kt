package com.appetiser.module.network.features.auth

import com.appetiser.module.domain.models.CountryCode
import com.appetiser.module.domain.models.auth.UsernameCheck
import com.appetiser.module.domain.models.token.AccessToken
import com.appetiser.module.domain.models.user.User
import com.appetiser.module.network.Stubs
import com.appetiser.module.network.base.BaseRemoteSource
import com.appetiser.module.network.BaseplateApiServices
import com.appetiser.module.network.features.auth.models.UsernameCheckDTO
import com.appetiser.module.network.features.user.models.UserDTO
import com.google.gson.Gson
import com.google.gson.JsonObject
import io.reactivex.Completable
import io.reactivex.Single
import javax.inject.Inject

class AuthRemoteSourceImpl @Inject constructor(
    private val apiServices: BaseplateApiServices,
    private val gson: Gson
) : BaseRemoteSource(), AuthRemoteSource {

    override fun logout(accessToken: AccessToken): Completable {
        return apiServices
            .logout(accessToken.bearerToken)
    }

    override fun deleteAccount(
        accessToken: AccessToken,
        verificationToken: String
    ): Completable {
        return apiServices
            .deleteAccount(
                accessToken.bearerToken,
                verificationToken
            )
    }

    override fun forgotPassword(username: String): Single<Boolean> {
        val emailJson = JsonObject()
            .apply {
                addProperty("username", username)
            }
        val requestBody = getJsonRequestBody(emailJson.toString())

        return apiServices
            .forgotPassword(requestBody)
            .map { response ->
                response.success
            }
    }

    override fun forgotPasswordCheckCode(username: String, code: String): Single<Boolean> {
        val emailJson = JsonObject()
            .apply {
                addProperty("username", username)
                addProperty("token", code)
            }

        return apiServices
            .forgotPasswordCheckCode(getJsonRequestBody(emailJson.toString()))
            .map { it.success }
    }

    override fun newPassword(username: String, token: String, password: String, confirmPassword: String): Single<Boolean> {
        val passwordJson = JsonObject()
            .apply {
                addProperty("username", username)
                addProperty("token", token)
                addProperty("password", password)
                addProperty("password_confirmation", confirmPassword)
            }

        return apiServices
            .resetPassword(getJsonRequestBody(passwordJson.toString()))
            .map { it.success }
    }

    override fun changePassword(
        accessToken: AccessToken,
        oldPassword: String,
        newPassword: String,
        newPasswordConfirm: String
    ): Single<Boolean> {
        val changePasswordJson = JsonObject()
            .apply {
                addProperty("old_password", oldPassword)
                addProperty("new_password", newPassword)
                addProperty("new_password_confirmation", newPasswordConfirm)
            }

        return apiServices
            .changePassword(
                accessToken.bearerToken,
                getJsonRequestBody(changePasswordJson.toString())
            )
            .map { it.success }
    }

    override fun checkUsername(username: String): Single<UsernameCheck> {
        val userMap = hashMapOf<String, String>()
        userMap["username"] = username

        return apiServices
            .checkUsernameIfExists(userMap)
            .map { UsernameCheckDTO.toDomain(it.data) }
    }

    override fun loginWithEmail(username: String, password: String): Single<Pair<User, AccessToken>> {
        val userMap = hashMapOf<String, String>()
        userMap["username"] = username
        userMap["password"] = password

        return apiServices
            .login(userMap)
            .map { UserDTO.mapAuthDataResponse(it) }
    }

    override fun loginWithMobile(username: String, otp: String): Single<Pair<User, AccessToken>> {
        val userMap = hashMapOf<String, String>()
        userMap["username"] = username
        userMap["otp"] = otp

        return apiServices
            .login(userMap)
            .map { UserDTO.mapAuthDataResponse(it) }
    }

    /**
     * Call this method to login via social media.
     *
     * @param accessToken
     * @param accessTokenProvider provider of access token ["facebook", "google"].
     */
    override fun socialLogin(accessToken: String, accessTokenProvider: String): Single<Pair<User, AccessToken>> {
        val map = mapOf(
            "token" to accessToken,
            "provider" to accessTokenProvider
        )

        return apiServices
            .socialLogin(map)
            .map { UserDTO.mapAuthDataResponse(it) }
    }

    override fun setOnBoardingEmailAddress(accessToken: String, email: String): Single<User> {
        return apiServices
            .setOnBoardingEmail(accessToken, email)
            .map { UserDTO.toDomain(it.data) }
    }

    override fun register(
        email: String,
        phone: String,
        password: String,
        confirmPassword: String,
        firstName: String,
        lastName: String,
        otp: String
    ): Single<Pair<User, AccessToken>> {
        val registerBody = JsonObject()
            .apply {
                if (email.isNotEmpty()) addProperty("email", email)
                if (phone.isNotEmpty()) addProperty("phone_number", phone)
                if (firstName.isNotEmpty()) addProperty("first_name", firstName)
                if (lastName.isNotEmpty()) addProperty("last_name", lastName)
                if (password.isNotEmpty()) addProperty("password", password)
                if (confirmPassword.isNotEmpty()) addProperty("password_confirmation", confirmPassword)
                if (otp.isNotEmpty()) addProperty("otp", otp)
            }

        return apiServices
            .register(getJsonRequestBody(registerBody.toString()))
            .map { UserDTO.mapAuthDataResponse(it) }
    }

    override fun verifyAccountEmail(accessToken: AccessToken, code: String): Single<Pair<Boolean, User>> {
        val jsonBody = JsonObject()
            .apply {
                addProperty("token", code)
                addProperty("via", "email")
            }
        return apiServices
            .verifyAccount(
                accessToken.bearerToken,
                getJsonRequestBody(jsonBody.toString())
            )
            .map { UserDTO.toDomain(it.user) }
            .map { Pair(it.emailVerified, it) }
    }

    override fun verifyAccountMobilePhone(accessToken: AccessToken, code: String): Single<Pair<Boolean, User>> {
        val jsonBody = JsonObject()
            .apply {
                addProperty("token", code)
                addProperty("via", "phone_number")
            }
        return apiServices
            .verifyAccount(
                accessToken.bearerToken,
                getJsonRequestBody(jsonBody.toString())
            )
            .map { UserDTO.toDomain(it.user) }
            .map { Pair(it.phoneNumberVerified, it) }
    }

    override fun resendEmailVerificationCode(accessToken: AccessToken): Single<Boolean> {
        val jsonBody = JsonObject()
            .apply {
                addProperty("via", "email")
            }
        return apiServices
            .resendVerificationCode(
                accessToken.bearerToken,
                getJsonRequestBody(jsonBody.toString())
            )
            .map {
                it.success
            }
    }

    override fun resendMobilePhoneVerificationCode(accessToken: AccessToken): Single<Boolean> {
        val jsonBody = JsonObject()
            .apply {
                addProperty("via", "phone_number")
            }
        return apiServices
            .resendVerificationCode(
                accessToken.bearerToken,
                getJsonRequestBody(jsonBody.toString())
            )
            .map {
                it.success
            }
    }

    override fun getCountryCode(countryCodes: List<Int>): Single<List<CountryCode>> {
        val jsonObject = JsonObject()

        if (countryCodes.isNotEmpty()) {
            jsonObject
                .add(
                    "calling_codes",
                    gson.toJsonTree(countryCodes.toList()).asJsonArray
                )
        }

        val countryCode =
            if (countryCodes.isNotEmpty()) {
                apiServices
                    .getCountryCodes(
                        getJsonRequestBody(jsonObject.toString())
                    )
            } else {
                apiServices.getAllCountryCodes()
            }

        return countryCode
            .toObservable()
            .flatMapIterable {
                it.data.countries
            }
            .map {
                CountryCode(
                    id = it.id,
                    name = it.name,
                    callingCode = it.callingCode,
                    flag = it.flag.orEmpty()
                )
            }
            .toList()
            .onErrorResumeNext(Single.just(Stubs.countries))
    }

    override fun verifyPassword(accessToken: AccessToken, password: String): Single<String> {
        val jsonBody = JsonObject()
            .apply {
                addProperty("password", password)
            }

        return apiServices
            .getVerificationToken(
                accessToken.bearerToken,
                getJsonRequestBody(jsonBody.toString())
            )
            .map { response ->
                response.data.token
            }
    }

    override fun verifyPhoneNumber(accessToken: AccessToken, otp: String): Single<String> {
        val jsonBody = JsonObject()
            .apply {
                addProperty("otp", otp)
            }

        return apiServices
            .getVerificationToken(
                accessToken.bearerToken,
                getJsonRequestBody(jsonBody.toString())
            )
            .map { response ->
                response.data.token
            }
    }

    override fun requestChangeEmail(
        accessToken: AccessToken,
        verificationToken: String?,
        newEmail: String
    ): Completable {
        val jsonBody = JsonObject()
            .apply {
                addProperty("email", newEmail)
            }

        return apiServices
            .requestChangeEmail(
                accessToken.bearerToken,
                verificationToken,
                getJsonRequestBody(jsonBody.toString())
            )
    }

    override fun verifyChangeEmail(
        accessToken: AccessToken,
        verificationToken: String,
        code: String
    ): Completable {
        val jsonBody = JsonObject()
            .apply {
                addProperty("token", code)
            }

        return apiServices
            .verifyChangeEmail(
                accessToken.bearerToken,
                verificationToken,
                getJsonRequestBody(jsonBody.toString())
            )
    }

    override fun requestChangePhone(
        accessToken: AccessToken,
        verificationToken: String,
        newPhoneNumber: String
    ): Completable {
        val jsonBody = JsonObject()
            .apply {
                addProperty("phone_number", newPhoneNumber)
            }

        return apiServices
            .requestChangePhone(
                accessToken.bearerToken,
                verificationToken,
                getJsonRequestBody(jsonBody.toString())
            )
    }

    override fun verifyChangePhone(accessToken: AccessToken, verificationToken: String, code: String): Completable {
        val jsonBody = JsonObject()
            .apply {
                addProperty("token", code)
            }

        return apiServices
            .verifyChangePhone(
                accessToken.bearerToken,
                verificationToken,
                getJsonRequestBody(jsonBody.toString())
            )
    }

    override fun generateOtp(accessToken: AccessToken, phoneNumber: String): Single<Boolean> {
        return apiServices
            .generateOTP(
                accessToken.bearerToken,
                phoneNumber
            )
            .map {
                it.isSuccessful()
            }
    }

    override fun onBoardingCompleted(accessToken: AccessToken): Single<User> {
        return apiServices
            .onboardingComplete(
                accessToken.bearerToken
            )
            .map {
                UserDTO.toDomain(it.data)
            }
    }
}
