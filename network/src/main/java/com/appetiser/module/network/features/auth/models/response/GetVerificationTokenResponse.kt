package com.appetiser.module.network.features.auth.models.response

data class GetVerificationTokenResponse(
    val token: String
)
