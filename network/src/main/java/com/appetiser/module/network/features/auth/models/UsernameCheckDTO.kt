package com.appetiser.module.network.features.auth.models

import com.appetiser.module.domain.models.auth.AuthType
import com.appetiser.module.domain.models.auth.UsernameCheck
import com.appetiser.module.domain.models.auth.UsernameType
import com.google.gson.annotations.SerializedName

data class UsernameCheckDTO(
    @SerializedName("username") val userName: String,
    @SerializedName("username_type") val userNameType: String,
    @SerializedName("auth_type") val authType: String,
    @SerializedName("has_password") val hasPassword: Boolean
) {
    companion object {
        fun toDomain(usernameCheckDto: UsernameCheckDTO): UsernameCheck {
            return with(usernameCheckDto) {
                UsernameCheck(
                    userName = userName,
                    userNameType = UsernameType.getUsernameTypeFromString(userNameType),
                    authType = AuthType.getAuthTypeFromString(authType),
                    hasPassword = hasPassword
                )
            }
        }
    }
}
