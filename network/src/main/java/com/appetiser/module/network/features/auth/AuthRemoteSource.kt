package com.appetiser.module.network.features.auth

import com.appetiser.module.domain.models.CountryCode
import com.appetiser.module.domain.models.auth.UsernameCheck
import com.appetiser.module.domain.models.token.AccessToken
import com.appetiser.module.domain.models.user.User
import io.reactivex.Completable
import io.reactivex.Single

interface AuthRemoteSource {
    fun logout(accessToken: AccessToken): Completable

    fun deleteAccount(
        accessToken: AccessToken,
        verificationToken: String
    ): Completable

    fun forgotPassword(username: String): Single<Boolean>

    fun forgotPasswordCheckCode(username: String, code: String): Single<Boolean>

    fun newPassword(username: String, token: String, password: String, confirmPassword: String): Single<Boolean>

    fun changePassword(
        accessToken: AccessToken,
        oldPassword: String,
        newPassword: String,
        newPasswordConfirm: String
    ): Single<Boolean>

    /**
     * Checks if username has an existing account in backend.
     *
     * @param username email or phone number
     */
    fun checkUsername(username: String): Single<UsernameCheck>

    fun loginWithEmail(username: String, password: String): Single<Pair<User, AccessToken>>

    fun loginWithMobile(username: String, otp: String): Single<Pair<User, AccessToken>>

    /**
     * Call this method to login via social media.
     *
     * @param accessToken
     * @param accessTokenProvider provider of access token ["facebook", "google"].
     */
    fun socialLogin(accessToken: String, accessTokenProvider: String): Single<Pair<User, AccessToken>>

    fun setOnBoardingEmailAddress(accessToken: String, email: String): Single<User>

    /**
     * If user uses phone number for logging in. Pass only [phone].
     */
    fun register(
        email: String = "",
        phone: String = "",
        password: String = "",
        confirmPassword: String = "",
        firstName: String = "",
        lastName: String = "",
        otp: String = ""
    ): Single<Pair<User, AccessToken>>

    /**
     * @return Returns pair of boolean true/false if verification succeeds and user session.
     */
    fun verifyAccountEmail(accessToken: AccessToken, code: String): Single<Pair<Boolean, User>>

    fun verifyAccountMobilePhone(accessToken: AccessToken, code: String): Single<Pair<Boolean, User>>

    fun resendEmailVerificationCode(accessToken: AccessToken): Single<Boolean>

    fun resendMobilePhoneVerificationCode(accessToken: AccessToken): Single<Boolean>

    fun getCountryCode(countryCodes: List<Int>): Single<List<CountryCode>>

    /**
     * Verifies if password is correct.
     *
     * @param accessToken
     * @param password
     * @return verification token to use in changing email/phone
     */
    fun verifyPassword(accessToken: AccessToken, password: String): Single<String>

    /**
     * Verifies if phoneNumber is correct.
     *
     * @param accessToken
     * @param otp
     * @return verification token to use in changing phone
     */
    fun verifyPhoneNumber(accessToken: AccessToken, otp: String): Single<String>

    /**
     * Requests changing of email.
     *
     * @param accessToken
     * @param verificationToken token returned from [verifyPassword]
     * @param newEmail new email
     */
    fun requestChangeEmail(
        accessToken: AccessToken,
        verificationToken: String?,
        newEmail: String
    ): Completable

    /**
     * Verifies changing of email.
     *
     * @param accessToken
     * @param verificationToken token returned from [verifyPassword]
     * @param code verification code
     */
    fun verifyChangeEmail(
        accessToken: AccessToken,
        verificationToken: String,
        code: String
    ): Completable

    /**
     * Requests changing of phone number.
     *
     * @param accessToken
     * @param verificationToken token returned from [verifyPassword]
     * @param newPhoneNumber new phone number
     */
    fun requestChangePhone(
        accessToken: AccessToken,
        verificationToken: String,
        newPhoneNumber: String
    ): Completable

    /**
     * Verifies changing of phone number.
     *
     * @param accessToken
     * @param verificationToken token returned from [verifyPassword]
     * @param code verification code
     */
    fun verifyChangePhone(
        accessToken: AccessToken,
        verificationToken: String,
        code: String
    ): Completable

    fun generateOtp(
        accessToken: AccessToken,
        phoneNumber: String
    ): Single<Boolean>

    fun onBoardingCompleted(
        accessToken: AccessToken
    ): Single<User>
}
