package com.appetiser.module.network.features.miscellaneous

import com.appetiser.module.domain.models.miscellaneous.UpdateGate
import com.appetiser.module.network.BaseplateApiServices
import com.appetiser.module.network.features.miscellaneous.models.UpdateGateDTO
import io.reactivex.Single
import retrofit2.HttpException
import java.net.HttpURLConnection
import javax.inject.Inject

class MiscellaneousRemoteSourceImpl @Inject constructor(
    private val apiService: BaseplateApiServices
) : MiscellaneousRemoteSource {

    override fun versionCheck(appVersion: String): Single<UpdateGate> {
        return apiService
            .versionCheck(version = appVersion)
            .map { response ->
                UpdateGateDTO.toDomain(response.data)
            }
            .onErrorResumeNext { exception ->
                val isVersionNotFound =
                    exception is HttpException &&
                        exception.code() == HttpURLConnection.HTTP_NOT_FOUND

                // If version is not found. No updates are available.
                if (isVersionNotFound) {
                    Single.just(UpdateGate.empty())
                } else {
                    Single.error(exception)
                }
            }
    }
}
