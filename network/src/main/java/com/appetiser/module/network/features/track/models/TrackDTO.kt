package com.appetiser.module.network.features.track.models

import com.appetiser.module.domain.models.track.Track
import com.google.gson.annotations.SerializedName

data class TrackDTO(
    @SerializedName("wrapperType") val wrapperType: String? = null,
    @SerializedName("kind") val kind: String? = null,
    @SerializedName("artistId") val artistId: Int? = null,
    @SerializedName("collectionId") val collectionId: Int? = null,
    @SerializedName("trackId") val trackId: Int? = null,
    @SerializedName("artistName") val artistName: String? = null,
    @SerializedName("collectionName") val collectionName: String? = null,
    @SerializedName("trackName") val trackName: String? = null,
    @SerializedName("collectionCensoredName") val collectionCensoredName: String? = null,
    @SerializedName("trackCensoredName") val trackCensoredName: String? = null,
    @SerializedName("artworkUrl100") val artworkUrl100: String? = null,
    @SerializedName("collectionPrice") val collectionPrice: Double? = null,
    @SerializedName("trackPrice") val trackPrice: Double? = null,
    @SerializedName("releaseDate") val releaseDate: String? = null,
    @SerializedName("collectionExplicitness") val collectionExplicitness: String? = null,
    @SerializedName("trackExplicitness") val trackExplicitness: String? = null,
    @SerializedName("discCount") val discCount: Int? = null,
    @SerializedName("discNumber") val discNumber: Int? = null,
    @SerializedName("trackCount") val trackCount: Int? = null,
    @SerializedName("trackNumber") val trackNumber: Int? = null,
    @SerializedName("trackTimeMillis") val trackTimeMillis: Int? = null,
    @SerializedName("country") val country: String? = null,
    @SerializedName("currency") val currency: String? = null,
    @SerializedName("primaryGenreName") val primaryGenreName: String? = null,
    @SerializedName("description") val description: String? = null,
    @SerializedName("longDescription") val longDescription: String? = null,
    @SerializedName("stampDate") val stampDate: Long? = null
) {
    companion object {
        fun toDomain(dto: TrackDTO): Track {
            return with(dto) {
                Track(
                    wrapperType = wrapperType,
                    kind = kind,
                    artistId = artistId,
                    collectionId = collectionId,
                    trackId = trackId,
                    artistName = artistName,
                    collectionName = collectionName,
                    trackName = trackName,
                    collectionCensoredName = collectionCensoredName,
                    trackCensoredName = trackCensoredName,
                    artworkUrl100 = artworkUrl100,
                    collectionPrice = collectionPrice,
                    trackPrice = trackPrice,
                    releaseDate = releaseDate,
                    collectionExplicitness = collectionExplicitness,
                    trackExplicitness = trackExplicitness,
                    discCount = discCount,
                    discNumber = discNumber,
                    trackCount = trackCount,
                    trackNumber = trackNumber,
                    trackTimeMillis = trackTimeMillis,
                    country = country,
                    currency = currency,
                    primaryGenreName = primaryGenreName,
                    description = description,
                    longDescription = longDescription,
                    stampDate = stampDate
                )
            }
        }
    }
}
