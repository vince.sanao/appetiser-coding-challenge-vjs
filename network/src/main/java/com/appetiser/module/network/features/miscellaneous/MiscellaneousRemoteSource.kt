package com.appetiser.module.network.features.miscellaneous

import com.appetiser.module.domain.models.miscellaneous.UpdateGate
import io.reactivex.Single

interface MiscellaneousRemoteSource {

    /**
     * Checks for app updates.
     *
     * @param appVersion versionName of the android app.
     */
    fun versionCheck(
        appVersion: String
    ): Single<UpdateGate>
}
