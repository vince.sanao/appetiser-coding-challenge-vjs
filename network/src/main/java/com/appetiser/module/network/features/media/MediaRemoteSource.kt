package com.appetiser.module.network.features.media

import com.appetiser.module.domain.models.token.AccessToken
import com.appetiser.module.domain.models.user.MediaFile
import io.reactivex.Single

interface MediaRemoteSource {
    fun uploadMedia(accessToken: AccessToken, filePath: String): Single<MediaFile>
}
