package com.appetiser.module.network.features.track.models

import com.appetiser.module.domain.models.track.TrackResult
import com.google.gson.annotations.SerializedName

data class TrackResultDTO(
    @SerializedName("resultCount") val resultCount: Int? = null,
    @SerializedName("results") val results: List<TrackDTO>? = null
) {
    companion object {
        fun toDomain(dto: TrackResultDTO): TrackResult {
            return with(dto) {
                TrackResult(
                    resultCount = resultCount,
                    results = results?.map {
                        TrackDTO.toDomain(it)
                    }
                )
            }
        }
    }
}
