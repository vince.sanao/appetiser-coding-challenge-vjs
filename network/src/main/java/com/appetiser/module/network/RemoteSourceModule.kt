package com.appetiser.module.network

import com.appetiser.module.network.features.auth.AuthRemoteSource
import com.appetiser.module.network.features.auth.AuthRemoteSourceImpl
import com.appetiser.module.network.features.media.MediaRemoteSource
import com.appetiser.module.network.features.media.MediaRemoteSourceImpl
import com.appetiser.module.network.features.miscellaneous.MiscellaneousRemoteSource
import com.appetiser.module.network.features.miscellaneous.MiscellaneousRemoteSourceImpl
import com.appetiser.module.network.features.track.TrackRemoteSource
import com.appetiser.module.network.features.track.TrackRemoteSourceImpl
import com.appetiser.module.network.features.user.UserRemoteSource
import com.appetiser.module.network.features.user.UserRemoteSourceImpl
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RemoteSourceModule {

    @Provides
    @Singleton
    fun providesAuthRemoteSource(
        apiServices: BaseplateApiServices,
        gson: Gson
    ): AuthRemoteSource = AuthRemoteSourceImpl(apiServices, gson)

    @Provides
    @Singleton
    fun providesUserRemoteSource(
        apiServices: BaseplateApiServices
    ): UserRemoteSource = UserRemoteSourceImpl(apiServices)

    @Provides
    @Singleton
    fun providesMiscellaneousRemoteSource(
        apiServices: BaseplateApiServices
    ): MiscellaneousRemoteSource = MiscellaneousRemoteSourceImpl(apiServices)

    @Provides
    @Singleton
    fun providesMediaRemoteSource(
        apiServices: BaseplateApiServices
    ): MediaRemoteSource = MediaRemoteSourceImpl(apiServices)

    @Provides
    @Singleton
    fun providesTrackRemoteSource(
        apiServices: BaseplateApiServices
    ): TrackRemoteSource = TrackRemoteSourceImpl(apiServices)
}
