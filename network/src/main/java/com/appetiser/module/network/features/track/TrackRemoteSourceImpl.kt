package com.appetiser.module.network.features.track

import com.appetiser.module.domain.models.track.TrackResult
import com.appetiser.module.network.BaseplateApiServices
import com.appetiser.module.network.features.track.models.TrackResultDTO
import io.reactivex.Single
import javax.inject.Inject

class TrackRemoteSourceImpl @Inject constructor(
    private val apiServices: BaseplateApiServices
) : TrackRemoteSource {

    override fun getTracks(): Single<TrackResult> {
        return apiServices.getTracks()
            .map {
                it
            }
            .map {
                TrackResultDTO.toDomain(it)
            }
    }
}
