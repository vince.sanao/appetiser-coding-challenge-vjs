package com.appetiser.module.network.features

import com.appetiser.module.domain.models.token.AccessToken
import com.appetiser.module.domain.models.user.MediaFile
import com.appetiser.module.domain.models.user.User
import com.appetiser.module.network.base.response.BaseResponse
import com.appetiser.module.network.features.auth.models.RawCountryCode
import com.appetiser.module.network.features.auth.models.response.*
import com.appetiser.module.network.features.profile.models.response.ProfileDataResponse
import com.appetiser.module.network.features.user.models.UserDTO

object Stubs {
    val BASE_RESPONSE_SUCCESSFUL = BaseResponse(success = true, message = "success", http_status = 200)
    val BASE_RESPONSE_FAILED = BaseResponse(success = false, message = "failed", http_status = 401)

    val VERIFY_EMAIL_RESPONSE_SUCCESSFUL = VerifyEmailResponse(
        UserDTO(
            phoneNumber = "+639435643214",
            emailVerified = true,
            phoneNumberVerified = true,
            verified = true,
            email = "josemarichan@gmail.com"
        )
    )

    val VERIFY_EMAIL_RESPONSE_FAILED = VerifyEmailResponse(
        UserDTO(
            phoneNumber = "+639435643214",
            emailVerified = false,
            phoneNumberVerified = false,
            verified = false,
            email = "josemarichan@gmail.com"
        )
    )

    val USER_SESSION_DTO = UserDTO(
        phoneNumber = "+639435643214",
        emailVerified = true,
        phoneNumberVerified = true,
        verified = true,
        email = "josemarichan@gmail.com",
        onBoardedAt = "2021-03-28T18:34:16.000000Z"
    ).apply {
        this.createdAt = "2021-03-28T18:34:16.000000Z"
        this.updatedAt = "2021-03-28T18:34:16.000000Z"
    }

    val PROFILE_DATA_RESPONSE = ProfileDataResponse(USER_SESSION_DTO)

    val ACCESS_TOKEN = AccessToken(
        token = "12345678",
        tokenType = "token_type",
        refresh = "refresh",
        expiresIn = "2020-03-11T10:47:31+0000"
    )

    val USER_SESSION = User(
        id = "1231232132123",
        fullName = "Jose Mari Chan",
        firstName = "Jose Mari",
        lastName = "Chan",
        email = "josemarichan@gmail.com",
        avatarPermanentThumbUrl = "http://www.google.com/",
        avatarPermanentUrl = "http://www.google.com/",
        phoneNumber = "+639435643214",
        emailVerified = true,
        phoneNumberVerified = true,
        verified = true,
        avatar = MediaFile.empty(),
        description = "description",
        birthDate = "12/28/1993"
    )

    val USER_DATA_RESPONSE = UserDataResponse(
        user = USER_SESSION_DTO,
        accessToken = ACCESS_TOKEN.token ?: "",
        tokenType = ACCESS_TOKEN.tokenType,
        expiresIn = ACCESS_TOKEN.expiresIn
    )

    val AUTH_DATA_RESPONSE = AuthDataResponse(USER_DATA_RESPONSE)

    val COUNTRY_CODE_DATA_RESPONSE = CountryCodeDataResponse(
        listOf(
            RawCountryCode(1, "PH", "63", "/images/flags/PH.png"),
            RawCountryCode(2, "AU", "61", "/images/flags/AU.png")
        )
    )

    val COUNTRY_CODE_RESPONSE = CountryCodeResponse(COUNTRY_CODE_DATA_RESPONSE)
}
