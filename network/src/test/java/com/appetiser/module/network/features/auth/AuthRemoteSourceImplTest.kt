package com.appetiser.module.network.features.auth

import com.appetiser.module.domain.models.auth.AuthType
import com.appetiser.module.domain.models.auth.UsernameType
import com.appetiser.module.domain.utils.any
import com.appetiser.module.network.base.response.BaseResponseNew
import com.appetiser.module.network.BaseplateApiServices
import com.appetiser.module.network.features.Stubs
import com.appetiser.module.network.features.auth.models.UsernameCheckDTO
import com.appetiser.module.network.features.user.models.UserDTO
import com.google.gson.Gson
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.*

class AuthRemoteSourceImplTest {

    private val mockApi = mock(BaseplateApiServices::class.java)

    private lateinit var sut: AuthRemoteSource

    @Before
    fun setUp() {
        sut = AuthRemoteSourceImpl(mockApi, Gson())
    }

    @Test
    fun forgotPassword_ShouldReturnTrue_WhenResponseIsSuccessful() {
        val email = "john@gmail.com"
        val response = Stubs.BASE_RESPONSE_SUCCESSFUL

        `when`(mockApi.forgotPassword(any()))
            .thenReturn(Single.just(response))

        sut.forgotPassword(email)
            .test()
            .assertComplete()
            .assertValue { it }

        verify(mockApi, times(1)).forgotPassword(any())
    }

    @Test
    fun forgotPassword_ShouldReturnFalse_WhenResponseHasFailed() {
        val email = "john@gmail.com"
        val response = Stubs.BASE_RESPONSE_FAILED

        `when`(mockApi.forgotPassword(any()))
            .thenReturn(
                Single.just(response)
            )

        sut.forgotPassword(email)
            .test()
            .assertComplete()
            .assertValue { !it }

        verify(mockApi, times(1)).forgotPassword(any())
    }

    @Test
    fun forgotPasswordCheckCode_ShouldReturnTrue_WhenResponseIsSuccessful() {
        val email = "john@gmail.com"
        val code = "123456"
        val response = Stubs.BASE_RESPONSE_SUCCESSFUL

        `when`(mockApi.forgotPasswordCheckCode(any()))
            .thenReturn(Single.just(response))

        sut.forgotPasswordCheckCode(email, code)
            .test()
            .assertComplete()
            .assertValue { it }

        verify(mockApi, times(1)).forgotPasswordCheckCode(any())
    }

    @Test
    fun forgotPasswordCheckCode_ShouldReturnFalse_WhenResponseHasFailed() {
        val email = "john@gmail.com"
        val code = "123456"
        val response = Stubs.BASE_RESPONSE_FAILED

        `when`(mockApi.forgotPasswordCheckCode(any()))
            .thenReturn(
                Single.just(response)
            )

        sut.forgotPasswordCheckCode(email, code)
            .test()
            .assertComplete()
            .assertValue { !it }

        verify(mockApi, times(1)).forgotPasswordCheckCode(any())
    }

    @Test
    fun newPassword_ShouldReturnTrue_WhenResponseIsSuccessful() {
        val email = "john@gmail.com"
        val token = "123456"
        val password = "123456"
        val confirmPassword = "123456"
        val response = Stubs.BASE_RESPONSE_SUCCESSFUL

        `when`(mockApi.resetPassword(any()))
            .thenReturn(Single.just(response))

        sut.newPassword(email, token, password, confirmPassword)
            .test()
            .assertComplete()
            .assertValue { it }

        verify(mockApi, times(1)).resetPassword(any())
    }

    @Test
    fun newPassword_ShouldReturnFalse_WhenResponseHasFailed() {
        val email = "john@gmail.com"
        val token = "123456"
        val password = "123456"
        val confirmPassword = "123456"
        val response = Stubs.BASE_RESPONSE_FAILED

        `when`(mockApi.resetPassword(any()))
            .thenReturn(Single.just(response))

        sut.newPassword(email, token, password, confirmPassword)
            .test()
            .assertComplete()
            .assertValue { !it }

        verify(mockApi, times(1)).resetPassword(any())
    }

    @Test
    fun checkEmail_ShouldMapToDomain_WhenExecuted() {
        val email = "john@gmail.com"
        val response =
            BaseResponseNew(
                data = UsernameCheckDTO(
                    email,
                    UsernameType.Email.value,
                    AuthType.Password.value,
                    true
                )
            )
        val expectedValue = UsernameCheckDTO.toDomain(response.data)

        `when`(mockApi.checkUsernameIfExists(any()))
            .thenReturn(Single.just(response))

        sut.checkUsername(email)
            .test()
            .assertComplete()
            .assertValue { it == expectedValue }

        verify(mockApi, times(1)).checkUsernameIfExists(any())
    }

    @Test
    fun login_ShouldReturnMappedAuthDataResponse() {
        val username = "jose"
        val password = "12345678"
        val response = Stubs.AUTH_DATA_RESPONSE
        val expected = UserDTO.mapAuthDataResponse(response)

        `when`(mockApi.login(any()))
            .thenReturn(Single.just(response))

        sut.loginWithEmail(username, password)
            .test()
            .assertComplete()
            .assertValue { it == expected }

        verify(mockApi, times(1)).login(any())
    }

    @Test
    fun socialLogin_ShouldReturnMappedAuthDataResponse() {
        val accessToken = Stubs.ACCESS_TOKEN.token!!
        val accessTokenProvider = "google"
        val response = Stubs.AUTH_DATA_RESPONSE
        val expected = UserDTO.mapAuthDataResponse(response)

        `when`(mockApi.socialLogin(any()))
            .thenReturn(Single.just(response))

        sut.socialLogin(accessToken, accessTokenProvider)
            .test()
            .assertComplete()
            .assertValue { it == expected }

        verify(mockApi, times(1)).socialLogin(any())
    }

    @Test
    fun register_ShouldReturnMappedAuthDataResponse() {
        val email = Stubs.USER_SESSION.email
        val password = "12345678"
        val confirmPassword = "12345678"
        val mobileNumber = Stubs.USER_SESSION.phoneNumber
        val firstName = Stubs.USER_SESSION.firstName!!
        val lastName = Stubs.USER_SESSION.lastName!!
        val response = Stubs.AUTH_DATA_RESPONSE
        val expected = UserDTO.mapAuthDataResponse(response)

        `when`(mockApi.register(any()))
            .thenReturn(Single.just(response))

        sut
            .register(
                email = email,
                phone = mobileNumber,
                password = password,
                confirmPassword = confirmPassword,
                firstName = firstName,
                lastName = lastName
            )
            .test()
            .assertComplete()
            .assertValue { it == expected }

        verify(mockApi, times(1)).register(any())
    }

    @Test
    fun resendEmailVerificationCode_ShouldReturnTrue_WhenResponseIsSuccessful() {
        val accessToken = Stubs.ACCESS_TOKEN

        `when`(mockApi.resendVerificationCode(any(), any()))
            .thenReturn(Single.just(Stubs.BASE_RESPONSE_SUCCESSFUL))

        sut
            .resendEmailVerificationCode(accessToken)
            .test()
            .assertComplete()
            .assertValue { it }

        verify(mockApi, times(1)).resendVerificationCode(any(), any())
    }

    @Test
    fun resendEmailVerificationCode_ShouldReturnFalse_WhenResponseHasFailed() {
        val accessToken = Stubs.ACCESS_TOKEN

        `when`(mockApi.resendVerificationCode(any(), any()))
            .thenReturn(Single.just(Stubs.BASE_RESPONSE_FAILED))

        sut
            .resendEmailVerificationCode(accessToken)
            .test()
            .assertComplete()
            .assertValue { !it }

        verify(mockApi, times(1)).resendVerificationCode(any(), any())
    }

    @Test
    fun getCountryCode_ShouldGetSpecificCountryCodes_WhenCountryCodesArgumentIsNotEmpty() {
        val response = Stubs.COUNTRY_CODE_RESPONSE
        val countryCodes = listOf(1, 2, 3)

        `when`(mockApi.getCountryCodes(any()))
            .thenReturn(Single.just(response))

        sut
            .getCountryCode(countryCodes)
            .test()
            .assertComplete()

        verify(mockApi, times(1)).getCountryCodes(any())
    }

    @Test
    fun getCountryCode_ShouldGetAllCountryCodes_WhenCountryCodesArgumentIsEmpty() {
        val response = Stubs.COUNTRY_CODE_RESPONSE
        val countryCodes = emptyList<Int>()

        `when`(mockApi.getAllCountryCodes())
            .thenReturn(Single.just(response))

        sut
            .getCountryCode(countryCodes)
            .test()
            .assertComplete()

        verify(mockApi, times(1)).getAllCountryCodes()
    }

    @Test
    fun getCountryCode_ShouldReturnStubCountries_WhenErrorHappens() {
        val countryCodes = emptyList<Int>()
        val expected = com.appetiser.module.network.Stubs.countries

        `when`(mockApi.getAllCountryCodes())
            .thenReturn(Single.error(Exception("Mock exception.")))

        sut
            .getCountryCode(countryCodes)
            .test()
            .assertComplete()
            .assertValue { it == expected }

        verify(mockApi, times(1)).getAllCountryCodes()
    }
}
