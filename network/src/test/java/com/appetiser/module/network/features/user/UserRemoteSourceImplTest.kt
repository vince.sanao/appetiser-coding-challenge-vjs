package com.appetiser.module.network.features.user

import com.appetiser.module.domain.utils.any
import com.appetiser.module.network.BaseplateApiServices
import com.appetiser.module.network.features.Stubs
import com.appetiser.module.network.features.user.models.UserDTO
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito

class UserRemoteSourceImplTest {

    private val mockApi = Mockito.mock(BaseplateApiServices::class.java)

    private lateinit var sut: UserRemoteSource

    @Before
    fun setUp() {
        sut = UserRemoteSourceImpl(mockApi)
    }

    @Test
    fun updateUserSession_ShouldReturnMappedUserSessionDtoToDomainUserSession() {
        val userSession = Stubs.USER_SESSION
        val response = Stubs.PROFILE_DATA_RESPONSE
        val accessToken = Stubs.ACCESS_TOKEN
        val expected = UserDTO.toDomain(response.data)

        Mockito.`when`(mockApi.updateUserInfo(any(), any()))
            .thenReturn(Single.just(response))

        sut.updateUser(accessToken, userSession)
            .test()
            .assertComplete()
            .assertValue { it == expected }

        Mockito.verify(mockApi, Mockito.times(1)).updateUserInfo(any(), any())
    }
}
