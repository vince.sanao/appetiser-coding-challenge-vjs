package com.appetiser.module.common.base

import android.app.Activity
import android.content.Intent
import android.content.pm.ActivityInfo
import android.hardware.camera2.CameraCharacteristics
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import androidx.appcompat.widget.AppCompatButton
import androidx.core.content.FileProvider
import androidx.databinding.ViewDataBinding
import com.appetiser.baseplate.common.R
import com.appetiser.module.common.extensions.createImageFile
import com.appetiser.module.common.extensions.ninjaTap
import com.appetiser.module.common.utils.PathUtils
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.tbruyelle.rxpermissions2.RxPermissions
import com.zhihu.matisse.Matisse
import com.zhihu.matisse.MimeType
import com.zhihu.matisse.engine.impl.GlideEngine
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber
import java.io.File
import java.io.IOException

abstract class BaseCameraFragment<B : ViewDataBinding, VM : BaseViewModel> :
    BaseViewModelFragment<B, VM>() {

    companion object {
        const val REQUEST_GALLERY = 0
        const val REQUEST_CAMERA = 1
        const val REQUEST_MULTIPLE_IMAGE_GALLERY = 2
    }

    private var captureCameraPaths: MutableList<String> = mutableListOf()

    val cameraPathUrls: List<String> get() = captureCameraPaths

    private lateinit var imagePickerBottomSheetDialog: BottomSheetDialog

    protected val rxPermissions: RxPermissions by lazy {
        RxPermissions(this)
    }

    private fun choosePhotoFromGallery() {
        val intent =
            Intent(
                Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI
            )
        intent.type = "image/*"

        if (getMultipleImageEnabled()) {
            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true)
        }

        startActivityForResult(
            Intent.createChooser(intent, getString(R.string.select_picture)),
            REQUEST_GALLERY
        )
    }

    private fun navigateToCameraPhoto() {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            // Ensure that there's a camera activity to handle the intent
            val resolvedActivity =
                takePictureIntent.resolveActivity(requireActivity().packageManager)

            if (resolvedActivity == null) {
                Timber.e("No activity for MediaStore.ACTION_IMAGE_CAPTURE!")
                return
            }

            // Create the File where the photo should go
            val photoFile: File? = try {
                createImageFile().apply {
                    addImage(this.absolutePath)
                }
            } catch (ex: IOException) {
                Timber.e(ex)
                null
            }

            if (photoFile == null) {
                Timber.e("File for capturing image not created!")
                return
            }

            // Continue only if the File was successfully created
            val photoURI: Uri = FileProvider.getUriForFile(
                requireContext(),
                "${requireActivity().packageName}.fileprovider",
                photoFile
            )
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
            takePictureIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
            takePictureIntent.putExtra(
                "android.intent.extras.CAMERA_FACING",
                CameraCharacteristics.LENS_FACING_FRONT
            )

            when {
                Build.VERSION.SDK_INT >= Build.VERSION_CODES.O -> {
                    takePictureIntent.putExtra(
                        "android.intent.extras.CAMERA_FACING",
                        CameraCharacteristics.LENS_FACING_FRONT
                    )
                    takePictureIntent.putExtra("android.intent.extra.USE_FRONT_CAMERA", true)
                }
                else -> takePictureIntent.putExtra("android.intent.extras.CAMERA_FACING", 1)
            }

            startActivityForResult(takePictureIntent, REQUEST_CAMERA)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupImagePickerDialog()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            REQUEST_GALLERY -> {
                if (resultCode == Activity.RESULT_OK) {
                    data?.data?.also { uri ->
                        val imagePath = PathUtils.getPath(requireContext(), uri)
                        if (imagePath != null) {
                            addImage(imagePath)
                            imageUrlPath(captureCameraPaths)
                        }
                    }
                }
            }
            REQUEST_CAMERA -> {
                if (resultCode == Activity.RESULT_CANCELED) {
                    deleteImage()
                } else {
                    imageUrlPath(captureCameraPaths)
                }
            }
            REQUEST_MULTIPLE_IMAGE_GALLERY -> {
                data?.let {
                    val obtainPathResult = Matisse.obtainPathResult(it)
                    if (obtainPathResult.isNotEmpty()) {
                        Observable.just(obtainPathResult)
                            .subscribeOn(scheduler.ui())
                            .subscribeBy(
                                onNext = { imageUrlPaths ->
                                    imageUrlPath(imageUrlPaths)
                                },
                                onError = {
                                    Timber.e("Error $it")
                                }
                            )
                            .addTo(disposables)
                    }
                }
            }
            else -> {
                super.onActivityResult(requestCode, resultCode, data)
            }
        }
    }

    private fun deleteImage() {
        Observable.fromIterable(captureCameraPaths)
            .flatMapCompletable { url ->
                Completable.create {
                    val file = File(url)
                    if (file.exists()) {
                        file.delete()
                    }
                    it.onComplete()
                }
            }
            .subscribeOn(scheduler.computation())
            .observeOn(scheduler.ui())
            .subscribeBy(
                onComplete = {
                    captureCameraPaths.clear()
                }
            )
            .addTo(disposables)
    }

    private fun setupImagePickerDialog() {
        imagePickerBottomSheetDialog =
            BottomSheetDialog(requireContext(), R.style.Styles_BottomSheet)

        val dialogView = this.layoutInflater.inflate(R.layout.bottom_sheet_upload_image, null)

        imagePickerBottomSheetDialog.setContentView(dialogView)

        val takePhoto = dialogView.findViewById<AppCompatButton>(R.id.btnTakePhoto)
        val gallery = dialogView.findViewById<AppCompatButton>(R.id.btnGallery)
        val cancelBtn = dialogView.findViewById<AppCompatButton>(R.id.btnCancel)

        takePhoto
            .ninjaTap {
                hidePickerDialog()
                navigateToCameraPhoto()
            }
            .addTo(disposables)

        gallery
            .ninjaTap {
                hidePickerDialog()

                if (getMultipleImageEnabled()) {
                    Matisse.from(this)
                        .choose(MimeType.ofImage(), false)
                        .countable(true)
                        .maxSelectable(10)
                        .restrictOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT)
                        .imageEngine(GlideEngine())
                        .forResult(REQUEST_MULTIPLE_IMAGE_GALLERY)
                } else {
                    choosePhotoFromGallery()
                }
            }
            .addTo(disposables)

        cancelBtn
            .ninjaTap {
                hidePickerDialog()
            }
            .addTo(disposables)
    }

    fun showPickerDialog() {
        imagePickerBottomSheetDialog.show()
    }

    fun hidePickerDialog() {
        imagePickerBottomSheetDialog.dismiss()
    }

    protected open fun getMultipleImageEnabled(): Boolean {
        return false
    }

    abstract fun imageUrlPath(captureCameraPaths: List<String>)

    private fun addImage(imagePath: String) {
        if (captureCameraPaths.isNotEmpty()) {
            captureCameraPaths.clear()
        }
        captureCameraPaths.add(imagePath)
    }
}
