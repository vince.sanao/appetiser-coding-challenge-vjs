package com.appetiser.module.common.base

import android.app.Activity
import android.content.Intent
import android.hardware.camera2.CameraCharacteristics
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import androidx.appcompat.widget.AppCompatButton
import androidx.core.content.FileProvider
import androidx.databinding.ViewDataBinding
import com.appetiser.baseplate.common.R
import com.appetiser.module.common.extensions.createImageFile
import com.appetiser.module.common.extensions.ninjaTap
import com.appetiser.module.common.utils.PathUtils
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.tbruyelle.rxpermissions2.RxPermissions
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import java.io.File
import java.io.IOException

abstract class BaseCameraActivity<B : ViewDataBinding, VM : BaseViewModel> :
    BaseViewModelActivity<B, VM>() {

    companion object {
        const val REQUEST_GALLERY = 0
        const val REQUEST_CAMERA = 1
    }

    private var captureCameraPath: String = ""

    val cameraUrl: String get() = captureCameraPath

    private lateinit var imagePickerBottomSheetDialog: BottomSheetDialog

    protected val rxPermissions: RxPermissions by lazy {
        RxPermissions(this)
    }

    private fun choosePhotoFromGallery() {
        val galleryIntent = Intent(Intent.ACTION_OPEN_DOCUMENT).apply {
            addCategory(Intent.CATEGORY_OPENABLE)
            type = "image/*"
        }
        startActivityForResult(
            Intent.createChooser(
                galleryIntent,
                getString(R.string.select_picture)
            ), REQUEST_GALLERY
        )
    }

    private fun navigateToCameraPhoto() {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            // Ensure that there's a camera activity to handle the intent
            takePictureIntent.resolveActivity(packageManager)?.also {
                // Create the File where the photo should go
                val photoFile: File? = try {
                    createImageFile().apply {
                        captureCameraPath = this.absolutePath
                    }
                } catch (ex: IOException) {
                    null
                }

                // Continue only if the File was successfully created
                photoFile?.also {
                    val photoURI: Uri = FileProvider.getUriForFile(
                        this,
                        "$packageName.fileprovider",
                        it
                    )
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                    takePictureIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                    takePictureIntent.putExtra(
                        "android.intent.extras.CAMERA_FACING",
                        CameraCharacteristics.LENS_FACING_FRONT
                    )

                    when {
                        Build.VERSION.SDK_INT >= Build.VERSION_CODES.O -> {
                            takePictureIntent.putExtra(
                                "android.intent.extras.CAMERA_FACING",
                                CameraCharacteristics.LENS_FACING_FRONT
                            )
                            takePictureIntent.putExtra(
                                "android.intent.extra.USE_FRONT_CAMERA",
                                true
                            )
                        }
                        else -> takePictureIntent.putExtra("android.intent.extras.CAMERA_FACING", 1)
                    }

                    startActivityForResult(takePictureIntent, REQUEST_CAMERA)
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupImagePickerDialog()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            REQUEST_GALLERY -> {
                if (resultCode == Activity.RESULT_OK) {
                    data?.data?.also { uri ->
                        val imagePath = PathUtils.getPath(this, uri)
                        if (imagePath != null) {
                            captureCameraPath = imagePath
                            setImagePathUrl(captureCameraPath)
                        }
                    }
                }
            }
            REQUEST_CAMERA -> {
                if (resultCode == Activity.RESULT_CANCELED) {
                    deleteImage()
                } else {
                    setImagePathUrl(captureCameraPath)
                }
            }
            else -> {
                super.onActivityResult(requestCode, resultCode, data)
            }
        }
    }

    private fun deleteImage() {
        disposables.add(Completable.create {
            val file = File(captureCameraPath)
            if (file.exists()) {
                file.delete()
            }
            it.onComplete()
        }
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(onComplete = {
                captureCameraPath = ""
            })
        )
    }

    private fun setupImagePickerDialog() {
        imagePickerBottomSheetDialog = BottomSheetDialog(this, R.style.Styles_BottomSheet)

        val dialogView = this.layoutInflater.inflate(R.layout.bottom_sheet_upload_image, null)

        imagePickerBottomSheetDialog.setContentView(dialogView)

        val takePhoto = dialogView.findViewById<AppCompatButton>(R.id.btnTakePhoto)
        val gallery = dialogView.findViewById<AppCompatButton>(R.id.btnGallery)
        val cancelBtn = dialogView.findViewById<AppCompatButton>(R.id.btnCancel)

        takePhoto
            .ninjaTap {
                hidePickerDialog()
                navigateToCameraPhoto()
            }
            .addTo(disposables)

        gallery
            .ninjaTap {
                hidePickerDialog()
                choosePhotoFromGallery()
            }
            .addTo(disposables)

        cancelBtn
            .ninjaTap {
                hidePickerDialog()
            }
            .addTo(disposables)
    }

    fun showPickerDialog() {
        imagePickerBottomSheetDialog.show()
    }

    fun hidePickerDialog() {
        imagePickerBottomSheetDialog.dismiss()
    }

    abstract fun setImagePathUrl(captureCameraPath: String)
}
