package com.appetiser.module.common.utils

import android.annotation.SuppressLint
import android.content.Context
import android.provider.Settings
import android.telephony.TelephonyManager
import android.util.Patterns
import androidx.annotation.*
import com.appetiser.module.common.PHOTO_COMPRESSION_QUALITY
import com.appetiser.module.common.PHOTO_MAX_DIMENSION
import io.reactivex.Maybe
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import java.io.File
import javax.inject.Inject

/**
 * Holder for android related calls so view models would still be testable.
 */
class ResourceManager @Inject constructor(
    private val context: Context
) {
    @ColorInt
    fun getColor(@ColorRes resId: Int): Int {
        return context.getColor(resId)
    }

    fun getString(@StringRes resId: Int): String {
        return context.getString(resId)
    }

    fun getString(@StringRes resId: Int, vararg formatArgs: Any): String {
        return context.getString(resId, *formatArgs)
    }

    fun getDimen(@DimenRes resId: Int) = context.resources.getDimension(resId)

    fun getInteger(@IntegerRes resId: Int) = context.resources.getInteger(resId)

    fun getAndroidPackageName() = context.applicationContext.packageName

    @SuppressLint("HardwareIds")
    fun getDeviceId(): String {
        return Settings.Secure.getString(context.contentResolver, Settings.Secure.ANDROID_ID)
    }

    fun getStringArray(@ArrayRes resId: Int): Array<String> {
        return context.resources.getStringArray(resId)
    }

    fun validateEmail(email: String): Boolean {
        return Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }

    fun getQuantityString(@PluralsRes resId: Int, quantity: Int, vararg formatArgs: Any): String {
        return context.resources.getQuantityString(resId, quantity, *formatArgs)
    }

    /**
     * Returns Pair of scaled down image path and path of original image.
     *
     * @param imagePath path of image to scale down
     */
    fun scaleDownImage(imagePath: String): Single<Pair<String, String>> {
        return if (imagePath.isBlank()) {
            Single
                .just(
                    Pair("", imagePath)
                )
        } else {
            FileUtils.scaleDownPhoto(
                "file:$imagePath",
                context,
                PHOTO_MAX_DIMENSION,
                PHOTO_COMPRESSION_QUALITY,
                false
            )
                .subscribeOn(Schedulers.computation())
                .map { scaledDownImageUri ->
                    Pair(scaledDownImageUri.path!!, imagePath)
                }
                .doOnError { Timber.e(it, "Error scaling down photo: $it") }
                .onErrorResumeNext {
                    Single
                        .just(
                            Pair("", imagePath)
                        )
                }
        }
    }

    /**
     * Returns scaled down image if scaling down succeeds. Else, returns original image.
     *
     * @param imagePath path of image to scale down
     */
    fun scaleDownImageToFile(imagePath: String): Maybe<File> {
        return if (imagePath.isBlank()) {
            Maybe.empty()
        } else {
            FileUtils.scaleDownPhoto(
                "file:$imagePath",
                context,
                PHOTO_MAX_DIMENSION,
                PHOTO_COMPRESSION_QUALITY,
                false
            )
                .map { scaledDownImageUri ->
                    File(scaledDownImageUri.path!!)
                }
                .doOnError { Timber.e(it, "Error scaling down photo: $it") }
                .onErrorResumeNext {
                    // scaling down of photo failed, return original file.
                    Single
                        .just(
                            File(imagePath)
                        )
                }
                .toMaybe()
        }
    }

    /**
     * Returns device's country name code (i.e. "ph", "au", "cn").
     */
    fun getDeviceCountryNameCode(): String {
        val telephonyManager = context.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
        return telephonyManager.networkCountryIso
    }
}
