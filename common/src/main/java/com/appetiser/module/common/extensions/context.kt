package com.appetiser.module.common.extensions

import android.content.Context
import android.content.Intent
import android.net.Uri
import androidx.annotation.AttrRes
import androidx.annotation.ColorInt
import com.google.android.material.color.MaterialColors

fun Context.openBrowser(url: String) {
    startActivity(
        Intent(
            Intent.ACTION_VIEW,
            Uri.parse(url)
        )
    )
}

@ColorInt
fun Context.getThemeColor(@AttrRes attrResId: Int): Int {
    return MaterialColors
        .getColor(
            this,
            attrResId,
            android.graphics.Color.TRANSPARENT
        )
}
