package com.appetiser.module.common.extensions

import android.text.Editable
import android.text.TextWatcher
import android.text.method.PasswordTransformationMethod
import android.widget.EditText
import androidx.annotation.DrawableRes
import com.appetiser.module.common.widget.CustomPasswordTransformation
import com.google.android.material.textfield.TextInputLayout
import com.jakewharton.rxbinding3.widget.textChangeEvents
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.subscribeBy
import java.util.*

fun EditText.capitalizeFirstLetter(): Disposable {
    return this.textChangeEvents()
        .skipInitialValue()
        .observeOn(AndroidSchedulers.mainThread())
        .map {
            val typeString = it.text.toString()
            if (typeString.length == 1) {
                if (typeString.single().isLowerCase()) {
                    setText(typeString.toUpperCase(Locale.getDefault()))
                }
            }
            return@map typeString
        }
        .subscribeBy(
            onNext = {
                setSelection(it.length)
            }
        )
}

fun EditText.setDrawableEnd(@DrawableRes drawableId: Int) {
    setCompoundDrawablesWithIntrinsicBounds(
        0,
        0,
        drawableId,
        0
    )
}

fun EditText.indicatorIcons(@DrawableRes enabledIcon: Int, @DrawableRes disabled: Int) {
    addTextChangedListener(object : TextWatcher {

        override fun onTextChanged(input: CharSequence, start: Int, before: Int, count: Int) {
            if (input.isNotEmpty()) {
                setDrawableEnd(enabledIcon)
            } else {
                setDrawableEnd(disabled)
            }
        }

        override fun afterTextChanged(s: Editable?) {}

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
    })
}

fun TextInputLayout.setCustomPasswordTransformation() {
    setEndIconOnClickListener {
        editText?.let { et ->
            // Store the current cursor position
            val selection = et.selectionEnd

            if (et.transformationMethod != null &&
                et.transformationMethod is PasswordTransformationMethod
            ) {
                et.transformationMethod = null
            } else {
                et.transformationMethod = CustomPasswordTransformation.getInstance()
            }

            // And restore the cursor position
            if (selection >= 0) {
                et.setSelection(selection)
            }
        }
    }

    editText?.transformationMethod = CustomPasswordTransformation.getInstance()
}
