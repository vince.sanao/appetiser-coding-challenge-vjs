package com.appetiser.module.local.features.track

interface TrackLocalSource {
    fun getDateFromPref(): String

    fun saveDateFromPref(date: String)
}
