package com.appetiser.module.local.features.user.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.appetiser.module.domain.models.user.MediaFile
import com.appetiser.module.domain.models.user.PrimaryUserNameType
import com.appetiser.module.domain.models.user.User
import java.time.Instant

@Entity(tableName = UserDB.USER_TABLE_NAME)
data class UserDB(
    @ColumnInfo(name = "full_name")
    var fullName: String? = "",
    @ColumnInfo(name = "first_name")
    var firstName: String? = "",
    @ColumnInfo(name = "last_name")
    var lastName: String? = "",
    var email: String? = "",
    @ColumnInfo(name = "avatar_permanent_url")
    var avatarPermanentUrl: String? = "",
    @ColumnInfo(name = "avatar_permanent_thumb_url")
    var avatarPermanentThumbUrl: String? = "",
    @ColumnInfo(name = "phone_number")
    var phoneNumber: String? = "",
    @ColumnInfo(name = "email_verified")
    var emailVerified: Boolean = false,
    @ColumnInfo(name = "phone_number_verified")
    var phoneNumberVerified: Boolean = false,
    var verified: Boolean = false,
    @PrimaryKey
    var uid: String = "",
    var avatar: MediaFile = MediaFile.empty(),
    var description: String = "",
    @ColumnInfo(name = "birthdate") var birthDate: String = "",
    @ColumnInfo(name = "created_at")
    var createdAt: Instant = Instant.now(),
    @ColumnInfo(name = "updated_at")
    var updatedAt: Instant = Instant.now(),
    @ColumnInfo(name = "blocked_at")
    var blockedAt: Instant? = Instant.now(),
    @ColumnInfo(name = "onboarded_at")
    var onboardedAt: Instant? = Instant.now(),
    @ColumnInfo(name = "primary_username")
    val primaryUserNameType: String = ""
) {

    companion object {
        const val USER_TABLE_NAME = "user"
        const val EMPTY_USER_ID = "empty"

        /**
         * Returns an empty user.
         */
        fun empty(): UserDB {
            return UserDB(
                fullName = EMPTY_USER_ID
            )
        }

        fun fromDomain(user: User): UserDB {
            return with(user) {
                UserDB(
                    fullName = fullName,
                    firstName = firstName,
                    lastName = lastName,
                    email = email,
                    avatarPermanentUrl = avatarPermanentUrl,
                    avatarPermanentThumbUrl = avatarPermanentThumbUrl,
                    phoneNumber = phoneNumber,
                    emailVerified = emailVerified,
                    phoneNumberVerified = phoneNumberVerified,
                    verified = verified,
                    uid = id,
                    avatar = avatar,
                    description = description,
                    birthDate = birthDate,
                    blockedAt = blockedAt,
                    updatedAt = updatedAt ?: Instant.now(),
                    createdAt = createdAt ?: Instant.now(),
                    onboardedAt = onBoardedAt,
                    primaryUserNameType = if (primaryUserNameType == PrimaryUserNameType.EMAIL) PrimaryUserNameType.EMAIL.value else PrimaryUserNameType.PHONE_NUMBER.value
                )
            }
        }

        fun toDomain(userDB: UserDB): User {
            return with(userDB) {
                User(
                    phoneNumber = phoneNumber.orEmpty(),
                    emailVerified = emailVerified,
                    phoneNumberVerified = phoneNumberVerified,
                    verified = verified,
                    email = email.orEmpty(),
                    lastName = lastName.orEmpty(),
                    firstName = firstName.orEmpty(),
                    fullName = fullName.orEmpty(),
                    avatarPermanentUrl = avatarPermanentUrl.orEmpty(),
                    avatarPermanentThumbUrl = avatarPermanentThumbUrl.orEmpty(),
                    id = uid,
                    avatar = avatar,
                    description = description,
                    birthDate = birthDate,
                    blockedAt = blockedAt,
                    updatedAt = updatedAt,
                    createdAt = createdAt,
                    onBoardedAt = onboardedAt,
                    primaryUserNameType = if (primaryUserNameType == PrimaryUserNameType.EMAIL.value) PrimaryUserNameType.EMAIL else PrimaryUserNameType.PHONE_NUMBER
                )
            }
        }
    }

    fun isEmptyUser(): Boolean {
        return fullName == EMPTY_USER_ID
    }
}
