package com.appetiser.module.local.features

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.appetiser.module.local.BuildConfig
import com.appetiser.module.local.features.token.dao.TokenDao
import com.appetiser.module.local.features.token.models.AccessTokenDB
import com.appetiser.module.local.features.user.dao.UserDao
import com.appetiser.module.local.features.user.models.UserDB
import com.appetiser.module.local.features.user.models.UserDBConverters
import net.sqlcipher.database.SQLiteDatabase
import net.sqlcipher.database.SupportFactory

@Database(
    entities = [
        UserDB::class,
        AccessTokenDB::class
    ],
    version = 1,
    exportSchema = true
)
@TypeConverters(UserDBConverters::class)
abstract class AppDatabase : RoomDatabase() {

    abstract fun userDao(): UserDao

    abstract fun tokenDao(): TokenDao

    companion object {
        @Volatile
        private var INSTANCE: AppDatabase? = null

        fun getInstance(context: Context): AppDatabase =
            INSTANCE ?: synchronized(this) {
                INSTANCE
                    ?: buildDatabase(context).also { INSTANCE = it }
            }

        private fun buildDatabase(context: Context): AppDatabase {
            val dbName = "codingappchallenge.db"

            val builder = Room.databaseBuilder(context, AppDatabase::class.java, dbName)
                .fallbackToDestructiveMigration()

            if (!BuildConfig.DEBUG) {
                val passphrase = SQLiteDatabase.getBytes(dbName.toCharArray())
                val factory = SupportFactory(passphrase)
                builder.openHelperFactory(factory)
            }

            return builder.build()
        }
    }
}
