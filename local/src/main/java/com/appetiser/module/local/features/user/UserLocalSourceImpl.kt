package com.appetiser.module.local.features.user

import androidx.room.EmptyResultSetException
import com.appetiser.module.domain.models.user.User
import com.appetiser.module.domain.utils.OnErrorResumeNext
import com.appetiser.module.local.features.user.dao.UserDao
import com.appetiser.module.local.features.user.models.UserDB
import io.reactivex.Completable
import io.reactivex.Single
import timber.log.Timber
import javax.inject.Inject

class UserLocalSourceImpl @Inject constructor(
    private val userDao: UserDao
) : UserLocalSource {
    override fun saveUser(user: User): Single<User> {
        return userDao
            .deleteUsers()
            .andThen(userDao.saveUser(UserDB.fromDomain(user)))
            .map {
                Timber.tag("saveUser").d("$it")
                user
            }
    }

    override fun getUser(): Single<User> {
        return userDao
            .getUser()
            .compose(
                OnErrorResumeNext<UserDB, EmptyResultSetException>(
                    UserDB.empty(),
                    EmptyResultSetException::class.java
                )
            )
            .map {
                UserDB.toDomain(it)
            }
    }

    override fun deleteUser(): Completable {
        return userDao.deleteUsers()
    }
}
