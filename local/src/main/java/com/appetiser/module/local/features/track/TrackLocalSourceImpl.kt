package com.appetiser.module.local.features.track

import android.content.SharedPreferences
import javax.inject.Inject

class TrackLocalSourceImpl @Inject constructor(
    private val sharedPreferences: SharedPreferences
) : TrackLocalSource {

    companion object {
        private const val PREF_DATE = "PREF_DATE"
    }

    override fun getDateFromPref(): String {
        return sharedPreferences.getString(PREF_DATE, "") ?: ""
    }

    override fun saveDateFromPref(date: String) {
        sharedPreferences
            .edit()
            .apply {
                putString(PREF_DATE, date)
                apply()
            }
    }
}
