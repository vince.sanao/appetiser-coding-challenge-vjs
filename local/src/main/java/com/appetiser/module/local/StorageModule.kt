package com.appetiser.module.local

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import androidx.security.crypto.EncryptedSharedPreferences
import androidx.security.crypto.MasterKeys
import com.appetiser.module.local.features.AppDatabase
import com.appetiser.module.local.features.session.SessionLocalSource
import com.appetiser.module.local.features.session.SessionLocalSourceImpl
import com.appetiser.module.local.features.token.AccessTokenLocalSource
import com.appetiser.module.local.features.token.AccessTokenLocalSourceImpl
import com.appetiser.module.local.features.track.TrackLocalSource
import com.appetiser.module.local.features.track.TrackLocalSourceImpl
import com.appetiser.module.local.features.user.UserLocalSource
import com.appetiser.module.local.features.user.UserLocalSourceImpl
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class StorageModule {

    @Provides
    @Singleton
    fun providesSharedPreferences(application: Application): SharedPreferences {
        return if (BuildConfig.DEBUG) {
            application.getSharedPreferences(
                getDefaultPreferencesName(application),
                Context.MODE_PRIVATE
            )
        } else {
            val masterKeyAlias = MasterKeys.getOrCreate(MasterKeys.AES256_GCM_SPEC)
            EncryptedSharedPreferences
                .create(
                    getDefaultPreferencesName(application),
                    masterKeyAlias,
                    application,
                    EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
                    EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
                )
        }
    }

    private fun getDefaultPreferencesName(application: Application): String {
        return application.packageName + "_preferences"
    }

    @Provides
    @Singleton
    fun providesUserLocalSource(appDatabase: AppDatabase): UserLocalSource {
        return UserLocalSourceImpl(appDatabase.userDao())
    }

    @Provides
    @Singleton
    fun providesAccessTokenLocalSource(
        sharedPreferences: SharedPreferences,
        appDatabase: AppDatabase
    ): AccessTokenLocalSource {
        return AccessTokenLocalSourceImpl(
            sharedPreferences,
            appDatabase.tokenDao()
        )
    }

    @Provides
    @Singleton
    fun providesSessionLocalSource(
        userLocalSource: UserLocalSource,
        accessTokenLocalSource: AccessTokenLocalSource,
        sharedPreferences: SharedPreferences
    ): SessionLocalSource {
        return SessionLocalSourceImpl(
            userLocalSource,
            accessTokenLocalSource,
            sharedPreferences
        )
    }

    @Provides
    @Singleton
    fun providesTrackLocalSource(
        sharedPreferences: SharedPreferences
    ): TrackLocalSource {
        return TrackLocalSourceImpl(sharedPreferences)
    }
}
